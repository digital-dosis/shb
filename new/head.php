<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">

    <meta name="description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. “Fast food” pero “Healthy” y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. " />
    <meta name="keywords" content="Surf House Barcelona, smoothies, milkshakes, betidos, sandwich, hamburguesa, burger, fitness, health, Barceloneta, playa, restaurant, cafetería, frappe, cocktail, desayunos, comida, beach, ensaladas, food." />

    <base href="/surfhouse/">

    <link rel="apple-touch-icon-precomposed" href="apple-touch-SHB.jpg" />
    <link rel="shortcut icon" href="img/shb-favicon.png" />

    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/outdatedbrowser.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/animations.css" />

    <script type="text/javascript" src="js/vendor/jquery-2.1.4.min.js,vendor/modernizr.custom.js,vendor/fsvs.js,vendor/respimage.min.js,vendor/dragdealer.min.js,vendor/classie.js,vendor/jquery.simpleWeather.min.js"></script>

    <script type="text/javascript" src="js/vendor/outdatedbrowser.min.js"></script>
