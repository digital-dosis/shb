<?php
/*
  ------------------
  Language: Castellano
  ------------------
 */

$lang = array();

$lang['IDIOMA'] =

    'es';

$lang['COOKIES_SHB'] =

    'Esta web utiliza cookies para dar un mejor servicio. Las cookies no se utilizan para recoger informaci&oacute;n personal. Si contin&uacute;a navegando se considera que acepta su uso.';

$lang['VIEW_OUR_MENU'] =

    'VER NUESTRO MENÚ';

$lang['HIGHLIGHTED_ACTIVITY'] =

    'ACTIVIDAD DESTACADA';

$lang['TODAY_AT'] =

    'HOY A LAS';

$lang['VIEW_ALL_ACTIVITIES'] =

    'VER LAS ACTIVIDADES';

$lang['NEXT_EVENT'] =

    'SIGUIENTE EVENTO';

$lang['NEXT_EVENT_NAME'] =

    'PALO ALTO MARKET';

$lang['NEXT_EVENT_DATE'] =

    '01 AL 02 DE OCTUBRE';

$lang['VIEW_ALL_EVENTS'] =

    'MÁS INFORMACIÓN';

$lang['SOMETHING_ABOUT_US'] =

    'SOBRE NOSOTROS';

$lang['NOW_IN'] =

    'AHORA EN LA';

$lang['ASK_US_FOR_TIMES'] =
     '¡PREGÚNTANOS!';

$lang['WHENEVER_YOU_LIKE'] =
     '¡CUANDO TÚ QUIERAS!';

$lang['MORE_INFO'] =
     'M&Aacute;S INFO';
/*
  ------------------
  FOOD & DRINKS
  ------------------
 */

$lang['INTRO_TEXT_FD'] =

    'Comida desenfadada distinta y&nbsp;de calidad';

$lang['INTRO_TEXT_2_FD'] =

    'Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. ¡no es broma! ';

$lang['CARTA'] =

    'La Carta';


$lang['DOWNLOAD_MENU'] =

    'VER CARTA';

$lang['FD_LAST_TEXT_TOP'] =

  'ARE U HUNGRY?';

$lang['FD_LAST_TEXT_BOTTOM'] =

  'COME and VISIT US';

$lang['PDF_MENU'] =

    'MENU_SHB_CASTELLANO.pdf';

$lang['PDF_MENU_2'] =

    'Carta-BRUNCH.pdf';

$lang['PDF_MENU_3'] =

    'Carta-DINNER.pdf';

$lang['PDF_BEBIDAS'] =

    'BEBIDAS_SHB_CASTELLANO.pdf';

/*
  ------------------
  ACTIVITIES
  ------------------
 */

$lang['AC_TEXT'] =

    '¡queremos que sea toda una experiencia!
    <p>Ofrecemos actividades diarias como paddle surf, workout en la playa para estar en forma, running, broadcast de campeonatos de surf…</p>';

$lang['ACTIVITIES'] =

    'ACTIVIDADES';

$lang['ACTIVITIES_LOWER'] =

    'Actividades';

$lang['AUTUM_WINTER'] =

    'OTOÑO / INVIERNO';

$lanf['SPECIAL_EVENTS'] =

    'EVENTOS <span>ESPECIALES</span>';

$lang['DOWNLOAD_PDF'] =

    'DESCARGAR PDF';

$lang['AC_LAST_TEXT_TOP'] =

    '<span>EAT GOOD,</span>';

$lang['AC_LAST_TEXT_BOTTOM'] =

    '<span>LOOK GOOD, FEEL GOOD</span>';
$lang['TODAY'] =

    '¡HOY!';

$lang['PDF_ACTIVITIES'] =

    'Agenda-SHB(ESP).pdf';

$lang['AC_LOC_TEXT_TOP'] =

    'RESERVA CUALQUIER ACTIVIDAD ESCRIBIÉNDONOS A:';

$lang['AC_LOC_MAIL'] =

    'actividades@surfhousebarcelona.com';

$lang['RESERVE'] =

    '¡RESERVA!';
/*
  ------------------
  ABOUT
  ------------------
 */

$lang['ABOUT_SHB'] =

    'SOBRE SHB';

$lang['ABOUT_SHB_LOWER'] =

    'Sobre SHB';

$lang['AB_TEXT_1'] =

    'SHB';

$lang['AB_TEXT_2'] =

    'Surf House Barcelona nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. Comida desenfadada, distinta y de calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca.<br>¡No es broma!';

$lang['AB_TEXT_3'] =

    'Pero SHB no es solo un lugar en el que comer y beber, queremos que sea una experiencia. Ofrecemos actividades como Paddle Surf, Yoga, Running, retransmisiones de campeonatos de surf en directo, eventos, presentaciones de marcas y viajes de surf.';

$lang['AB_TEXT_4'] =

    'Bienvenido a SHB. Esperamos que pronto seas un miembro más de esta gran familia.';

$lang['AB_TEXT_5'] =

    'VER EL VIDEO DE NUESTRO 5º ANIVERSARIO';

$lang['AB_LAST_TEXT_TOP'] =

    "LIFE'S A WAVE,";

$lang['AB_LAST_TEXT_BOTTOM'] =

    "CATCH IT";
/*
  ------------------
  TRIPS
  ------------------
 */

$lang['TRIPS_SHB'] =

    'TRIPS';

$lang['TRIPS_TEXT_1'] =

    'SHB <span>CUSTOM SURF TRIPS</span>';

$lang['TRIPS_TEXT_2'] =

    'SHB TRIPS es nuestra plataforma para crear tu viaje de surf idóneo.';

$lang['TRIPS_TEXT_3'] =

    'Somos amantes de este deporte y nos apasiona viajar, conocer gente y compartir experiencias únicas.';

$lang['TRIPS_TEXT_4'] =

    'Contamos con años de aventuras por todo el mundo y conocemos muy bien los destinos a los que vamos.';

$lang['TRIPS_TEXT_5'] =

    'Lugares idílicos con olas perfectas para todos los niveles y calor. ';

$lang['TRIPS_TEXT_6'] =

    'Todos los viajes incluyen fotógrafo y guía para hacer de tu viaje una experiencia inolvidable.';

$lang['TRIPS_TEXT_7'] =

    'Únete a nuestra próxima experiencia o dinos cuál es tu viaje de ensueño y te lo organizamos.';

$lang['TRIPS_TEXT_8'] =

    'Buen viaje!';

$lang['TRIPS_VIDEOS'] =

    "VIDEOS";

$lang['TRIPS_LAST_TEXT_TOP'] =

    "SURF, EAT, SLEEP...";

$lang['TRIPS_LAST_TEXT_BOTTOM'] =

    "¡REPEAT!";

$lang['TRIPS_INFO_1'] =

    "ESCRÍBENOS PARA MÁS INFORMACIÓN";

$lang['TRIPS_INFO_2'] =

    "ACTIVIDADES@SURFHOUSEBARCELONA.COM";

/*
  ------------------
  SECRET SPOT
  ------------------
 */

$lang['SECRETSPOT_SHB'] =

     'SECRET SPOT';

$lang['SECRETSPOT_TEXT_1'] =

     'SHB';

$lang['SECRETSPOT_TEXT_2'] =

     'Tras seis años del nacimiento de Surf House en la playa de la Barceloneta, nace Surf House Secret Spot.';

$lang['SECRETSPOT_TEXT_3'] =

     'Un rincón especial y acogedor, como esa playa tan recóndita y escondida que solo compartes con los tuyos.';

$lang['SECRETSPOT_TEXT_4'] =

     'Mismos valores, mismos objetivos, misma ilusión y mismo ambiente. Aunque en un zona más alejada del mar y las olas, mismo buen rollo y misma calidad en cada uno de sus detalles.';

$lang['SECRETSPOT_TEXT_5'] =

     'Actividades deportivas en los parques de alrededor, eventos semanales, proyecciones de campeonatos en directo y muchas cosas que se irán cociendo en la cocina de este nuevo mágico spot de Barcelona.';

$lang['SECRETSPOT_TEXT_6'] =

     'Carta Brunch';

$lang['SECRETSPOT_TEXT_7'] =

     'Carta Cenas';

$lang['SECRETSPOT_LAST_TEXT_TOP'] =

     'COME AND DISCOVER';

$lang['SECRETSPOT_LAST_TEXT_BOTTOM'] =

     'WHAT IT HIDES!';

/*
  ------------------
  SHOP
  ------------------
 */

$lang['SHB_SHOP'] =

    'SURF HOUSE SHOP';

$lang['SH_SHOP'] =

    'SH SHOP';

/*
  ------------------
  TEAM
  ------------------
 */

  $lang['ALEX_PONS_TEXT'] =

  'Alex paso de competir en esquí alpino a surfear y ser reconocido por tener posiblemente el mejor estilo en toda la costa mediterránea.  Local de una de las mejores derechas, Port Ginesta (Castelldefels) ha ido evolucionando mucho en los últimos años y si consigues verlo en el pico disfrutarás mucho de su surfing.';

$lang['JONY_GONZALEZ_TEXT'] =

  'El Jonathan Gonzalez, alias Jony es el campeón de Europa de 2017. Posiblemente tenga unos de los mejores estilos de todo el circuito mundial y su personalidad humilde y tranquila hacen de Jony un surfista 10. Es local de Tenerife pero siempre está viajando para competir en las mejores pruebas del circuito WQS. Suele hacer escala en Barcelona y no duda en pasar por SHB a comerse una Burger con su zumito y siempre, un café para terminar. Desde luego, el mejor surfista que podríamos tener en nuestro team.';

$lang['JAN_BEIN_TEXT'] =

  'Jan vive en Castelldefels y tan solo tiene 14 años. Siempre que puede se escapa a buscar olas y cuando no las hay y los estudios se lo permiten está dándole en cualquier skatepark cerca de casa. Tiene la gran suerte de que sus padres también surfean, así que ha tenido la oportunidad de viajar bastante y descubrir olas de varias partes del mundo. Además le gusta la comida sana, estar con la family y sus amigos.';

$lang['HELEN_SAIGI_TEXT'] =

  'Surfista con mucho estilo en su tiempo libre y tatuadora como profesión, con tan solo 20 añitos y surfeando desde los 16, Helena es una de nuestras mejores surfistas de Barcelona y alrededores. Su sueño es vivir en un sitio de olas perfectas y poder vivir de tatuar a los suyos. El Snowboard es su segundo hobby tras el Surf mientras que el Skate lo tuvo que dejar por tener que cuidar de sus manos para poder tatuar las maravillas que tatúa. ';

$lang['ANDREA_LUELMO_TEXT'] =

  'Andrea es una de las surfistas con más estilo y clase en el agua de nuestra costa. Siempre que hay olas y su trabajo lo permite, podemos verla en las playas de Barcelona y alrededores. Desde que empezó a trabajar en Pukas Surf Eskola de Barcelona se aficionó mucho más a lo que ahora es su pasión. A la que puede se escapa a Zarautz o a Indonesia y cuando no hay olas en casa, su segundo lugar favorito para pasar el rato y reunirse con amig@s es SHB.';

  $lang['MARTA_DAVILA_TEXT'] = 

  'Skater&Surfer de Sitges e ingeniera de Datos e Inteligencia Artificial, cosa que estudió en Holanda al terminar el bachillerato, trabaja en la agencia Espacial Europea (lo que sería la NASA de Europa) desarrollando el software del satélite meteosat. Cuando no trabaja se pasa sus días buscando skateparks y olas donde poder patinar o surfear. Su combinación agua-mar hace de Marta una rider muy peculiar que va a dar mucho de que hablar.';

$lang['ALEX_KNOPFEL_TEXT'] =

  'Su pasión por el Surf le llevó a crear Surf House Barcelona. Siempre pensando en nuevos platos o zumos y cuando puede, se escapa a surfear ya sea delante de su trabajo o escapándose de viaje a cualquier lugar con olitas.';

$lang['CARLES_MEDINA_TEXT'] =

  'Carles Medina, surfer innovador y creativo de la marca MBC es un joven con un surf radical que  caracteriza por su buen rollo y  sonrisa permanente en la cara tanto dentro como fuera del agua. Su estancia en Australia le hizo volver a su ciudad natal con mucho mas surfing y con un proyecto que hoy en día es muy prometedor.';

$lang['CLARIS_CARRASCO_TEXT'] =

  'Con raíces Koreanas y local de Barceloneta es una de nuestras riders favoritas. Se inició en la playa de Razo, Galicia y desde entonces no ha dejado de entrar al agua y progresar. Siempre que no está en clase la vemos en el agua o en el skatepark. dejándose las rodillas. Surfea desde los 15 añitos y su afición por el patín le ayuda a progresar dentro del agua. Siempre busca hacer alguna maniobra radical y cada día que la vemos en el agua nos sorprende más. Serán nuestros zumitos que le dan ese power?';



$lang['PABLO_VIAPLANA_TEXT'] =

  'Le Cucut, así se hace llamar nuestro Dj preferido y surfista peculiar. Si no está en el agua lo podemos ver poniendo buenísima música como “Mambo Number 5” en la terraza de SHB o en locales con Be Cool, en Barcelona. No es un DJ normal, tiene algo especial. El tiempo lo dirá, atentos.';


$lang['ELI_MELERO_TEXT'] =

  'Eli Melero es nuestra integrante del team más internacional. Amante de los viajes y estancias largas en otros países ha ido progresando su surfing año tras año. Tenemos la suerte de tenerla temporadas en Barcelona visitándonos en SHB para explicarnos sus hazañas.';



$lang['ALBERT_HERNANDEZ_TEXT'] =

  'Campeón Junior en Galicia en 2007. El primer catalán en salir y ganar un circuito en un lugar como Galicia donde hay buen surf. Tuvo unos añitos de parón por trabajo pero ha vuelto muy fuerte: Humilde, simpático y muy motivador para ir a la playa que tenga lo mínimo para practicar el deporte que más nos mueve a todos.';

$lang['ADRIA_PERARNAU_TEXT'] =

  'Adriá Perarnau  desde bien pequeño empezó con la natación y eso hizo que su afición por el agua le llevase a probar el surf, cogiendo la mínima espuma que le arrastrara. A partir de esto el surf y el mar son para Adri más que una afición. Siempre que puede saca tiempo para escaparse en busca de las mejores olas. Su dia a dia gira entorno a compaginar su trabajo con poder entrar al agua siempre que haya un baño.<br>¡Un baño es un baño!';

/* COMIDA */

$lang['PATATAS'] =

    'Patatas';

$lang['COSTILLAS_BBQ'] =

    'Costillas<br/>BBQ';

$lang['HELADOS_SHB'] =

    'Helados SHB';

$lang['GOFRE_NUTELLA'] =

    'Gofre<br/>Nutella';

/* BEBIDA */

$lang['LILA'] =

    'Lila';

$lang['AGUA_DE_COCO'] =

    'Agua <br/>de Coco';

$lang['TU_CREACION'] =

    'Tu Creación';

/*
  ------------------
  SCHEDULE
  ------------------
 */


$lang['HORARIO'] =

  'HORARIO:';

$lang['MIERCOLES'] =

  'LUNES Y MARTES: CERRADO';


$lang['VIERNES'] =

  'MIÉRCOLES Y JUEVES: 13h a 20h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VIERNES Y SÁBADO: 12h a 24h';

$lang['DOMINGO'] =

  'DOMINGO: 12h a 22h';

$lang['HORARIO_1'] =

  '<strong>Lunes y Martes:</strong> Cerrado';

$lang['HORARIO_2'] =

  '<strong>Miércoles y Jueves:</strong> 12h a 18h';

$lang['HORARIO_3'] =

  '<strong>Viernes y Sábado:</strong> 12h a 23h';

$lang['HORARIO_4'] =

  '<strong>Domingo:</strong> 12h a 22h';

$lang['HORARIO_5'] =

  '<strong>Lunes:</strong> Cerrado';

$lang['HORARIO_6'] =

  '<strong>Martes a Viernes:</strong> 13.30h a 16h y 19.30 a 24h';

$lang['HORARIO_7'] =

  '<strong>Viernes:</strong> 10h a 1h';

$lang['HORARIO_8'] =

  '<strong>Sábado:</strong> 10h a 16h y de 20h a 1h';

$lang['HORARIO_9'] =

  '<strong>Domingo:</strong> 10h a 17h';


?>
