<?php
/*
  ------------------
  Language: English
  ------------------
 */

$lang = array();

$lang['IDIOMA'] =

    'en';

$lang['COOKIES_SHB'] =

    'This website uses cookies to provide a better service.<br>The cookies are not used to collect personal information. Continuing browsing means that you accept that use.';

$lang['VIEW_OUR_MENU'] =

    'VIEW OUR MENU';

$lang['HIGHLIGHTED_ACTIVITY'] =

    'HIGHLIGHTED ACTIVITY';

$lang['TODAY_AT'] =

    'TODAY AT';

$lang['VIEW_ALL_ACTIVITIES'] =

    'VIEW ALL ACTIVITIES';

$lang['NEXT_EVENT'] =

    'NEXT EVENT';

$lang['NEXT_EVENT_NAME'] =

    'PALO ALTO MARKET';

$lang['NEXT_EVENT_DATE'] =

  'OCTOBER 1th to 2nd';

$lang['VIEW_ALL_EVENTS'] =

    'MORE INFO';

$lang['SOMETHING_ABOUT_US'] =

    'SOMETHING ABOUT US';

$lang['NOW_IN'] =

    'NOW IN';

$lang['ASK_US_FOR_TIMES'] =
     'ASK US FOR TIMES';

$lang['WHENEVER_YOU_LIKE'] =
     'WHENEVER YOU LIKE!';

$lang['MORE_INFO'] =
         'MORE INFO';

/*
  ------------------
  FOOD & DRINKS
  ------------------
 */

$lang['INTRO_TEXT_FD'] =

    'Casual, different and&nbsp;quality food';

$lang['INTRO_TEXT_2_FD'] =

    'since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['CARTA'] =

    'Menu';

$lang['DOWNLOAD_MENU'] =

    'VIEW MENU';

$lang['FD_LAST_TEXT_TOP'] =

  'ARE U HUNGRY?';

$lang['FD_LAST_TEXT_BOTTOM'] =

  'COME and VISIT US';

$lang['PDF_MENU'] =

    'MENU_SHB_INGLES.pdf';

$lang['PDF_MENU_2'] =

    'Carta-BRUNCH.pdf';

$lang['PDF_MENU_3'] =

    'Carta-DINNER.pdf';

$lang['PDF_BEBIDAS'] =

    'BEBIDAS_SHB_INGLES.pdf';

/*
  ------------------
  ACTIVITIES
  ------------------
 */

$lang['AC_TEXT'] =

    'we want you <br/>to have a full experience!
                    <p>We offer DAILY activities such as paddle surf, BEACH workouts to stay in shape, RUNNING, surfing championship broadcasts...</p>';

$lang['ACTIVITIES'] =

    'ACTIVITIES';

$lang['ACTIVITIES_LOWER'] =

    'Activities';

$lang['AUTUM_WINTER'] =

    'AUTUMN / WINTER';

$lanf['SPECIAL_EVENTS'] =

    'SPECIAL <span>EVENTS</span>';

$lang['DOWNLOAD_PDF'] =

    'DOWNLOAD PDF';

$lang['AC_LAST_TEXT_TOP'] =

    '<span>EAT GOOD,</span>';

$lang['AC_LAST_TEXT_BOTTOM'] =

    '<span>LOOK GOOD, FEEL GOOD</span>';

$lang['TODAY'] =

    'TODAY!';

$lang['PDF_ACTIVITIES'] =

    'Agenda-SHB(ENG).pdf';

$lang['AC_LOC_TEXT_TOP'] =

    'SEND US AN EMAIL TO RESERVE ANY ACTIVITY';

$lang['AC_LOC_MAIL'] =

    'actividades@surfhousebarcelona.com';

$lang['RESERVE'] =

    'BOOK!';
/*
  ------------------
  ABOUT
  ------------------
 */

$lang['ABOUT_SHB'] =

    'ABOUT SHB';

$lang['ABOUT_SHB_LOWER'] =

    'About SHB';

$lang['AB_TEXT_1'] =

    'SHB';

$lang['AB_TEXT_2'] =

    'Surf House Barcelona has its origins in California’s lifestyle and we want to give you something different to what you can find anywhere else in the city.Casual, different and quality food. Since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['AB_TEXT_3'] =

    'But SHB is not a place where you’ll only come to eat and drink, we want you to have a full experience! We offer activities such as paddle surf, workouts to stay in shape (SHB Insane Training), jogging on the beach boardwalk, live surfing championship broadcasts, brand launch events, surf school…';

$lang['AB_TEXT_4'] =

    'Welcome to SHB. We hope you become a new member of this family!';

$lang['AB_TEXT_5'] =

    'VIEW THE VIDEO OF OUR 5th ANNIVERSARY';

$lang['AB_LAST_TEXT_TOP'] =

    "LIFE'S A WAVE,";

$lang['AB_LAST_TEXT_BOTTOM'] =

    "CATCH IT";

/*
  ------------------
  TRIPS
  ------------------
 */

$lang['TRIPS_SHB'] =

    'TRIPS';

$lang['TRIPS_TEXT_1'] =

    'SHB <span>CUSTOM SURF TRIPS</span>';

$lang['TRIPS_TEXT_2'] =

    'SHB Trips is our own platform to create your perfect surf trip.';

$lang['TRIPS_TEXT_3'] =

    'We are surfers and traveling, meeting people and sharing experiences is our passion.';

$lang['TRIPS_TEXT_4'] =

    'We have been doing this for years and know the places we are going to very well.';

$lang['TRIPS_TEXT_5'] =

    'Paradises with perfect weaves, warmth and safety.';

$lang['TRIPS_TEXT_6'] =

    'All our trips include a photographer and a local guide, to make them unforgettable experiences.';

$lang['TRIPS_TEXT_7'] =

    'Join us for our next adventure or let us know where or how your dream trip is and we’ll organize it for you.';

$lang['TRIPS_TEXT_8'] =

    'Have a nice trip!';

$lang['TRIPS_VIDEOS'] =

    "VIDEOS";

$lang['TRIPS_LAST_TEXT_TOP'] =

    "SURF, EAT, SLEEP...";

$lang['TRIPS_LAST_TEXT_BOTTOM'] =

    "¡REPEAT!";

$lang['TRIPS_INFO_1'] =

    "SEND US AN EMAIL FOR MORE INFORMATION";

$lang['TRIPS_INFO_2'] =

    "ACTIVIDADES@SURFHOUSEBARCELONA.COM";

/*
  ------------------
  SECRET SPOT
  ------------------
 */

$lang['SECRETSPOT_SHB'] =

     'SECRET SPOT';

$lang['SECRETSPOT_TEXT_1'] =

     'SHB';

$lang['SECRETSPOT_TEXT_2'] =

    '6 years after the birth of Surf House Barcelona, Surf House Secret Spot is born.';

$lang['SECRETSPOT_TEXT_3'] =

    'A cozy and charming corner, like that hidden secret beach you only share with a few.';

$lang['SECRETSPOT_TEXT_4'] =

    'Same values, same goals, same stoke and same feel. Not as close to the beach and waves, but same good vibes and quality on each of its details.';

$lang['SECRETSPOT_TEXT_5'] =

    'Sport activities in surrounding parks, weekly events, live surfing contest projections and many new and exciting things will be cooked in this new magic spot.';

$lang['SECRETSPOT_TEXT_6'] =

    'Brunch Menu';

$lang['SECRETSPOT_TEXT_7'] =

    'Dinner Menu';

$lang['SECRETSPOT_LAST_TEXT_TOP'] =

     'COME AND DISCOVER';

$lang['SECRETSPOT_LAST_TEXT_BOTTOM'] =

     'WHAT IT HIDES!';


/*
  ------------------
  SHOP
  ------------------
 */

$lang['SHB_SHOP'] =

    'SURF HOUSE SHOP';

$lang['SH_SHOP'] =

    'SH SHOP';

/*
  ------------------
  TEAM
  ------------------
 */
 $lang['ALEX_PONS_TEXT'] =

  'Alex went from competing alpine ski competitions to surfing and having one of the best styles of the Mediterranean Coast. He’s a Port Ginesta (Castelldefels) local and has envolved tremendously in the last years. If you got the chance to see him surfing live, you will definitely enjoy it.';

 $lang['JONY_GONZALEZ_TEXT'] =

  'Jonathan Gonzalez, known by his friends as Jony, is the current WSL European Champion. His surfing style (probably one of the best in the WSL) and his humble and calm personality make him an almost perfect surfer. He is a Tenerife Island local but is always traveling the world taking part in the WQS. He often has layovers in Barcelona and, when so, comes by SHB to have his favorite burger, a smoothie and a coffee. We couldn’t ask for a better surfer to be in our team.';


$lang['JAN_BEIN_TEXT'] =

  'Jan is only 14 years old and lives in Castelldefels. If there are waves (and he can get away from school and homweork) you’ll find him in the water ripping; if there are none, you’ll find him at any skatepark close to his home. His parents both surf too and he’s been lucky enough to travel and discover breaks all over the world. He also enjoys healthy food and spending time with family and friends.';

$lang['ADRIA_PERARNAU_TEXT'] =

  'Adria Perarnau started swimming at a very young age and his love for water made him try surfing, getting immediately hooked. The sea and surfing are more than a hobby for Adri, making him escape in search of waves every time he has a chance. ';

$lang['HELEN_SAIGI_TEXT'] =

  'A tattoo artist and a stylish surfer in her free time, Helena is one of the best surfer girls in Barcelona and surroundings. Being only 20 years old and surfing since she was 16, her dream is to live in a place loaded with perfect waves. Snowboarding is another of her favourite hobbies and she used to be a great skateboarder, but had to quit in order to take care of her hands so she could keep tattooing the beauful art pieces she does.';

$lang['ANDREA_LUELMO_TEXT'] =

  'Andrea has one of the best surfing styles of our coast. Always with a big smile on her face, she&#39;s a cool girl loved by everyone.

She teaches surf lessons at Pukas Surf Eskola and you can see her surfing the Barcelona beaches whenever there are waves. She travels often to Zarautz and Indonesia, and her favourite place out of the water is, obviously, Surf House Barcelona.';

$lang['MARTA_DAVILA_TEXT'] = 
  
  'A Skater and surfer from Sitges, Marta moved to Holland after finishing high school. She is a Data and Artificial Intelligence Engineer and works tor the European Space Agency, developing software for the Meteosat satellites.  When not working, she spends her free time looking for skateparks to skate and waves to surf. Marta is a total badass and you will for sure hear a lot from her in the near future!';

$lang['CLARIS_CARRASCO_TEXT'] =

  'Being half Korean and a Barceloneta local is pretty special. Claris is one of our favorite riders. She started surfing in Razo (Galicia) when she was 15 and hasn&#39;t stopped getting better since. You&#39;ll find her in the water or ripping at the skatepark whenever she&#39;s not studying. Her awesome skating plays a big roll in her surfing style, always trying some radical move in the water  and surprising everyone. We suspect she might be getting all her power thanks to our smoothies and juices.';

$lang['ALBERT_HERNANDEZ_TEXT'] =

  'Galicia’s Junior Champion in 2007, Albert is the first Catalan surfer that has won a circuit outside our territory. And doing so in a place with such good surfing like Galicia is astounding.  He’s back in the game after a couple years focusing mainly on work. Humble and the nicest guy.';

$lang['ALEX_KNOPFEL_TEXT'] =

  'His passion for surfing led him to create Surf House Barcelona, inspired in California, where he lived fora  a little bit. A bit of a workaholic and always thinking of new smoothies or tasty dishes,  he escapes to go surf whenever he has a spare minute, whether it’s in front of SHB or embarking on a road trip / boarding a plane to wherever waves are pumping.';

$lang['CARLES_MEDINA_TEXT'] =

  'Carles Medina, an innovative surfer and the creative mind behind the brand MBC, is known for his good vibe and big smile in and out of the water. The time he spent in Australia enriched his radical surfing and inspired him to bring back to his home town a project with a very bright future.';

$lang['PABLO_VIAPLANA_TEXT'] =

  'Le Cucut. That’ss our favorite DJ’s (and most special surfer) artistic name. When not in the water, we’ll find him spinning the funnest music at places like Surf House or clubs like Be Cool.  He’s not your regular DJ, the kid’s got something special. Stay tuned and you’ll see how far he gets. ';

  $lang['ELI_MELERO_TEXT'] =

  'Eli Melero is a restless (nomadic) globetrotter shredding the wind, waves, and mountains with a deep passion for travelling & exploring the world. I self taught myself how to photograph over the past couple of years, and am now lucky enough to be able to use this skill and be able to work with many different clients to create unique content around the world.';

/* FOOD */

$lang['PATATAS'] =

  'French<br/>Fries';

$lang['COSTILLAS_BBQ'] =

  'Pork Ribs';

$lang['HELADOS_SHB'] =

  'SHB<br/>Ice cream';

$lang['GOFRE_NUTELLA'] =

  'Nutella<br/>Waffle';

/* DRINK */

$lang['LILA'] =

  'Purple';

$lang['AGUA_DE_COCO'] =

  'Coconut<br/>Water';

$lang['TU_CREACION'] =

  'Your own<br/>creation';


/*
  ------------------
  SCHEDULE
  ------------------
 */


$lang['HORARIO'] =

 'SCHEDULE:';

$lang['MIERCOLES'] =

 'MONDAY AND TUESDAY: CLOSED';

$lang['VIERNES'] =

 'WEDNESDAY AND THURSDAY: 13h to 20h&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FRIDAY AND SATURDAY: 12h to 24h';

$lang['DOMINGO'] =

 'SUNDAY: 12h to 22h';

$lang['HORARIO_1'] =

 '<strong>Monday and Tuesday:</strong> Closed';

$lang['HORARIO_2'] =

 '<strong>Wednesday and Thursday:</strong> 12h to 18h';

$lang['HORARIO_3'] =

 '<strong>Friday and Saturday:</strong> 12h to 23h';

$lang['HORARIO_4'] =

 '<strong>Sunday:</strong> 12h to 22h';

$lang['HORARIO_5'] =

 '<strong>Monday:</strong> Closed';

$lang['HORARIO_6'] =

 '<strong>Tuesday to Friday:</strong> 13.30h to 16h and 19.30 to 24h';

$lang['HORARIO_7'] =

 '<strong>Friday:</strong> 10h to 1h';

$lang['HORARIO_8'] =

 '<strong>Saturday:</strong> 10h to 16h and  20h to 1h';

$lang['HORARIO_9'] =

 '<strong>Sunday:</strong> 10h to 17h';

?>
