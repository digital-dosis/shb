<?php require_once("admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Activities" />
<meta property="og:description" content="SURF HOUSE BARCELONA. We offer daily activities such as paddle surf, beach workouts to stay in shape, running, surfing championship broadcasts..." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-Activities.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/activities" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/activities.js"></script>

<title>SHB <?php echo $lang['ACTIVITIES'] ?></title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

    <div id="fsvs-body" class="activities-content">

        <div class="slide">
            <div class="img-wrap">
            </div>
            <img class="portada-title left-align" src="img/activities/title.png" srcset="img/activities/title.png 1600w, img/activities/title@2x.png 2560w" alt="FOOD & DRINKS" sizes="(max-width: 1280px) 1600px" >
            <div class="down">
                <a href="javascript:void('0');" id="down" class="blink">
                    <img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
                </a>

                <span></span>
            </div>
        </div>
        <div class="slide">
            <div class="center-wrapper animation-scale-1">

                <img src="img/activities/surfer.png" srcset="img/activities/surfer.png 1280w, img/activities/surfer@2x.png 2560w" sizes="(max-width: 1280px) 1600px" class="animation-DownToTop3 animation-delay-3">
                <h1 class="animation-DownToTop2 animation-delay-6">
                    <?php echo $lang['AC_TEXT']?>
                </h1>
                <div class="green-border"></div>

            </div>
        </div>
        <div class="slide">
            <h1><?php echo $lang['ACTIVITIES']?> <span><?php echo $lang['AUTUM_WINTER']?> 2016</span></h1>
            <div class="nav">
                <a href="javascript:void(0)" id="prev" class="prev blink animation-left animation-delay-8"><img src="img/fooddrinks/back.png"></a>
                <a href="javascript:void(0)" id="next" class="next blink animation-right animation-delay-8"><img src="img/fooddrinks/next.png"></a>
            </div>
            <div id="activities-carousel" class="dragdealer">
                <div class="handle animation-fade animation-delay-3" >
                    <div class="page">

                        <?php
                        $i=5;
                        $location = "";
                        if ( $lang['IDIOMA'] == 'es' ) {
                            $location = "_es";
                        }

                        foreach(collection("Actividades")->find()->sort(["orden"=>1]) as $item):

                            if ($i%2==0):
                                $classAnimation="DownToTop3";
                            else :
                                $classAnimation="topToDown3";
                            endif;
                            ?>
                        <div class="activitie <?php echo $item["Css_Class"]; ?> animation-<?php echo $classAnimation ?> animation-delay-<?php echo $i;?>" data-dayofweek="<?php echo $item["Dias_en_num"]; ?>">
                            <div></div>
                            <div>
                                <div>
                                    <div>
                                        <img src="img/activities/<?php echo $item["Css_Class"]; ?>-icon@2x.png" >
                                    </div>
                                    <?php
                                    if ( $item["Precio"] != -1) {
                                        ?>
                                        <div>
                                            <?php
                                            if ( $item["Precio"] != 0) {
                                                echo $item["Precio"];
                                                ?>
                                                €
                                                <?php
                                            } else {
                                                echo '<img sizes="(max-width: 1280px) 1600px" src="img/activities/free.png" srcset="img/activities/free.png 1600w, img/activities/free@2x.png 2560w">';
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div><div>
                                <div>
                                    <h1><?php echo $item["Nombre"]; ?></h1>
                                    <p><?php echo $item["Descripcion" . $location]; ?></p>
                                    <span></span>
                                    <h2><?php echo $item["Dias" . $location]; ?></h2>
                                </div>

                                <div class="scheduled">
                                    <img src="img/activities/clock@2x.png" >
                                    <?php
                                    if ($item["Hora_Inicio"] == "<p>-1</p>") {
                                        echo "<span>" . $lang['ASK_US_FOR_TIMES'] . "</span>";

                                    }
                                    else if($item["Hora_Inicio"] == "<p>-2</p>"){
                                        echo "<span>" . $lang['WHENEVER_YOU_LIKE'] . "</span>";
                                    }

                                    else {
                                      if($item["Hora_Final"]==""):
                                        echo "<span>" . $item["Hora_Inicio"] . " H";
                                      else :
                                      echo "<span>" . $item["Hora_Inicio"]." - ". $item["Hora_Final"]." H" . "</span>";
                                    endif;

                                    }
                                    ?>
                                </div>



                                <?php if ($item['booking'] == 1) { ?>
                                  <a href="mailto:<?php echo $lang['AC_LOC_MAIL']; ?>" class="booking blink">
                                    <img src="img/activities/mail@2x.png" >
                                    <span><?php echo $lang['RESERVE'];?></span>
                                  </a>
                                  <?php }else { }
                                ?>


                                <?php
                                if ($item["more_info"]) { ?>
                                  <a href="<?php echo $item["more_info"]; ?>" title="<?php echo $lang['MORE_INFO'];?>" target="_blank" class="more-info blink">
                                    <img src="img/activities/moreinfo@2x.png" >
                                    <span><?php echo $lang['MORE_INFO'];?></span>
                                  </a>
                                  <?php }else { }
                                ?>



                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                    endforeach;
                    ?>


                    </div>
                </div>
            </div>

            <a class="blink download-menu" href="/pdf/<?php echo $lang['PDF_ACTIVITIES']?>" target="_blank"><span class="animation-DownToTop animation-delay-10"><?php echo $lang['DOWNLOAD_PDF']?></span> <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/download-icon.png" srcset="img/fooddrinks/download-icon.png 1600w, img/fooddrinks/download-icon.png 2560w"  class="animation-DownToTop animation-delay-7"></a>
        </div>
        <div class="slide">
            <h1><?php echo $lang['SPECIAL_EVENTS']?></h1>

            <div id="special-carousel" class="dragdealer active">
                <div class="handle animation-fade animation-delay-3" >
                    <div class="page">
                        <div class="special-event animation-DownToTop2 animation-delay-4">
                            <img sizes="(max-width: 1280px) 1600px" src="img/activities/special-events/event-1.jpg" srcset="img/activities/special-events/event-1.jpg 1600w, img/activities/special-events/event-1@2x.jpg 2560w"><div>
                            <div>
                                <div>
                                    <span>06</span> OCTUBRE
                                    <span class="event-hour">20:00 H</span>
                                </div><div>
                                <div>
                                    <img sizes="(max-width: 1280px) 1600px" src="img/activities/special-events/special-event-icon.png" srcset="img/activities/special-events/special-event-icon.png 1600w, img/activities/special-events/special-event-icon@2x.png 2560w">
                                </div>
                                <div>VIP</div>
                            </div>
                        </div>
                        <div>
                            <h1>
                                PSYCHIC
                                MIGRATIONS
                            </h1>
                            <span class="event-line"></span>
                            <p>
                                Premier de la nueva película de surf de Volcom, Psychic Migrations de la mano de Cerveza Salada y Surf House Barcelona. Birras Gratis para todos los asistentes.
                            </p>
                            <a href="" class="blink">MORE INFO</a>
                        </div>
                    </div>
                </div><div class="special-event animation-DownToTop animation-delay-5">
                <img sizes="(max-width: 1280px) 1600px" src="img/activities/special-events/event-2.jpg" srcset="img/activities/special-events/event-2.jpg 1600w, img/activities/special-events/event-2@2x.jpg 2560w"><div>
                <div>
                    <div>
                        <span>24</span> OCTUBRE
                        <span class="event-hour">20:30 H</span>
                    </div><div>
                    <div>
                        <img sizes="(max-width: 1280px) 1600px" src="img/activities/special-events/special-event-icon.png" srcset="img/activities/special-events/special-event-icon.png 1600w, img/activities/special-events/special-event-icon@2x.png 2560w">
                    </div>
                    <div><img sizes="(max-width: 1280px) 1600px" src="img/activities/free.png" srcset="img/activities/free.png 1600w, img/activities/free@2x.png 2560w"></div>
                </div>
            </div>
            <div>
                <h1>
                    EL CIRUELO
                    POR EUROPA
                </h1>
                <span class="event-line"></span>
                <p>
                    Este sábado, premier del tour europeo de nuestro amigo @marioazurza en la escuela Pukas de Barcelona! Cervecitas Moritz y pica-pica SHB para todos!
                </p>
                <a href="" class="blink">MORE INFO</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="slide">
    <div class="center-wrapper animation-scale-1">
        <span class="line animation-DownToTop animation-delay-7"></span>
        <h1>
        <span class="top animation-DownToTop animation-delay-4"><?php echo $lang['AC_LAST_TEXT_TOP']?></span>
        <span class="bottom animation-DownToTop animation-delay-6"><?php echo $lang['AC_LAST_TEXT_BOTTOM']?></span>
        </h1>
        <div class="location animation-DownToTop animation-delay-7">

                    <p><?php echo $lang['AC_LOC_TEXT_TOP']?><br/>
                       <a href="mailto:<?php echo $lang['AC_LOC_MAIL']?>">
                          <span class="blink"><?php echo $lang['AC_LOC_MAIL']?></span>
                       </a>
                    </p>


            </div>
        </div>

        <ul class="menu-bottom">
            <li class="blink animation-DownToTop animation-delay-7">
                <a href="home/">
                    HOME
                </a>
            </li>
            <li class="blink animation-DownToTop animation-delay-8">
                <a href="fooddrinks/">
                    FOOD&amp;DRINKS
                </a>
            </li>
            <li class="blink animation-DownToTop animation-delay-9">
                <a href="activities/">
                    <?php echo $lang['ACTIVITIES'] ?>
                </a>
            </li>
            <li class="blink animation-DownToTop animation-delay-10">
                <a href="about/">
                    <?php echo $lang['ABOUT_SHB'] ?>
                </a>
            </li>
            <li class="blink animation-DownToTop animation-delay-10">
                <a href="http://www.surfhousebarcelona.com/onwheels/">
                    ON WHEELS
                </a>
            </li>
            <li class="blink animation-DownToTop animation-delay-12">
                <a href="http://surfhouse-shop.com/" target="_blank">
                    SH SHOP
                </a>
            </li>

            </ul>

            <a href="http://www.digitaldosis.com" target="_blank" class="blink firma">
                <span class="topline"></span>
                Digital Dosis
                <span>Web Design</span>
            </a>

        </div>

    </div>
    <style type="text/css">

        html.fsvs body.active-slide-1 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-1 #fsvs-pagination li.active > span > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-1 #fsvs-pagination li > span > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');

            var d = new Date();
            var n = d.getDay() - 1;
            //console.log(n);

            $('.activitie').each(function(index, el) {
                var dayOfWeek = $(el).attr('data-dayofweek');

                if ( dayOfWeek.length > 1) {
                    for (var i = 0; i < dayOfWeek.length; i++) {
                        if ( n == dayOfWeek[i] ) {
                            $(el).prepend('<span class="today-activity"><span class="text-today animation-fade animation-delay-14"><?php echo $lang['TODAY']?></span> <span class="line-today animation-line animation-delay-6"></span></span>');
                        }
                    }
                } else {
                    if ( n == dayOfWeek ) {
                        $(el).prepend('<span class="today-activity"><span class="text-today animation-fade animation-delay-14"><?php echo $lang['TODAY']?></span> <span class="line-today animation-line animation-delay-6"></span></span>');
                    }
                }
            });
        });
    </script>
</body>
</html>
