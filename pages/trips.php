<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head_all.php' ?>
<meta property="og:title" content="Surf House Barcelona | Trips" />
<meta property="og:description" content="SHB Trips is our own platform to create your perfect surf trip. We are surfers and traveling, meeting people and sharing experiences is our passion." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-Trips.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/trips" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/trips.js"></script>

<title><?php echo $lang['TRIPS_SHB'] ?></title>

</head>
<body id="fsvs-body" class="bg-trips">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

    <!-- LIGHTBOX -->
    <div class="lightbox js-lightbox">
      <div class="lightbox__overlay js-close-lightbox"></div>
      <div class="lightbox__content">
        <span class="lightbox__close js-close-lightbox">
          <img src="img/trips/cruz.svg" alt="" class="lightbox__icon">
        </span>
        <div class="lightbox__body">
          <video class="lightbox__video js-video" data-video="01" loop playsinline>
            <source data-src="media/trips-01.mp4" type="video/mp4">
          </video>
          <video class="lightbox__video js-video" data-video="02" loop playsinline>
            <source data-src="media/trips-02.mp4" type="video/mp4">
          </video>
          <video class="lightbox__video js-video" data-video="03" loop playsinline>
            <source data-src="media/trips-03.mp4" type="video/mp4">
          </video>
          <video class="lightbox__video js-video" data-video="04" loop playsinline>
            <source data-src="media/trips-04.mp4" type="video/mp4">
          </video>
        </div>
      </div>
    </div>

    <div id="fullpage" class="fullpage trips-content">



      <!-- COVER SECTION-->
      <div class="section slide" id="section0">
        <div id="portada">
          <div data-offset="2" class="poster-wrapper">
            <div class="poster">
              <div data-offset="3" class="layer-bg animation-topToDown3"></div>
              <div data-offset="10" class="layer-title portada-title">
                <img class="portada-title left-align" src="img/trips/title.png" srcset="img/trips/title.png 1600w, img/trips/title@2x.png 2560w" alt="ABOUT" sizes="(max-width: 1280px) 1600px">
              </div>
            </div>
          </div>
          <div class="down">
            <a href="javascript:void('0');" id="down" class="blink">
              <img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
            </a>
            <span></span>
          </div>
        </div>
      </div>




      <!-- INTRO SECTION-->

      <div class="section slide" id="section1">
        <div class="center-wrapper animation-scale-1">
          <div class="green-border"></div>
          <div>
            <div>
              <h1 class="animation-topToDown animation-delay-3">
                <?php echo $lang['TRIPS_TEXT_1'];?>
              </h1>
              <p class="animation-DownToTop animation-delay-4">
                <?php echo $lang['TRIPS_TEXT_2'];?>
                <br/><br/>
                <?php echo $lang['TRIPS_TEXT_3'];?>
                <br/><br/>
                <?php echo $lang['TRIPS_TEXT_4'];?>
              </p>
            </div>
            <p class="animation-topToDown4 animation-delay-3">
              <?php echo $lang['TRIPS_TEXT_5'];?>
              <br/><br/>
              <?php echo $lang['TRIPS_TEXT_6'];?>
              <br/><br/>
              <?php echo $lang['TRIPS_TEXT_7'];?>
              <br/><br/>
              <?php echo $lang['TRIPS_TEXT_8'];?>
            </p>
          </div>
        </div>
      </div>




      <div class="section slide" id="section2">

      </div>




      <div class="section slide" id="section3">
        <h2 class="trips-title">VIDEOS</h2>
        <div class="nav">
          <a href="javascript:void(0)" class="prev js-video-prev animation-left animation-delay-8"><img src="img/fooddrinks/back.png"></a>
          <a href="javascript:void(0)" class="next js-video-next animation-right animation-delay-8"><img src="img/fooddrinks/next.png"></a>
        </div>
        <div id="trips-carousel" class="dragdealer active">
          <div class="handle" >
            <div class="page">

              <div class="trip-video js-open-lightbox" data-video="01">
                <img src="img/trips/play.png" srcset="img/trips/play.png 1600w, img/trips/play@2x.png 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__icon">
                <img src="img/trips/01-video.jpg" srcset="img/trips/01-video.jpg 1600w, img/trips/01-video@2x.jpg 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__poster">
              </div>
              <div class="trip-video js-open-lightbox" data-video="02">
                <img src="img/trips/play.png" srcset="img/trips/play.png 1600w, img/trips/play@2x.png 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__icon">
                <img src="img/trips/02-video.jpg" srcset="img/trips/02-video.jpg 1600w, img/trips/02-video@2x.jpg 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__poster">
              </div>
              <div class="trip-video js-open-lightbox" data-video="03">
                <img src="img/trips/play.png" srcset="img/trips/play.png 1600w, img/trips/play@2x.png 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__icon">
                <img src="img/trips/03-video.jpg" srcset="img/trips/03-video.jpg 1600w, img/trips/03-video@2x.jpg 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__poster">
              </div>
              <div class="trip-video js-open-lightbox" data-video="04">
                <img src="img/trips/play.png" srcset="img/trips/play.png 1600w, img/trips/play@2x.png 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__icon">
                <img src="img/trips/04-video.jpg" srcset="img/trips/04-video.jpg 1600w, img/trips/04-video@2x.jpg 2560w" sizes="(max-width: 1280px) 1600px" alt="" class="trip-video__poster">
              </div>

             </div>
            </div>
          </div>
        </div>

          <div class="section slide slide-1" id="section4">
            <div class="center-wrapper animation-scale-1">
                <span class="line animation-DownToTop animation-delay-7"></span>
                <h1>
                <span class="top animation-DownToTop animation-delay-4"><?php echo $lang['TRIPS_LAST_TEXT_TOP']?></span>
                <span class="bottom animation-DownToTop animation-delay-6"><?php echo $lang['TRIPS_LAST_TEXT_BOTTOM']?></span>
                </h1>

                <div class="location animation-DownToTop animation-delay-7">
                    <a href="mailto:actividades@surfhousebarcelona.com" target="_blank" class="blink">
                        <p><?php echo $lang['TRIPS_INFO_1']; ?><br/>
                        <strong><?php echo $lang['TRIPS_INFO_2']; ?></strong></p>
                    </a>
                </div>
            </div>





            <!-- MENU -->
            <?php include('inc/footer/menu.php'); ?>

            <!-- CREDITS-->
            <?php include('inc/footer/credits.php'); ?>





        </div>

    </div>

    <style type="text/css">

        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
