<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head_all.php' ?>
<meta property="og:title" content="Surf House Barcelona | About" />
<meta property="og:description" content="Surf House Barcelona has its origins in California’s lifestyle and we want to give you something different to what you can find anywhere else in the city." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-About.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/about" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/about.js"></script>

<title><?php echo $lang['ABOUT_SHB'] ?></title>

</head>
<body id="fsvs-body" class="bg-about">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

    <div id="fullpage" class="fullpage about-content">



      <!-- COVER SECTION-->
      <div class="section slide" id="section0">
        <div id="portada">
          <div data-offset="2" class="poster-wrapper">
            <div class="poster">
              <div data-offset="3" class="layer-bg animation-topToDown3"></div>
              <div data-offset="10" class="layer-title portada-title">
                <img class="portada-title left-align" src="img/about/title.png" srcset="img/about/title.png 1600w, img/about/title@2x.png 2560w" alt="ABOUT" sizes="(max-width: 1280px) 1600px">
              </div>
            </div>
          </div>
          <div class="down">
            <a href="javascript:void('0');" id="down" class="blink">
              <img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
            </a>
            <span></span>
          </div>
        </div>
      </div>




      <!-- INTRO SECTION-->

      <div class="section slide" id="section1">
        <div class="center-wrapper animation-scale-1">
          <div class="green-border"></div>
          <div>
            <div>
              <h1 class="animation-topToDown animation-delay-3">
                <?php echo $lang['AB_TEXT_1']?>
              </h1>
              <p class="animation-DownToTop animation-delay-4">
                <?php echo $lang['AB_TEXT_2']?>
              </p>
            </div>
            <p class="animation-topToDown4 animation-delay-3 about-column-2">
              <?php echo $lang['AB_TEXT_3']?>
              <br/><br/>
              <?php echo $lang['AB_TEXT_4']?>
              <span class="video-link blink js-open-lightbox" data-video="01">
                <img src="img/about/play.svg" alt="" class="video-link__icon">
                <span class="video-link__content">
                  <?php echo $lang['AB_TEXT_5']?>
                </span>
              </span>
            </p>
          </div>
        </div>
      </div>




      <div class="section slide" id="section2">

      </div>




      <div class="section slide" id="section3">
        <h1 class="about">SHB <span>SURF TEAM</span></h1>
        <div class="nav">
          <a href="javascript:void(0)" id="prevTeam" class="prev animation-left animation-delay-8"><img src="img/fooddrinks/back.png"></a>
          <a href="javascript:void(0)" id="nextTeam" class="next animation-right animation-delay-8"><img src="img/fooddrinks/next.png"></a>
        </div>
        <div id="team-carousel" class="dragdealer active">
          <div class="handle" >
            <div class="page">



                        <a href="team/marta-davila" class="team-member animation-left animation-delay-3">
                            <div class="img-team marta">
                            <div class="foto"></div>
                            </div>
                            <p>Marta <b>Dávila</b>
                              <span class="team-line"></span>
                            </p>
                        </a>

                        <!-- <a href="team/helen-saigi" class="team-member animation-topToDown animation-delay-3">
			  				<p>Helen <b>Saigi</b>
				  				<span class="team-line"></span>
                            </p>
                            <div class="img-team helen-saigi"><div class="foto"></div></div>
                        </a> -->



                        <a href="team/alex-knopfel" class="team-member animation-topToDown animation-delay-3">
                          <p>
                                Alex <b>Knopfel</b>
                                <span class="team-line"></span>
                            </p>
                            <div class="img-team alex-knopfel"><div class="foto"></div></div>
                        </a>



                        <a href="team/jony-gonzalez" class="team-member animation-DownToTop animation-delay-3">
                            <div class="img-team jony-gonzalez"><div class="foto"></div></div>
                            <p>Jonathan <b>Gonzalez</b>
                  <span class="team-line"></span>
                            </p>
                        </a>

                       <a href="team/alex-pons" class="team-member animation-topToDown animation-delay-3">
                          <p>
                                Alex <b>Pons</b>
                                <span class="team-line"></span>
                            </p>
                            <div class="img-team alex-pons"><div class="foto"></div></div>
                        </a>


                       <a href="team/jan-bein" class="team-member animation-DownToTop animation-delay-3">
                           <div class="img-team jan-bein"><div class="foto"></div></div>
                            <p>

                                Jan <b>Bein</b>
                                <span class="team-line"></span>

                            </p>

                        </a>

					            	<a href="team/carles-medina" class="team-member animation-topToDown animation-delay-3">

                            <p>
                                Carles <b>Medina</b>
                                <span class="team-line"></span>
                            </p>
                            <div class="img-team carles-medina"><div class="foto"></div></div>
                        </a>

                        <!-- <a href="team/claris-carrasco" class="team-member animation-DownToDown animation-delay-3">
                        	<div class="img-team claris-carrasco"><div class="foto"></div></div>
                             <p>

                                Claris <b>Carrasco</b>
                                <span class="team-line"></span>

                            </p>

                        </a> -->

                        <a href="team/eli-melero" class="team-member animation-DownToTop animation-delay-3">
                             <div class="img-team eli-melero"><div class="foto"></div></div>
                             <p>
                                Eli <b>Melero</b>
                                <span class="team-line"></span>
                            </p>
                        </a>

                        <a href="team/adria-perarnau" class="team-member animation-topToDown animation-delay-3">
                        	
                        	<p>

                                Adria <b>Perarnau</b>
                                <span class="team-line"></span>

                            </p>
                            <div class="img-team adria-perarnau"><div class="foto"></div></div>
                        </a>

                        <span class="team-member"></span>

                    </div>
                </div>
            </div>
        </div>

          <div class="section slide slide-1" id="section4">
            <div class="center-wrapper animation-scale-1">
                <span class="line animation-DownToTop animation-delay-7"></span>
                <h1>
                <span class="top animation-DownToTop animation-delay-4"><?php echo $lang['AB_LAST_TEXT_TOP']?></span>
                <span class="bottom animation-DownToTop animation-delay-6"><?php echo $lang['AB_LAST_TEXT_BOTTOM']?></span>
                </h1>
                <!-- LOCATION -->
                <?php include('inc/footer/location.php'); ?>
            </div>





            <!-- MENU -->
            <?php include('inc/footer/menu.php'); ?>

            <!-- CREDITS-->
            <?php include('inc/footer/credits.php'); ?>





        </div>

    </div>
    <!-- LIGHTBOX -->
    <div class="lightbox js-lightbox">
      <div class="lightbox__overlay js-close-lightbox"></div>
      <div class="lightbox__content">
        <span class="lightbox__close js-close-lightbox">
          <img src="img/trips/cruz.svg" alt="" class="lightbox__icon">
        </span>
        <div class="lightbox__body">
          <div class="lightbox__video js-video" data-video="01">
            <iframe style="height:100%;width:100%" src="https://www.youtube.com/embed/WpS29THJrOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>

        </div>
      </div>
    </div>

    <style type="text/css">

        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
