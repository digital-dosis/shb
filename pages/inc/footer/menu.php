<ul class="menu-bottom">
  <li class="blink animation-DownToTop animation-delay-7">
    <a href="home/">HOME</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-8">
    <a href="fooddrinks/">FOOD&DRINKS</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-9">
    <a href="activities/"><?php echo $lang['ACTIVITIES'] ?></a>
  </li>
  <li class="blink animation-DownToTop animation-delay-10">
    <a href="about/"><?php echo $lang['ABOUT_SHB'] ?></a>
  </li>
  <li class="blink animation-DownToTop animation-delay-11">
    <a href="http://www.surfhousebarcelona.com/trips/">TRIPS</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-12">
    <a href="http://surfhouse-shop.com/" target="_blank">SHOP</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-13">
    <a href="secretspot/" target="_blank">SECRET SPOT</a>
  </li>
</ul>
