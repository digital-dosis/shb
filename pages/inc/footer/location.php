<div class="location animation-DownToTop animation-delay-7">
    <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank" class="blink">
        <img src="img/fooddrinks/map.png" srcset="img/fooddrinks/map.png 1600w, img/fooddrinks/map@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
        <p><b>C/ L’ALMIRALL AIXADA 22</b><br/>
        08003 BARCELONA <span>T. 932 507 023</span></p>
    </a>
</div>
