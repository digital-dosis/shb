<div class="location animation-DownToTop animation-delay-7">
    <a href="https://www.google.es/maps/place/Carrer+d'Amig%C3%B3,+53,+08021+Barcelona/@41.3959585,2.1412942,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498767158a267:0xcff4ce410346db54!8m2!3d41.3959585!4d2.1434882" target="_blank" class="blink">
        <img src="img/fooddrinks/map.png" srcset="img/fooddrinks/map.png 1600w, img/fooddrinks/map@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
        <p><b>C/ AMIGÓ 53 (ESQUINA MADRAZO)</b><br/>
        08021 BARCELONA <span>T. 930 313 135</span></p>
    </a>
</div>
