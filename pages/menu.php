<div class="overlay overlay-hugeinc">
    <a href="javascript:void('0');" class="overlay-close opacity-hover">Close</a>

    <div class="lang-switch menu-lang">
        <a href="en" <?php if ( $lang['IDIOMA'] == 'en' ) echo 'class="selected"'; ?>>ENG <span class="border"></span></a>
        <span>  /  </span>
        <a href="es" <?php if ( $lang['IDIOMA'] == 'es' ) echo 'class="selected"'; ?>>ESP <span class="border"></span></a>
    </div>

    <nav>
        <ul>
            <li>
                    <a href="home">
                        Home
                    </a>
                </li>
                <li>
                    <a href="fooddrinks">
                        Food & Drinks
                    </a>
                </li>
                <li>
                    <a href="activities">
                        <?php echo $lang['ACTIVITIES_LOWER'] ?>
                    </a>
                </li>
                <li>
                    <a href="about">
                        <?php echo $lang['ABOUT_SHB_LOWER'] ?>
                    </a>
                </li>
                <li>
                    <a href="http://www.surfhousebarcelona.com/trips/">
                        SHB Trips
                    </a>
                </li>
                <li>
                    <a href="http://surfhouse-shop.com/" target="_blank" class="block-popup-shop">
                        SHB Shop
                    </a>
                </li>
                <li>
                    <a href="secretspot" class="link-secretspot">
                        SHB <span>secret spot</span>
                    </a>
                </li>
        </ul>
    </nav>
    <div class="contact-info">
      <div class="contact-info__wrapper">

        <div class="contact-info__card">
          <h3 class="contact-info__title">
            <strong>SHB</strong> Barceloneta
          </h3>
          <h4 class="contact-info__address">
            C/ L’Almirall Aixada 22, 08001 Barcelona<br/>T. 932 507 023
          </h4>
          <ul class="contact-info__list">
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_1']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_2']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_3']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_4']; ?>
            </li>
          </ul>
        </div>

        <div class="contact-info__card">
          <h3 class="contact-info__title">
            <strong>SHB</strong> Secret Spot
          </h3>
          <h4 class="contact-info__address">
            C/ Amigó 53, 08021 Barcelona<br/>T. 930 313 135
          </h4>
          <ul class="contact-info__list">
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_5']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_6']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_8']; ?>
            </li>
            <li class="contact-info__item">
              <?php echo $lang['HORARIO_9']; ?>
            </li>
          </ul>
        </div>
        <nav class="contact-info__links">
          <a href="mailto:hello@surfhousebarcelona.com" class="contact-info__link contact-info__link--mail"></a>
          <a href="https://www.facebook.com/surfhousebarcelona" class="contact-info__link contact-info__link--facebook"></a>
          <a href="http://instagram.com/surfhousebarcelona" class="contact-info__link contact-info__link--instagram"></a>
        </nav>

      </div>

    </div>
    <div class="popup popup-home popup-home--shop">
      <div class="msg">
        <a href="javascript:void('0');" class="popup-close"></a>
        <a href="mailto:orders@surfhousebarcelona.com">
       
        <?php if ( $lang['IDIOMA'] == 'es' ) echo '<img src="img/home/SHB-online-shop-desktop2-CAST.png">';?>
        <?php if ( $lang['IDIOMA'] == 'en' ) echo '<img src="img/home/SHB-online-shop-desktop2-ENG.png">';?>
        </a>
      </div> 
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.popup-home--shop').click(function(){
      $('.popup').fadeOut();
    });

   $('.popup-close').click(function(){
    $('.popup').fadeOut();
    });

   $('.block-popup-shop').click(function(e){
      e.preventDefault();
      $('.popup-home--shop').addClass('is-visible').fadeIn();
   });
  });
</script>
