<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Marta Dávila</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-marta">

        <div class="">
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/marta-s.jpg 1600w, img/about/team/marta-s@2x.jpg 2560w" src="img/about/team/andrea-luelmo-s.jpg" alt="Marta Dávila">
                <p>
                    <b>Birth Date:</b> 24 02 1998<br/>
                    <b>Fav. Spot:</b> Soup Bowl, Barbados.<br/>
                    <b>SHB Smoothie:</b> Banana Power<br/>
                    <b>SHB Meal:</b> Rice Paper Rolls<br/>
                    <b>Surfing Maneuvre:</b>FS 50-50 grind<br/>
                    <b>Other interests:</b> Physics, Mathematics and Astrophysics.
                </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    <br/>
                    Marta Dávila
                </span>
                <span class="line-member"></span>
                <?php echo $lang['MARTA_DAVILA_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>