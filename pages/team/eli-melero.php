<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Eli Melero</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-eli-melero">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/eli-melero-s.jpg 1600w, img/about/team/eli-melero-s@2x.jpg 2560w" src="img/about/team/eli-melero-s.jpg" alt="Pablo Viaplana">
                <p>
                    <b>Birth Date:</b>22 09 1989<br/>
                    <b>Fav. Spot:</b> Sumbawa<br/>
                    <b>SHB Smoothie:</b> Detox<br/>
                    <b>SHB Meal:</b>Acai bowl<br/>
                    <b>Surfing Maneuvre:</b> Barrel Dreaming<br/>
                    <b>Other interests:</b> Traveling, piano, ski, snow, kitesurf.
                </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    Eli<br/>
                    Melero
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ELI_MELERO_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>