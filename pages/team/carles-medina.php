<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Carles Medina</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-carles-medina">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/carles-medina-s.jpg 1600w, img/about/team/carles-medina-s@2x.jpg 2560w" src="img/about/team/carles-medina-s.jpg" alt="Carles Medina">
                <p>
                    <b>Birth Date:</b> 29 09 1984<br/>
                    <b>Fav. Spot:</b> Broken Head, Byron Bay<br/>
                    <b>SHB Smoothie:</b> ENERGY +<br/>
                    <b>SHB Meal:</b> Nachos con guacamole<br/>
                    <b>Surfing Maneuvre:</b> Frontside air<br/>
                    <b>Other interests:</b> Architecture and photography
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    <br/>
                    Carles Medina
                </span>
                <span class="line-member"></span>
                <?php echo $lang['CARLES_MEDINA_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>