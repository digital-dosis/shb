<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Helen Saigi</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-helen-saigi">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/helen-saigi-s.jpg 1600w, img/about/team/helen-saigi-s@2x.jpg 2560w" src="img/about/team/helen-saigi-s.jpg" alt="Helen Saigi">
                <p>
                    <b>Birth Date:</b> 31 07 1997  <br/>
                    <b>Fav. Spot:</b> Balangan. Bali<br/>
                    <b>SHB Smoothie:</b> Oreo Milkshake<br/>
                    <b>SHB Meal:</b> Classic Burger & Rice Paper Rolls!<br/>
                    <b>Surfing Maneuvre:</b> Reentry Cutback<br/>
                    <b>Other interests:</b> Traveling. Tattoo, dance, snowboard, skate and wakeboard.
                </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    Helen Saigi
                </span>
                <span class="line-member"></span>
                <?php echo $lang['HELEN_SAIGI_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>