<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head.php' ?>

<title>SHB TEAM - Jonathan Gonzalez</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-jony-gonzalez">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/jony-gonzalez-s.jpg 1600w, img/about/team/jony-gonzalez-s@2x.jpg 2560w" src="img/about/team/jony-gonzalez-s.jpg" alt="Adria Perarnau">
                <p>
                    <b>Birth Date:</b> 22 08 1980<br/>
                    <b>Fav. Spot:</b> La Caleta de Adeje<br/>
                    <b>Surfing Maneuvre:</b> Tubos/aeros<br/>
                    <b>Other interests:</b> Swimming and snorkeling.
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    Jonathan<br> Gonzalez
                </span>
                <span class="line-member"></span>
                <?php echo $lang['JONY_GONZALEZ_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>