<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Alex Knopfel</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-alex-knopfel">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/alex-knopfel-s.jpg 1600w, img/about/team/alex-knopfel-s@2x.jpg 2560w" src="img/about/team/alex-knopfel-s.jpg" alt="Alex Knopfel">
                <p>
                    <b>Birth Date:</b> 14 12 1986<br/>
                    <b>Fav. Spot:</b> Punta Chipehua (MX)<br/>
                    <b>SHB Smoothie:</b> Ñoki <br/>
                    <b>SHB Meal:</b> Surfer's Burrito!<br/>
                    <b>Surfing Maneuvre:</b> Floater 360<br/>
                    <b>Other interests:</b> Business, Marketing, traveling, skiing and of course, do my best at my incredible job.
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    <br/>
                    Alex Knopfel
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ALEX_KNOPFEL_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>