<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Jan Bein</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-jan-bein">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/jan-bein-s.jpg 1600w, img/about/team/jan-bein-s@2x.jpg 2560w" src="img/about/team/jan-bein-s.jpg" alt="Jan Bein">
                <p>
                    <b>Birth Date:</b> 09 09 2002  <br/>
                    <b>Fav. Spot:</b> Banco de arena delante de casa en Castelldefels y Main Point en Arugam Bay, Sri Lanka<br/>
                    <b>Surfing Maneuvre:</b> Roundhouse Cutback<br/>
                    <b>Other interests:</b> Skate and ladies!
                 </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    Jan Bein
                </span>
                <span class="line-member"></span>
                <?php echo $lang['JAN_BEIN_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>