<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Claris Carrasco</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-claris-carrasco">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/claris-carrasco-s.jpg 1600w, img/about/team/claris-carrasco-s@2x.jpg 2560w" src="img/about/team/claris-carrasco-s.jpg" alt="Claris Carrasco">
                <p>
                    <b>Birth Date:</b> 18 02 1994<br/>
                    <b>Fav. Spot:</b> Rock Island (Philippines)<br/>
                    <b>SHB Smoothie:</b> Rojo<br/>
                    <b>SHB Meal:</b> Veggie burguer<br/>
                    <b>Surfing Maneuvre:</b> Cutback<br/>
                    <b>Other interests:</b> Viajar en busca de nuevos rincones y el deporte como terapia esencial.
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    Claris<br/>
                    Carrasco
                </span>
                <span class="line-member"></span>
                <?php echo $lang['CLARIS_CARRASCO_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>