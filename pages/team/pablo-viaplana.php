<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Pablo Viaplana</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-pablo-viaplana">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/pablo-viaplana-s.jpg 1600w, img/about/team/pablo-viaplana-s@2x.jpg 2560w" src="img/about/team/pablo-viaplana-s.jpg" alt="Pablo Viaplana">
                <p>
                    <b>Birth Date:</b> 17 09 1993<br/>
                    <b>Fav. Spot:</b> Home Spot<br/>
                    <b>SHB Smoothie:</b> Petit Suisse<br/>
                    <b>SHB Meal:</b> Chicken Joe<br/>
                    <b>Surfing Maneuvre:</b> Barrell<br/>
                    <b>Other interests:</b> El baile, el zumo de cevada, la noche, el funky-disco, pool skate…
                </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    Pablo<br/>
                    Viaplana
                </span>
                <span class="line-member"></span>
                <?php echo $lang['PABLO_VIAPLANA_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>