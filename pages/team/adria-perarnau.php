<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head.php' ?>

<title>SHB TEAM - Adria Perarnau</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-adria-perarnau">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/adria-perarnau-s.jpg 1600w, img/about/team/adria-perarnau-s@2x.jpg 2560w" src="img/about/team/adria-perarnau-s.jpg" alt="Adria Perarnau">
                <p>
                    <b>Birth Date:</b> 29 12 1992<br/>
                    <b>Fav. Spot:</b> Lakey Peak (Sumbawa)<br/>
                    <b>SHB Smoothie:</b> Ñoki<br/>
                    <b>SHB Meal:</b> Beach Nachitos<br/>
                    <b>Surfing Maneuvre:</b> Cut Back<br/>
                    <b>Other interests:</b> Photography, traveling, carving and enjoy all I do.
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    Adria<br> Perarnau
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ADRIA_PERARNAU_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>