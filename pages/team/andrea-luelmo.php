<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<title>SHB TEAM - Andrea Luelmo</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-andrea-luelmo">

        <div class="">
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/andrea-luelmo-s.jpg 1600w, img/about/team/andrea-luelmo-s@2x.jpg 2560w" src="img/about/team/andrea-luelmo-s.jpg" alt="Andrea Luelmo">
                <p>
                    <b>Birth Date:</b> 01 02 1990<br/>
                    <b>Fav. Spot:</b> Old man's (Canggu, Bali) // El cementerio (Sitges)<br/>
                    <b>SHB Smoothie:</b> Roger Rabbit<br/>
                    <b>SHB Meal:</b> Ensalada Alice Mango<br/>
                    <b>Surfing Maneuvre:</b>Floater<br/>
                    <b>Other interests:</b> El aguacate y los atardeceres de verano con sabor a sal en buena compañía
                </p>
            </div>
            <p class="animation-DownToTop animation-delay-5">
                <span>
                    <br/>
                    Andrea Luelmo
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ANDREA_LUELMO_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>