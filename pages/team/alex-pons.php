<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head.php' ?>

<title>SHB TEAM - Alex Pons</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-alex-pons">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/alex-pons-s.jpg 1600w, img/about/team/alex-pons-s@2x.jpg 2560w" src="img/about/team/alex-pons-s.jpg" alt="Alex Pons">
                <p>
                    <b>Birth Date:</b> 07 07 1995<br/>
                    <b>Fav. Spot:</b> Lafitenia<br/>
                    <b>Surfing Maneuvre:</b> Lay Back<br/>
                    <b>Other interests:</b> Surf, skate, animals, music, food and skiing.
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    Alex Pons
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ALEX_PONS_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>