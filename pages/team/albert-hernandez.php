<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head.php' ?>

<title>SHB TEAM - Albert Hernandez</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <a class="menu-button" href="about#4">BACK <span></span></a>

    <div class="team-member-wrapper bg-albert-hernandez">

        <div>
            <div class="animation-topToDown animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" srcset="img/about/team/albert-hernandez-s.jpg 1600w, img/about/team/albert-hernandez-s@2x.jpg 2560w" src="img/about/team/albert-hernandez-s.jpg" alt="Albert Hernandez">
                <p>
                    <b>Birth Date:</b> 28 07 1987<br/>
                    <b>Fav. Spot:</b> Killers<br/>
                    <b>SHB Smoothie:</b> SHB Detox (Strawberry, lemon and melon)<br/>
                    <b>SHB Meal:</b> Bingin mie goreng<br/>
                    <b>Surfing Maneuvre:</b> Barrels<br/>
                    <b>Other interests:</b> Family, friends, girlfriend, music, F. C. Barcelona...
                </p>
            </div><p class="animation-DownToTop animation-delay-5">
                <span>
                    Albert<br/>
                    Hernandez
                </span>
                <span class="line-member"></span>
                <?php echo $lang['ALBERT_HERNANDEZ_TEXT']; ?>
            </p>
        </div>

    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>