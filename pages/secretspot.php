<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head_all.php' ?>
<meta property="og:title" content="Surf House Barcelona | Secret Spot" />
<meta property="og:description" content="Surf House Secret Spot. Un rinc�n especial y acogedor, como esa playa tan rec�ndita y escondida que solo compartes con los tuyos." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-SecretSpot.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/secretspot" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/secretspot.js"></script>

<title><?php echo $lang['SECRETSPOT_SHB'] ?></title>

</head>
<body id="fsvs-body" class="bg-secretspot">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>


    <div id="fullpage" class="fullpage secretspot-content">



      <!-- COVER SECTION-->
      <div class="section slide" id="section0">
        <div id="portada">
          <div data-offset="2" class="poster-wrapper">
            <div class="poster">
              <div data-offset="3" class="layer-bg animation-topToDown3"></div>
              <div data-offset="10" class="layer-title portada-title">
                <img class="portada-title left-align" src="img/secretspot/title.png" srcset="img/secretspot/title.png 1600w, img/secretspot/title@2x.png 2560w" alt="ABOUT" sizes="(max-width: 1280px) 1600px">
              </div>
            </div>
          </div>
          <div class="down">
            <a href="javascript:void('0');" id="down" class="blink">
              <img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
            </a>
            <span></span>
          </div>
        </div>
      </div>




      <!-- INTRO SECTION-->

      <div class="section slide" id="section1">
        <div class="center-wrapper animation-scale-1">
          <div class="green-border"></div>
          <div>
            <div>
              <h1 class="animation-topToDown animation-delay-3 secretspot-logo">
                <?php echo $lang['SECRETSPOT_TEXT_1'];?>
              </h1>
              <p class="animation-DownToTop animation-delay-4">
                <?php echo $lang['SECRETSPOT_TEXT_2'];?>
                <br/><br/>
                <?php echo $lang['SECRETSPOT_TEXT_3'];?>
              </p>
            </div>
            <p class="animation-topToDown4 animation-delay-3 secretspot-column-2">
              <?php echo $lang['SECRETSPOT_TEXT_4'];?>
              <br/><br/>
              <?php echo $lang['SECRETSPOT_TEXT_5'];?>
              <br/><br/>
              <a href="/pdf/<?php echo $lang['PDF_MENU_2']; ?>" target="_blank" class="menu-link menu-link--secret blink">
                <img src="img/icon-menu.svg" alt="" class="menu-link__icon">
                <span class="menu-link__content">
                  <span class="menu-link__text">
                    <?php echo $lang['SECRETSPOT_TEXT_6']; ?>
                  </span>
                </span>
              </a>
              <a href="/pdf/<?php echo $lang['PDF_MENU_3']; ?>" target="_blank" class="menu-link menu-link--secret menu-link--secret-2 blink">
                <img src="img/icon-menu.svg" alt="" class="menu-link__icon">
                <span class="menu-link__content">
                  <span class="menu-link__text">
                    <?php echo $lang['SECRETSPOT_TEXT_7']; ?>
                  </span>
                </span>
              </a>
            </p>
          </div>
        </div>
      </div>




      <div class="section slide" id="section2">

      </div>




          <div class="section slide slide-1" id="section4">
            <div class="center-wrapper animation-scale-1">
                <span class="line animation-DownToTop animation-delay-7"></span>
                <h1>
                <span class="top animation-DownToTop animation-delay-4"><?php echo $lang['SECRETSPOT_LAST_TEXT_TOP']?></span>
                <span class="bottom animation-DownToTop animation-delay-6"><?php echo $lang['SECRETSPOT_LAST_TEXT_BOTTOM']?></span>
                </h1>
                <!-- LOCATION -->
                <?php include('inc/footer/location2.php'); ?>
            </div>






            <!-- MENU -->
            <?php include('inc/footer/menu.php'); ?>

            <!-- CREDITS-->
            <?php include('inc/footer/credits.php'); ?>





        </div>

    </div>

    <style type="text/css">

        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
