<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head_all.php' ?>

<meta property="og:title" content="Surf House Barcelona | Food & Drinks" />
<meta property="og:description" content='SURF HOUSE BARCELONA. Healthy “fast food” with the best quality.' />
<meta property="og:image" content='http://www.surfhousebarcelona.com/fb-SHB-Food.png' />
<meta property="og:url" content='http://www.surfhousebarcelona.com/fooddrinks' />
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/fooddrinks.js"></script>

<title>SHB FOOD & DRINKS</title>
</head>
<body id="fsvs-body" class="fooddrinks">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

    <div id="fullpage" class="fullpage fooddrinks-content">


      <!-- COVER SECTION-->
      <div class="section slide" id="section0">
        <div id="portada">
          <div data-offset="2" class="poster-wrapper">
            <div class="poster">
              <div>
                <div data-offset="5" class="layer-bg"></div>
              </div>
              <div data-offset="10" class="layer-title portada-title">
                <img class="portada-title layer-" src="img/fooddrinks/title.png" srcset="img/fooddrinks/title.png 1600w, img/fooddrinks/title@2x.png 2560w" alt="FOOD &amp; DRINKS" sizes="(max-width: 1280px) 1600px">
              </div>
            </div>
          </div>
          <div class="down">
            <a href="javascript:void('0');" id="down" class="blink">
              <img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
            </a>
            <span></span>
          </div>
        </div>
      </div>




      <!-- INTRO SECTION-->
      <div class="section slide" id="section1">
        <div class="center-wrapper animation-scale-1">
          <span class="line animation-scale-1"></span>
          <h1 class="animation-topToDown animation-delay-2"><?php echo $lang['INTRO_TEXT_FD'] ?></h1>
          <h2 class="animation-downToTop animation-delay-3"><?php echo $lang['INTRO_TEXT_2_FD'] ?></h2>
          <span class="line animation-scale-1"></span>
        </div>
      </div>






      <!-- FOOD SECTION-->
      <div class="section slide" id="section2">
        <h1>FOOD</h1>
        <div class="nav">
          <a href="javascript:void(0)" id="prev" class="prev animation-left animation-delay-8"><img src="img/fooddrinks/back.png"></a>
          <a href="javascript:void(0)" id="next" class="next animation-right animation-delay-8"><img src="img/fooddrinks/next.png"></a>
        </div>
        <div id="foods-carousel" class="dragdealer">
          <div class="handle">
            <div class="page grid">
              <span class="image portrait-img animation-topToDown animation-delay-3">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/1.jpg" srcset="img/fooddrinks/food/1.jpg 1600w, img/fooddrinks/food/1@2x.jpg 2560w">
                <div class="name">
                  <p><span>Infarto<b class="linia-hover"></b></span></p>
                </div>
              </span>
              <span class="image portrait-img second-img animation-DownToTop animation-delay-2">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/2.jpg" srcset="img/fooddrinks/food/2.jpg 1600w, img/fooddrinks/food/2@2x.jpg 2560w">
                <div class="name"><p><span>SHB Poke Tuna<b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div">
                <span class="image img-in-div animation-topToDown animation-delay-4">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/3.jpg" srcset="img/fooddrinks/food/3.jpg 1600w, img/fooddrinks/food/3@2x.jpg 2560w">
                  <div class="name"><p><span>Tropical<br>Goat<b class="linia-hover"></b></span></p></div>
                </span>
                <div>
                  <span class="image animation-DownToTop animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/4.jpg" srcset="img/fooddrinks/food/4.jpg 1600w, img/fooddrinks/food/4@2x.jpg 2560w">
                    <div class="name"><p><span>Surfer’s<br/>Burrito <b class="linia-hover"></b></span></p></div>
                  </span>
                  <span class="image animation-DownToTop animation-delay-4">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/5.jpg" srcset="img/fooddrinks/food/5.jpg 1600w, img/fooddrinks/food/5@2x.jpg 2560w">
                    <div class="name"><p><span>Beach<br/>Nachitos<b class="linia-hover"></b></span></p></div>
                  </span>
                </div>
              </div>
              <span class="image portrait-img animation-right animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/6.jpg" srcset="img/fooddrinks/food/6.jpg 1600w, img/fooddrinks/food/6@2x.jpg 2560w">
                <div class="name"><p><span>Veggie<br/>Burger <b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div portrait-img second-img">
                <span class="image img-in-div animation-right animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/7.jpg" srcset="img/fooddrinks/food/7.jpg 1600w, img/fooddrinks/food/7@2x.jpg 2560w">
                  <div class="name"><p><span><?php echo $lang['PATATAS'] ?> <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image img-in-div animation-left animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/8.jpg" srcset="img/fooddrinks/food/8.jpg 1600w, img/fooddrinks/food/8@2x.jpg 2560w">
                  <div class="name"><p><span>Avocado<br/>Toast<b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <div class="img-div">
                <span class="image img-in-div animation-topToDown animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/9.jpg" srcset="img/fooddrinks/food/9.jpg 1600w, img/fooddrinks/food/9@2x.jpg 2560w">
                  <div class="name"><p><span><?php echo $lang['COSTILLAS_BBQ'] ?> <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image animation-DownToTop animation-delay-2">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/10.jpg" srcset="img/fooddrinks/food/10.jpg 1600w, img/fooddrinks/food/10@2x.jpg 2560w">
                  <div class="name"><p><span>Uluwatu <b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <!-- start -->
              <span class="image portrait-img animation-topToDown animation-delay-3">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/15.jpg" srcset="img/fooddrinks/food/15.jpg 1600w, img/fooddrinks/food/15@2x.jpg 2560w">
                <div class="name">
                  <p><span>Pickle Salmon<b class="linia-hover"></b></span></p>
                </div>
              </span>
              <div class="img-div">
                <span class="image img-in-div animation-topToDown animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/16.jpg" srcset="img/fooddrinks/food/16.jpg 1600w, img/fooddrinks/food/16@2x.jpg 2560w">
                  <div class="name"><p><span>Todos Santos <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image animation-DownToTop animation-delay-2">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/17.jpg" srcset="img/fooddrinks/food/17.jpg 1600w, img/fooddrinks/food/17@2x.jpg 2560w">
                  <div class="name"><p><span>Rice Paper Rolls <b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <!-- end -->
              <span class="image portrait-img second-img animation-DownToTop animation-delay-2">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/11.jpg" srcset="img/fooddrinks/food/11.jpg 1600w, img/fooddrinks/food/11@2x.jpg 2560w">
                <div class="name"><p><span><?php echo $lang['HELADOS_SHB'] ?><b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div">
                <span class="image img-in-div animation-topToDown animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px"  src="img/fooddrinks/food/12.jpg" srcset="img/fooddrinks/food/12.jpg 1600w, img/fooddrinks/food/12@2x.jpg 2560w">
                  <div class="name"><p><span><?php echo $lang['GOFRE_NUTELLA'] ?> <b class="linia-hover"></b></span></p></div>
                </span>
                <div>
                  <span class="image animation-right  animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/13.jpg" srcset="img/fooddrinks/food/13.jpg 1600w, img/fooddrinks/food/13@2x.jpg 2560w">
                    <div class="name"><p><span>Açai<br/>Bowl <b class="linia-hover"></b></span></p></div>
                  </span>
                  <span class="image animation-DownToTop animation-delay-4">
                    <img src="img/fooddrinks/food/14.jpg" srcset="img/fooddrinks/food/14.jpg 1600w, img/fooddrinks/food/14@2x.jpg 2560w">
                    <div class="name"><p><span>Cheesecake <b class="linia-hover"></b></span></p></div>
                  </span>
                </div>
              </div>
              <span class="image portrait-img animation-topToDown animation-delay-3">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/18.jpg" srcset="img/fooddrinks/food/18.jpg 1600w, img/fooddrinks/food/18@2x.jpg 2560w">
                <div class="name">
                  <p><span>Cenningan's<br/>Yoghurt <b class="linia-hover"></b></span></p>
                </div>
              </span>
              <div class="img-div portrait-img second-img">
                <span class="image img-in-div animation-right animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/19.jpg" srcset="img/fooddrinks/food/19.jpg 1600w, img/fooddrinks/food/19@2x.jpg 2560w">
                  <div class="name"><p><span>Pancakes <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image img-in-div animation-left animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/food/20.jpg" srcset="img/fooddrinks/food/20.jpg 1600w, img/fooddrinks/food/20@2x.jpg 2560w">
                  <div class="name"><p><span>SHB FRENCH<br/>TOAST <b class="linia-hover"></b></span></p></div>
                </span>
              </div>
            </div>
          </div>
        </div>

        <a href="/pdf/<?php echo $lang['PDF_MENU']; ?>" target="_blank" class="menu-link blink animation-DownToTop animation-delay-6">
          <img src="img/icon-menu.svg" alt="" class="menu-link__icon">
          <div class="menu-link__content">
            <span class="menu-link__text">
              <?php echo $lang['CARTA']; ?>
            </span>
            <span class="menu-link__text">
              <strong>SHB</strong> Barceloneta
            </span>
          </div>
        </a>
        <a href="/pdf/<?php echo $lang['PDF_MENU_2']; ?>" target="_blank" class="menu-link menu-link--alt blink animation-DownToTop animation-delay-7">
          <img src="img/icon-menu.svg" alt="" class="menu-link__icon">
          <div class="menu-link__content">
            <span class="menu-link__text">
              <?php echo $lang['CARTA']; ?>
            </span>
            <span class="menu-link__text">
              <strong>SHB</strong> Secret Spot
            </span>
          </div>
        </a>

      </div>









      <!-- DRINKS -->
      <div class="section slide" id="section3">
        <h1>DRINKS</h1>
        <div class="nav">
          <a href="javascript:void(0)" id="prevDrinks" class="prev animation-left animation-delay-8"><img src="img/fooddrinks/back.png"></a>
          <a href="javascript:void(0)" id="nextDrinks" class="next animation-right animation-delay-8"><img src="img/fooddrinks/next.png"></a>
        </div>
        <div id="drinks-carousel" class="dragdealer">
          <div class="handle" >
            <div class="page">
              <span class="image portrait-img second-img animation-DownToTop animation-delay-2">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/1.jpg" srcset="img/fooddrinks/drinks/1.jpg 1600w, img/fooddrinks/drinks/1@2x.jpg 2560w">
                <div class="name"><p><span>Galáctico<br/>SHB <b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div">
                <div>
                  <span class="image animation-topToDown animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/2.jpg" srcset="img/fooddrinks/drinks/2.jpg 1600w, img/fooddrinks/drinks/2@2x.jpg 2560w">
                    <div class="name"><p><span>Fernando<br/>de Noronha <b class="linia-hover"></b></span></p></div>
                  </span>
                  <span class="image animation-DownToTop animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/3.jpg" srcset="img/fooddrinks/drinks/3.jpg 1600w, img/fooddrinks/drinks/3@2x.jpg 2560w">
                    <div class="name"><p><span>Classic<br/>Mojito <b class="linia-hover"></b></span></p></div>
                  </span>
                </div>
                <span class="image img-in-div animation-right animation-delay-2">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/4.jpg" srcset="img/fooddrinks/drinks/4.jpg 1600w, img/fooddrinks/drinks/4@2x.jpg 2560w">
                  <div class="name"><p><span>Petite Suisse<b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <div class="img-div portrait-img">
                <span class="image img-in-div animation-topToDown animation-delay-4">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/5.jpg" srcset="img/fooddrinks/drinks/5.jpg 1600w, img/fooddrinks/drinks/5@2x.jpg 2560w">
                  <div class="name"><p><span>Fresa<br/>Loca <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image img-in-div animation-DownToTop animation-delay-2">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/6.jpg" srcset="img/fooddrinks/drinks/6.jpg 1600w, img/fooddrinks/drinks/6@2x.jpg 2560w">
                  <div class="name"><p><span>Pink<br/>Lemonade <b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <span class="image portrait-img second-img animation-right animation-delay-3">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/7.jpg" srcset="img/fooddrinks/drinks/7.jpg 1600w, img/fooddrinks/drinks/7@2x.jpg 2560w">
                <div class="name"><p><span>Petite<br/>Suisse <b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div second-img">
                <span class="image  img-in-div animation-topToDown animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/8.jpg" srcset="img/fooddrinks/drinks/8.jpg 1600w, img/fooddrinks/drinks/8@2x.jpg 2560w">
                  <div class="name"><p><span>Roger Rabbit <b class="linia-hover"></b></span></p></div>
                </span>
                <span class="image animation-DownToTop animation-delay-3">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/9.jpg" srcset="img/fooddrinks/drinks/9.jpg 1600w, img/fooddrinks/drinks/9@2x.jpg 2560w">
                  <div class="name"><p><span>Japan Mary<b class="linia-hover"></b></span></p></div>
                </span>
              </div>
              <span class="image portrait-img second-img animation-DownToTop animation-delay-4">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/10.jpg" srcset="img/fooddrinks/drinks/10.jpg 1600w, img/fooddrinks/drinks/10@2x.jpg 2560w">
                <div class="name"><p><span><?php echo $lang['LILA'] ?> <b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div">
                <span class="image img-in-div animation-topToDown animation-delay-4">
                  <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/11.jpg" srcset="img/fooddrinks/drinks/11.jpg 1600w, img/fooddrinks/drinks/11@2x.jpg 2560w">
                  <div class="name"><p><span>Ice Tea SHB <b class="linia-hover"></b></span></p></div>
                </span>
                <div>
                  <span class="image animation-DownToTop animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/12.jpg" srcset="img/fooddrinks/drinks/12.jpg 1600w, img/fooddrinks/drinks/12@2x.jpg 2560w">
                    <div class="name"><p><span>Ñoki <b class="linia-hover"></b></span></p></div>
                  </span>
                  <span class="image animation-topToDown animation-delay-2">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/13.jpg" srcset="img/fooddrinks/drinks/13.jpg 1600w, img/fooddrinks/drinks/13@2x.jpg 2560w">
                    <div class="name"><p><span>Vainilla <b class="linia-hover"></b></span></p></div>
                  </span>
                </div>
              </div>
              <span class="image portrait-img animation-DownToTop animation-delay-2">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/14.jpg" srcset="img/fooddrinks/drinks/14.jpg 1600w, img/fooddrinks/drinks/14@2x.jpg 2560w">
                <div class="name"><p><span>Popeye<br/>& Brutus <b class="linia-hover"></b></span></p></div>
              </span>
              <span class="image portrait-img second-img animation-topToDown animation-delay-3">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/15.jpg" srcset="img/fooddrinks/drinks/15.jpg 1600w, img/fooddrinks/drinks/15@2x.jpg 2560w">
                <div class="name"><p><span>Tropical <b class="linia-hover"></b></span></p></div>
              </span>
              <div class="img-div">
                <div>
                  <span class="image animation-topToDown animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/16.jpg" srcset="img/fooddrinks/drinks/16.jpg 1600w, img/fooddrinks/drinks/16@2x.jpg 2560w">
                    <div class="name"><p><span><?php echo $lang['AGUA_DE_COCO'] ?> <b class="linia-hover"></b></span></p></div>
                  </span>
                  <span class="image animation-left animation-delay-3">
                    <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/17.jpg" srcset="img/fooddrinks/drinks/17.jpg 1600w, img/fooddrinks/drinks/17@2x.jpg 2560w">
                    <div class="name"><p><span>Oreo<br/>Milkshake <b class="linia-hover"></b></span></p></div>
                  </span>
                </div>
                <span class="image img-in-div animation-right animation-delay-4">
                  <img sizes="(max-width: 1280px) 1600px"  src="img/fooddrinks/drinks/18.jpg" srcset="img/fooddrinks/drinks/18.jpg 1600w, img/fooddrinks/drinks/18@2x.jpg 2560w">
                  <div class="name">
                    <p><span><?php echo $lang['TU_CREACION'] ?><b class="linia-hover"></b></span></p>
                  </div>
                </span>
              </div>
              <span class="image portrait-img animation-DownToTop animation-delay-2">
                <img sizes="(max-width: 1280px) 1600px" src="img/fooddrinks/drinks/19.jpg" srcset="img/fooddrinks/drinks/19.jpg 1600w, img/fooddrinks/drinks/19@2x.jpg 2560w">
                <div class="name"><p><span>Dragon<br/>Fruit <b class="linia-hover"></b></span></p></div>
              </span>
            </div>
          </div>
        </div>

        <a class="download-menu blink" href="/pdf/<?php echo $lang['PDF_BEBIDAS'] ?>" target="_blank"><span class="animation-DownToTop animation-delay-10"><?php echo $lang['DOWNLOAD_MENU'] ?></span> <img src="img/fooddrinks/download-icon.png" class="animation-DownToTop animation-delay-7"></a>

      </div>




      <!-- CONTACT SECTION-->

      <div class="section slide" id="section4">
        <div class="center-wrapper animation-scale-1">
          <span class="line animation-DownToTop animation-delay-5"></span>
          <h1>
            <span class="top animation-DownToTop animation-delay-4"><?php echo $lang['FD_LAST_TEXT_TOP'] ?></span>
            <span class="bottom animation-DownToTop animation-delay-5"><?php echo $lang['FD_LAST_TEXT_BOTTOM'] ?></span>
          </h1>
          <!-- LOCATION -->
          <?php include('inc/footer/location.php'); ?>
        </div>





        <!-- MENU -->
        <?php include('inc/footer/menu.php'); ?>

        <!-- CREDITS-->
        <?php include('inc/footer/credits.php'); ?>




      </div>


    </div>



    <style type="text/css">

        html.fsvs body.active-slide-1 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-1 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-1 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>



</body>
</html>
