<?php require_once("admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">
<?php include 'head_home.php' ?>
<meta property="og:title" content="Surf House Barcelona" />
<meta property="og:description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. 'Fast food' pero 'Healthy' y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/" />
<title>SHB</title>
<script type="text/javascript" src="js/vendor/openWeather.js"></script>
<script type="text/javascript" src="assets/swiper/js/swiper.min.js"></script>
<link rel="stylesheet" href="assets/swiper/css/swiper.min.css">
</head>

<body>
    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation" href="javascript:void('0');">MENU <span></span></a>

<!--Pop up -->
    <div class="popup popup-home popup-home--closed is-visible">
        <div class="msg">
            <a href="javascript:void('0');" class="popup-close">

            </a>
            <a href="http://surfhousebarcelona.com/pdf/SHB-BS-Carta.pdf">
                <img src="img/home/delivery-desktop.png">
            </a>
        </div>
    </div>
	<div class="popup popup-home popup-home--shop">
		<div class="msg">
			<a href="javascript:void('0');" class="popup-close">

			</a>
            <a href="mailto:orders@surfhousebarcelona.com">
            
            <?php if ( $lang['IDIOMA'] == 'es' ) echo '<img src="img/home/SHB-online-shop-desktop2-CAST.png">';?>
            <?php if ( $lang['IDIOMA'] == 'en' ) echo '<img src="img/home/SHB-online-shop-desktop2-ENG.png">';?>
            </a>
        </div> 
	</div>
   <!-- Hollidays 
      <?php if ( $lang['IDIOMA'] == 'es' ) echo '<p>¡ESTAMOS DE VACACIONES! <br/>VOLVEMOS EL 1 DE FEBRERO</p>';?>
      <?php if ( $lang['IDIOMA'] == 'en' ) echo '<p>WE ARE ON HOLIDAYS! <br/>WE COME BACK ON FEBRUARY 1ST</p>';?>
      -->
<!-- cierre popup-->

    <div id="fsvs-body" class="home-content">
        <div class="slide active-slide slide-0">
            <div class="img-wrap">
            </div>
            <img class="logo" src="img/home/logo.png" srcset="img/home/logo.png 1600w, img/home/logo.png 2560w" sizes="(max-width: 1280px) 1600px">

            <div class="lang-switch">
                <a href="en" <?php if ( $lang['IDIOMA'] == 'en' ) echo 'class="selected"'; ?>>ENG<span class="border"></span></a>
                <span>  /  </span>
                <a href="es" <?php if ( $lang['IDIOMA'] == 'es' ) echo 'class="selected"'; ?>>ESP<span class="border"></span></a>
            </div>

            <div class="tiles">
                <div class="fd">
                    <a href="fooddrinks" class="has-hover">
                        <div class="foto"></div>
                        <span class="hover-link">

                            <img src="img/home/fooddrinks-rollover.png" srcset="img/home/fooddrinks-rollover.png 1600w, img/home/fooddrinks-rollover@2x.png 2560w" sizes="(max-width: 1280px) 1600px" class="animation-topToDown3">

                            <p class="animation-DownToTop animation-delay-1"><?php echo $lang['VIEW_OUR_MENU'] ?></p>
                            <span class="animation-DownToTop animation-delay-3"></span>

                        </span>
                    </a>
                    <p class="tile-title">
                        FOOD&DRINKS
                        <span></span>
                    </p>
                </div>
                <div class="act">
                    <p class="tile-title align-right">
                        <span></span>
                        <?php echo $lang['ACTIVITIES'] ?>
                    </p>
                    <a href="activities" class="has-hover">
                        <div class="foto"></div>
                        <span class="hover-link">

                            <?php
                                date_default_timezone_set('Europe/Madrid');
                                $jd=cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
                                $day = jddayofweek($jd,1);
                                $day = strtoupper($day);

                                $dias_cerrado = collection("Dias Cerrado")->find()->toArray();
                                $cerrado = false;
                                for ($i=0; $i < count($dias_cerrado); $i++) {
                                    if ($dias_cerrado[$i]["Dia"] == $day) {
                                        $cerrado = true;
                                    }
                                }

                                if ($cerrado) {

                                } else {
                                    $item = collection("Actividades")->findOne(["Destacado"=>true, "featured_home"=>jddayofweek($jd,1)]);
                            ?>
                                    <img src="img/home/<?php echo $item["Css_Class"]; ?>.png" class="animation-topToDown3">
                                    <h2 class="animation-topToDown"><?php echo $lang['HIGHLIGHTED_ACTIVITY'] ?></h2>
                                    <h1 class="animation-topToDown"><?php echo $item["Nombre"]; ?></h1>

                                  <?php if($item["Hora_Inicio"] == "<p>-2</p>"){
                                      echo "<h2 class='animation-DownToTop animation-delay-2'>" . $lang['WHENEVER_YOU_LIKE'] . "</h2>";
                                  }else { ?>
                                  <h2 class="animation-topToDown"><?php echo $lang['TODAY_AT'] ?> <?php echo $item["Hora_Inicio"]. " H"; ?></h2>
                                <?php } ?>



                              <?php
                                  }
                                  ?>

                            <p class="animation-DownToTop animation-delay-1"><?php echo $lang['VIEW_ALL_ACTIVITIES'] ?></p>
                            <span class="animation-DownToTop animation-delay-3"></span>

                        </span>
                    </a>
                </div>
                <div class="wrap-tiles">
                  <div class="wh">
                      <p class="tile-title align-right">
                          <span></span>
                          SHB TRIPS
                      </p>
                      <a href="http://www.surfhousebarcelona.com/trips/" class="has-hover">
                          <div class="foto"></div>
                          <span class="hover-link">

              <!--<h2 class="animation-topToDown"><?php echo $lang['NEXT_EVENT'] ?></h2>
                              <h1 class="animation-topToDown"><?php echo $lang['NEXT_EVENT_NAME'] ?></h1>
                              <h2 class="animation-DownToTop"><?php echo $lang['NEXT_EVENT_DATE'] ?></h2>-->

                              <p class="animation-DownToTop animation-delay-1"><?php echo $lang['VIEW_ALL_EVENTS'] ?></p>
                              <span class="animation-DownToTop animation-delay-3"></span>

                          </span>
                      </a>
                  </div>
                  <div class="spot is-new">
                    <span class="new"></span>
                      <p class="tile-title align-right">
                          <span></span>
                          SHB SECRET SPOT
                      </p>
                      <a href="secretspot" class="has-hover">
                          <div class="foto"></div>
                          <span class="hover-link">

                              <p class="animation-DownToTop animation-delay-1"><?php echo $lang['MORE_INFO'] ?></p>
                              <span class="animation-DownToTop animation-delay-3"></span>

                          </span>
                      </a>
                  </div>
                    <div class="wr block-popup-shop-home">
                        <a href="http://surfhouse-shop.com/" target="_blank" class="has-hover">
                            <div class="foto"></div>
                            <span class="hover-link">
                              <img src="img/home/btn-external.png" srcset="img/home/btn-external.png 1600w, img/home/btn-external@2x.png 2560w" sizes="(max-width: 1280px) 1600px" class="external-link">
                              <img src="img/home/about-rollover.png" srcset="img/home/about-rollover.png 1600w, img/home/about-rollover@2x.png 2560w" sizes="(max-width: 1280px) 1600px" class="animation-topToDown">
                              <p class="animation-topToDown animation-delay-1"><?php echo $lang['SHB_SHOP'] ?></p>
                                <span class="animation-DownToTop animation-delay-3"></span>
                            </span>
                        </a>
                        <p class="tile-title">
                            <?php echo $lang['SH_SHOP'] ?>
                            <span></span>
                        </p>
                    </div>
                    <div class="wrap-info-tiles">
                        <div class="locat">


                          <div class="swiper-container">

                              <div class="swiper-wrapper">

                                  <div class="swiper-slide">

                                    <div class="blink">
                                        <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank">
                                            <img src="img/home/locat.png" srcset="img/home/locat.png 1600w, img/home/locat.png 2560w" sizes="(max-width: 1280px) 1600px">
                                            <div>
                                                <p>
                                                    <strong class="shb-strong">SHB</strong> Barceloneta<br/>
                                                    <strong>C/ L’Almirall Aixada 22<br/>
                                                    08001 Barcelona</strong><br/>
                                                </p>
                                                <span>T. 932 507 023</span>
                                            </div>
                                        </a>
                                    </div>

                                  </div>
                                  <div class="swiper-slide">

                                    <div class="blink">
                                        <a href="https://www.google.es/maps/place/Carrer+d'Amig%C3%B3,+53,+08021+Barcelona/@41.3959585,2.1412942,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498767158a267:0xcff4ce410346db54!8m2!3d41.3959585!4d2.1434882" target="_blank">
                                            <img src="img/home/locat.png" srcset="img/home/locat.png 1600w, img/home/locat.png 2560w" sizes="(max-width: 1280px) 1600px">
                                            <div>
                                                <p>
                                                    <strong class="shb-strong">SHB</strong> Secret Spot<br/>
                                                    <strong>C/ Amigó 53<br/>
                                                    08021 Barcelona</strong><br/>
                                                </p>
                                                <span>T. 930 313 135</span>
                                            </div>
                                        </a>
                                    </div>

                                  </div>
                              </div>

                              <div class="swiper-pagination"></div>
                          </div>


                        </div>
                        <div class="weather">
                           <!--  <a href="https://surfhouse-es.mydelivery.io" target="_blank"><img src="img/home/shb-delivery-icon.png" class="delivery-icon" alt="Delivery Icon" /></a> -->
                        <div>
                            <img src="" class="weather-icon" alt="Weather Icon" />
                            <br/>
                            <span><h2><strong class="weather-temperature"></strong>&nbsp;<?php echo $lang['NOW_IN']?><br/> BARCELONETA</span></h2></span>
                        </div>
                       <!--- <div>
                            <img src="http://surfhousebarcelona.com/img/weather/day/clear.gif" class="weather-icon" alt="Weather Icon" />
                            <br/>
                            <span><h2><strong class="weather-temperature"></strong>&nbsp;<?php echo $lang['NOW_IN']?><br/> BARCELONETA</span></h2></span>
                        </div>-->
                        </div>

                        <div class="social">
                            <div class="fb blink">
                                <a href="https://www.facebook.com/surfhousebarcelona" target="_blank">
                                    <img src="img/home/fb.png"
                                        srcset="img/home/fb.png 10w,
                                                img/home/fb@2x.png 20w"
                                        sizes="10px">
                                </a>
                            </div>
                            <div class="ig blink">
                                <a href="https://www.instagram.com/surfhousebarcelona/" target="_blank">
                                    <img src="img/home/ig.png" srcset="img/home/ig.png 1600w, img/home/ig@2x.png 2560w" sizes="(max-width: 1280px) 1600px, (min-width: 1600px) 2560px">
                                </a>
                            </div>
                            <div class="mail blink">
                                <a href="mailto:hello@surfhousebarcelona.com">
                                    <img src="img/home/mail.png" srcset="img/home/mail.png 1600w, img/home/mail@x2.png 2560w" sizes="(max-width: 1280px) 1600px" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- CREDITS-->
    <?php include('inc/footer/credits.php'); ?>

    <script type="text/javascript" src="js/menu.js"></script>

    <script type="text/javascript">

        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
            $('.popup-home--closed').addClass('is-visible');
        });


        $(document).ready(function() {
          setTimeout(function() {
            var homeSwiper = new Swiper('.swiper-container', {
              loop: true,
              autoplay: {
                delay: 4000,
              },
              pagination: {
                el: '.swiper-pagination',
              },
            });
          }, 3000);

          $('.popup-home--shop').click(function(){ //popup-shop-home
            $(this).fadeOut();
          });

         $('.popup-close').click(function(){
            $(this).parents('.popup').fadeOut();
            //$('.popup-home--shop').fadeOut();
          });

         $('.block-popup-shop-home').click(function(e){
            e.preventDefault();
            $('.popup-home--shop').addClass('is-visible').fadeIn();
         });

          /*$('.home-content > div:first-of-type').mousemove(function(e){
            var x = -(e.pageX + this.offsetLeft) / 55;
            var y = -(e.pageY + this.offsetTop) / 55;
            $(this).css('background-position', x + 'px ' + y + 'px');
          });*/


            $('.weather-temperature').openWeather({
                key: '9a70cfbbe140df39cd030991c75c6900',
                //lat: 41.3818,
                //lng: 2.1685,
                lat: 41.666672,
                lng: 2,
                descriptionTarget: '.weather-description',
                windSpeedTarget: '.weather-wind-speed',
                minTemperatureTarget: '.weather-min-temperature',
                maxTemperatureTarget: '.weather-max-temperature',
                humidityTarget: '.weather-humidity',
                sunriseTarget: '.weather-sunrise',
                sunsetTarget: '.weather-sunset',
                placeTarget: '.weather-place',
                iconTarget: '.weather-icon',
                customIcons: '../img/weather/',
                success: function() {
                    $('.weather').show();
                },
                error: function(message) {}
            });

        });

    </script>
