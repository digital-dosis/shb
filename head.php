<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name = "viewport" content = "width=device-width, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. “Fast food” pero “Healthy” y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. " />
    <meta name="keywords" content="Surf House Barcelona, smoothies, milkshakes, betidos, sandwich, hamburguesa, burger, fitness, health, Barceloneta, playa, restaurant, cafetería, frappe, cocktail, desayunos, comida, beach, ensaladas, food." />

    <base href="/">

    <link rel="apple-touch-icon-precomposed" href="apple-touch-SHB.jpg" />
    <link rel="shortcut icon" href="img/shb-favicon.png" />

    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/outdatedbrowser.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css?25042016" />
    <link rel="stylesheet" type="text/css" href="css/animations.css" />
    <link rel="stylesheet" href="css/font-awesome-animation.css">
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/cokkiecuttr.css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <?php ?>

    <script type="text/javascript" src="js/vendor/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/vendor/modernizr.custom.js"></script>
    <script type="text/javascript" src="js/vendor/fsvs.js"></script>
    <script type="text/javascript" src="js/vendor/respimage.min.js"></script>
    <script type="text/javascript" src="js/vendor/dragdealer.js"></script>
    <script type="text/javascript" src="js/vendor/classie.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/vendor/jquery.cookiecuttr.js"></script>

    <script type="text/javascript">
          jQuery(document).ready(function () {
            jQuery.cookieCuttr({
             cookieDeclineButton: false,
              cookiePolicyLink: false,
              cookieAnalyticsMessage: "<p><?php echo $lang['COOKIES_SHB'] ?></p>",
              cookieAcceptButtonText: "<img src='img/close.png' />",
              cookieDeclineButtonText: "No acepto",
              //cookieExpires: 1
          });
          });
    </script>

    <script type="text/javascript" src="js/vendor/outdatedbrowser.min.js"></script>

     <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43497139-1', 'auto');
  ga('send', 'pageview');

</script>
