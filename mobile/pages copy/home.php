<?php require_once("../admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<meta property="og:title" content="Surf House Barcelona" />
<meta property="og:description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. 'Fast food' pero 'Healthy' y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/" />

<script type="text/javascript" src="js/menu.js"></script>

<title>SHB</title>

</head>
<body class="bg-home">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-12" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>

    <div class="home-content">
        <div class="img-wrap animation-topToDown3 animation-delay-8"></div>
            <a id="trigger-overlay-logo" class="" href="javascript:void('0');"><img class="logo animation-DownToTop3 animation-delay-8" src="img/logo.png" srcset="img/logo.png 1600w, img/logo.png 2560w" sizes="(max-width: 1280px) 1600px"></span></a>
        <div class="bottom-bar animation-DownToTop animation-delay-12">
            <a href="tel:932 507 023" target="_blank">
                <img src="img/phone-icon.png" alt="Phone" />
            </a>
            <span class="bar"></span>
            <a href="comgooglemaps://?q=Carrer l'Almirall Aixada, 22, Barcelona&center=41.377533,2.190586" target="_blank">
                <img src="img/pin-icon.png" alt="location" />
            </a>
            <span class="bar"></span>
            <a href="mailto:hello@surfhousebarcelona.com" target="_blank">
                <img src="img/mail-icon.png" alt="Mail" />
            </a>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });

    </script>
</body>
</html>
