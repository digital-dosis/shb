<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<meta property="og:title" content="Surf House Barcelona" />
<meta property="og:description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. 'Fast food' pero 'Healthy' y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/" />

<script type="text/javascript" src="js/menu.js"></script>

<title>SHB Contact</title>

</head>
<body class="bg-home">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-16" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-16">
        <img src="img/top-logo.png" alt="Logo" />
    </a>

    <div class="contact-content">
        <div>
            <img class="animation-topToDown animation-delay-4" src="img/phone-icon.png" alt="Phone Number" />
            <p class="animation-topToDown animation-delay-10">
                T. 932 507 023
            </p>
             <img class="animation-topToDown animation-delay-5" src="img/mail-icon.png" alt="Mail" />
            <p class="animation-topToDown animation-delay-11">
                <a href="mailto:hello@surfhousebarcelona.com" >HELLO@SURFHOUSEBARCELONA.COM</a>
            </p>
            <img class="animation-topToDown animation-delay-6" src="img/pin-icon.png" alt="Location" />
            <p class="animation-topToDown animation-delay-12">
                <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank">
                    C/ L’ALMIRALL AIXADA 22 <br/>
                    08003 BARCELONA
                </a>
            </p>
           
            <img class="animation-topToDown animation-delay-7" src="img/clock-icon.png" alt="Schedule" />
            <p class="animation-topToDown animation-delay-14">
                <?php echo $lang['HORARIO'] ?>
                <br/>
                <?php echo $lang['MIERCOLES'] ?>
                <br/>
                <?php echo $lang['VIERNES'] ?> - <?php echo $lang['DOMINGO'] ?>

            </p>
        </div>

        <div class="bottom-bar animation-DownToTop animation-delay-12">
            <a href="https://www.facebook.com/surfhousebarcelona" target="_blank">
                <img src="img/fb.png" alt="Facebook" />
            </a>
            <span class="bar"></span>
            <a href="http://instagram.com/surfhousebarcelona" target="_blank">
                <img src="img/ig.png" alt="Instagram" />
            </a>
            <span class="bar"></span>
            <a href="https://twitter.com/surfhousebcn" target="_blank">
                <img src="img/tw.png" alt="Twitter" />
            </a>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });

    </script>
</body>
</html>
