<div class="overlay overlay-hugeinc">
    <a href="javascript:void('0');" class="overlay-close opacity-hover">Close</a>

    <div class="lang-switch menu-lang">
        <a href="en" <?php if ( $lang['IDIOMA'] == 'en' ) echo 'class="selected"'; ?>>ENG</a>
        <span>  /  </span>
        <a href="es" <?php if ( $lang['IDIOMA'] == 'es' ) echo 'class="selected"'; ?>>ESP</a>
    </div>

    <nav>
        <ul>
            <li>
                    <a href="home">
                        HOME
                        <span class="border"></span>
                    </a>
                </li>
                <li>
                    <a href="food">
                        FOOD
                        <span class="border"></span>
                    </a>
                </li>
                <li>
                    <a href="drinks">
                        DRINKS
                        <span class="border"></span>
                    </a>
                </li>
                <li>
                    <a href="activities">
                        <?php echo $lang['ACTIVITIES'] ?>
                        <span class="border"></span>
                    </a>
                </li>
                <li>
                    <a href="contact">
                        CONTACT
                        <span class="border"></span>
                    </a>
                </li>
                <li>
                    <a href="http://surfhouseonwheels.com/" target="_blank">
                        ON WHEELS
                        <span class="border"></span>
                    </a>
                </li>
                <!-- <li>
                    <a href="">
                        SHB WEAR
                    </a>
                </li> -->
        </ul>
    </nav>

    <a href="http://www.digitaldosis.com" target="_blank" class="firma-menu">
        DIGITAL DOSIS WEB DESIGN
    </a>

</div>
