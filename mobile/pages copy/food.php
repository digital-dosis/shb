<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Food & Drinks" />
<meta property="og:description" content='SURF HOUSE BARCELONA. Healthy “fast food” with the best quality.' />
<meta property="og:image" content='http://www.surfhousebarcelona.com/fb-SHB-Food.png' />
<meta property="og:url" content='http://www.surfhousebarcelona.com/fooddrinks' />

<script type="text/javascript" src="js/menu.js"></script>
<script src="js/vendor/jquery.swipeshow.min.js"></script>
<link rel="stylesheet" href="css/jquery.swipeshow.css" />

<title>SHB FOOD</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-8" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>

    <a href="home" class="top-logo animation-topToDown animation-delay-8">
        <img src="img/top-logo.png" alt="Logo" />
    </a>

    <p class="food-title animation-DownToTop3 animation-delay-10">FOOD</p>

    <div class="food-content">
        <div class="swipeshow">
            <ul class="slides">
	            <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/2.jpg" srcset="../img/fooddrinks/food/mobile/2.jpg 1600w, ../img/fooddrinks/food/mobile/2.jpg 2560w">
                        <img class="sombra" src="img/shadow.png" alt="Sombra" />
                        <div>
                            <p class="animation-topToDown3 animation-delay-12">Tuna Tataki</p>
                            <b class="line animation-DownToTop3 animation-delay-14"></b>
                        </div>
                    </span>
                </li>
				<li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/7.jpg" srcset="../img/fooddrinks/food/mobile/7.jpg 1600w, ../img/fooddrinks/food/mobile/7.jpg  2560w">
                        <div>
                            <p>Infarto </p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>

                <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/1.jpg" srcset="../img/fooddrinks/food/mobile/1.jpg 1600w, ../img/fooddrinks/food/mobile/1.jpg  2560w">
                        <div>
                            <p>Beach Nachitos</p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>
                <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/3.jpg" srcset="../img/fooddrinks/food/mobile/3.jpg 1600w, ../img/fooddrinks/food/mobile/3.jpg 2560w">
                        <img class="sombra" src="img/shadow.png" alt="Sombra" />
                        <div>
                            <p>Veggie Burger</p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>
                <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/4.jpg" srcset="../img/fooddrinks/food/mobile/4.jpg 1600w, ../img/fooddrinks/food/mobile/4.jpg 2560w">
                        <img class="sombra" src="img/shadow.png" alt="Sombra" />
                        <div>
                            <p>Surfer's Burrito</p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>
                <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/5.jpg" srcset="../img/fooddrinks/food/mobile/5.jpg 1600w, ../img/fooddrinks/food/mobile/5.jpg 2560w">
                        <img class="sombra" src="img/shadow.png" alt="Sombra" />
                        <div>
                            <p>Açai Bowl</p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>
                <li class="slide">
                    <span>
                        <img sizes="(max-width: 1280px) 1600px" src="../img/fooddrinks/food/mobile/6.jpg" srcset="../img/fooddrinks/food/mobile/6.jpg 1600w, ../img/fooddrinks/food/mobile/6.jpg 2560w">
                        <img class="sombra" src="img/shadow.png" alt="Sombra" />
                        <div>
                            <p>Nutella Waffle</p>
                            <b class="line"></b>
                        </div>
                    </span>
                </li>
            </ul>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
        jQuery(document).ready(function(){
            $(".swipeshow").swipeshow({
                autostart: false,    /* Set to `false` to keep it steady */
                interval: 2000,     /* Time between switching slides (ms) */
                initial: 0,         /* First slide's index */
                speed: 200,         /* Animation speed (ms) */
                friction: 0.3,      /* Bounce-back behavior; use `0` to disable */
                mouse: true,        /* enable mouse dragging controls */
                keys: true,         /* enable left/right keyboard keys */

                onactivate: function(){},
                onpause: function(){},
            });
        });
    </script>
</body>
</html>
