<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Food & Drinks" />
<meta property="og:description" content='SURF HOUSE BARCELONA. Healthy “fast food” with the best quality.' />
<meta property="og:image" content='http://www.surfhousebarcelona.com/fb-SHB-Food.png' />
<meta property="og:url" content='http://www.surfhousebarcelona.com/fooddrinks' />

<script type="text/javascript" src="js/menu.js"></script>
<script src="js/vendor/jquery.swipeshow.min.js"></script>
<link rel="stylesheet" href="css/jquery.swipeshow.css" />

<title>SHB FOOD & DRINKS</title>

</head>
<body>

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-8" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-8">
        <img src="img/top-logo.png" alt="Logo" />
    </a>

    <div class="food-drinks-content">
        <h2><?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA']; ?></h2>
        <div class="food-drinks__menus">
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_1']?>" target="_blank">
                <div class="food-drinks__menu">
                    <img src="img/icon-menu-food.svg" alt="">
                    <h3>SHB<br><span>Barceloneta</span></h3>
                </div>
            </a>
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_2']?>" target="_blank">
                <div class="food-drinks__menu">
                    <img src="img/icon-menu-food.svg" alt="">
                    <h3>SHB<br><span>Secret Spot</span></h3>
                </div>
            </a>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
