<div class="overlay overlay-hugeinc">
    <a href="javascript:void('0');" class="overlay-close opacity-hover">Close</a>

    <div class="lang-switch menu-lang">
        <a href="en" <?php if ( $lang['IDIOMA'] == 'en' ) echo 'class="selected"'; ?>>ENG</a>
        <span>  /  </span>
        <a href="es" <?php if ( $lang['IDIOMA'] == 'es' ) echo 'class="selected"'; ?>>ESP</a>
    </div>

    <nav>
        <ul>
            <li>
                    <a href="home">
                        Home
                    </a>
                </li>
                <li>
                    <a href="food">
                        Food
                    </a>
                </li>
                <li>
                    <a href="drinks">
                        Drinks

                    </a>
                </li>
                <li>
                    <a href="activities">
                        <?php echo $lang['ACTIVITIES'] ?>
                    </a>
                </li>
                <li>
                    <a href="http://www.surfhousebarcelona.com/mobile/trips/">
                        SHB Trips
                    </a>
                </li>
                <li>
                  <a href="contact">
                    <?php echo $lang['CONTACT'] ?>
                  </a>
                </li>
                <li>
                    <!-- <span class="border"></span> -->
                    <a href="http://surfhouse-shop.com/" target="_blank" class="block-popup-shop">
                        Shop
                    </a>
                </li>
                <li>
                    <a href="secretspot" class="menulink-secret-spot">
                        SHB
                    </a>
                </li>
                <!-- <li>
                    <a href="https://surfhouse-es.mydelivery.io" target="_blank">
                        DELIVERY
                    </a>
                </li> -->
        </ul>
    </nav>

    <a href="http://www.digitaldosis.com" target="_blank" class="firma-menu">
        DIGITAL DOSIS WEB DESIGN
    </a>

    <div class="popup popup-home popup-home--shop">
        <div class="msg">
            <a href="javascript:void('0');" class="popup-close">

            </a>
            <a href="mailto:orders@surfhousebarcelona.com">
              <?php if ( $lang['IDIOMA'] == 'es' ) echo '<img src="img/SHB-online-shop-mobile2-CAST.png">';?>
              <?php if ( $lang['IDIOMA'] == 'en' ) echo '<img src="img/SHB-online-shop-mobile2-ENG.png">';?>
            </a>
        </div> 
    </div>

</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.popup-home--shop').click(function(){
      $(this).fadeOut();
    });

   $('.popup-close').click(function(){
      $(this).parents('.popup').fadeOut();
    });

   $('.block-popup-shop').click(function(e){
      e.preventDefault();
      $('.popup-home--shop').addClass('is-visible').fadeIn();
   });
  });
</script>
