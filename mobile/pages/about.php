<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | About" />
<meta property="og:description" content="Surf House Barcelona has its origins in California’s lifestyle and we want to give you something different to what you can find anywhere else in the city." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-About.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/about" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/about.js"></script>

<title><?php echo $lang['ABOUT_SHB'] ?></title>

</head>
<body class="bg-about">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

    <div class="about-content">

        <div class="slide">
            <div class="img-wrap">
            </div>
            <img sizes="(max-width: 1280px) 1600px" class="portada-title" src="img/about/title.png" srcset="img/about/title.png 1600w, img/about/title@2x.png 2560w" alt="ABOUT">
            <div class="down">
                <a href="javascript:void('0');" id="down">
                    <img sizes="(max-width: 1280px) 1600px" src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w">
                </a>

                <span></span>
            </div>
        </div>
        <div class="slide">
            <div class="center-wrapper">

                <div class="green-border"></div>
                <div>
                    <div>
                        <h1>
                            <?php echo $lang['AB_TEXT_1']?>
                        </h1>
                        <p>
                            <?php echo $lang['AB_TEXT_2']?>
                        </p>
                    </div>
                    <p>
                        <?php echo $lang['AB_TEXT_3']?>
                        <br/><br/>
                        <?php echo $lang['AB_TEXT_4']?>
                    </p>
                </div>

            </div>
        </div>
        <div class="slide">

        </div>
        <div class="slide">
             <h1>SHB <span>SURF TEAM</span></h1>

            <div id="team-carousel" class="dragdealer active">
                <div class="handle" >
                    <div class="page">
                        <a href="team/albert-hernandez" class="team-member">
                            <div class="img-team albert-hernandez">
                                <div class="foto"></div>
                            </div>
                            <p>
                                Albert <b>Hernandez</b>
                                <span class="team-line"></span>
                            </p>
                        </a><a href="team/alex-knopfel" class="team-member">
                            <p>
                                Alex <b>Knopfel</b>
                                <span class="team-line"></span>
                            </p>
                            <div class="img-team alex-knopfel"><div class="foto"></div></div>
                        </a><a href="team/carles-medina" class="team-member">
                            <div class="img-team carles-medina"><div class="foto"></div></div>
                            <p>
                                Carles <b>Medina</b>
                                <span class="team-line"></span>
                            </p>
                        </a><a href="team/pablo-viaplana" class="team-member">
                            <p>
                                Pablo <b>Viaplana</b>
                                <span class="team-line"></span>
                            </p>
                            <div class="img-team pablo-viaplana"><div class="foto"></div></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="center-wrapper">
                <span class="line"></span>
                <h1><?php echo $lang['AB_LAST_TEXT']?></h1>
                <div>
                    <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank">
                        <img src="img/fooddrinks/map.png" srcset="img/fooddrinks/map.png 1600w, img/fooddrinks/map@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
                        <p><b>C/ L’ALMIRALL AIXADA 22</b><br/>
                        08003 BARCELONA <span>T. 932 507 023</span></p>
                    </a>
                </div>
            </div>

            <ul class="menu-bottom">
                <li>
                    <a href="home">
                        HOME
                    </a>
                </li>
                <li>
                    <a href="fooddrinks">
                        FOOD&DRINKS
                    </a>
                </li>
                <li>
                    <a href="activities">
                        <?php echo $lang['ACTIVITIES'] ?>
                    </a>
                </li>
                <li>
                    <a href="about">
                        <?php echo $lang['ABOUT_SHB'] ?>
                    </a>
                </li>
                <li>
                    <a href="http://www.surfhousebarcelona.com/trips/">
                        SHB TRIPS
                    </a>
                </li>
            </ul>

            <a href="http://www.digitaldosis.com" target="_blank" class="firma">
                <span class="topline"></span>
                Digital Dosis
                <span>Web Design</span>
            </a>

        </div>

    </div>
    <style type="text/css">

        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
        html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
            border-color: #4A1D88;
        }

        html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
            background: #4A1D88;
        }
    </style>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
