<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Secret Spot" />
<meta property="og:description" content="Surf House Secret Spot. Un rincón especial y acogedor, como esa playa tan recóndita y escondida que solo compartes con los tuyos." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-SecretSpot.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/secretspot" />

<script type="text/javascript" src="js/menu.js"></script>

<title><?php echo $lang['SECRET-SPOT_SHB'] ?></title>

</head>
<body class="bg-secretspot">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-8" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-8">
        <img src="img/top-logo.png" alt="Logo" />
    </a>
    <div class="secretspot-wrapper">
       <div class="secretspot animation-DownToTop3 animation-delay-6">
          <h1 class="secretspot__title">
              SHB
          </h1>
          <div class="secretspot__text">
            <p>
              <?php echo $lang['SECRET-SPOT_TEXT_1']; ?>
            </p>
            <p>
              <?php echo $lang['SECRET-SPOT_TEXT_2']; ?>
            </p>
            <img class="secretspot__image" sizes="(max-width: 1280px) 1600px" src="img/secretspot/img-mobile.jpg" srcset="img/secretspot/img-mobile.jpg 1600w, img/secretspot/img-mobile@2x.jpg 2560w" alt="">
            <p>
              <?php echo $lang['SECRET-SPOT_TEXT_3']; ?>
            </p>
            <p>
              <?php echo $lang['SECRET-SPOT_TEXT_4']; ?>
            </p>
          </div>
          <div class="secretspot__menu">
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_2']; ?>" target="_blank"><img src="img/icon-menu-food.svg" alt=""></a>
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_2']; ?>" target="_blank"><p><?php echo $lang['SECRET-SPOT_VER_CARTA_BRUNCH']; ?></p></a>
          </div>
          <div class="secretspot__menu secretspot__menu--alt">
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_3']; ?>" target="_blank"><img src="img/icon-menu-food.svg" alt=""></a>
            <a href="<?php echo $lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_3']; ?>" target="_blank"><p><?php echo $lang['SECRET-SPOT_VER_CARTA']; ?></p></a>
          </div>
          <div class="secretspot__bottom">
            <h2>
              <?php echo $lang['SECRET-SPOT_LAST_TEXT_BOTTOM_1']; ?><br>
              <span><?php echo $lang['SECRET-SPOT_LAST_TEXT_BOTTOM_2']; ?></span>
            </h2>
            <div class="secretspot__info">
              <img src="img/map.png" alt="">
              <a href="https://www.google.es/maps/place/arrer+d'Amig%C3%B3,+53,+08021+Barcelona/@41.3959585,2.1412942,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498767158a267:0xcff4ce410346db54!8m2!3d41.3959585!4d2.1434882" target="_blank">
                C/ Amigó 53 (Esquina Madrazo)<br>
                08021 Barcelona <span>T. 930 313 135</span>
              </a>
            </div>
          </div>
      </div>
    </div>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
