<?php require_once("../admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<meta property="og:title" content="Surf House Barcelona" />
<meta property="og:description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. 'Fast food' pero 'Healthy' y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/" />

<script type="text/javascript" src="js/menu.js"></script>

<title>SHB</title>

</head>
<body class="bg-home">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-12" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>

    <div class="popup popup-home popup-home--closed">
        <div class="msg">
            <a href="javascript:void('0');" class="popup-close">

            </a>
            <a href="http://surfhousebarcelona.com/pdf/SHB-BS-Carta.pdf">
                <img src="img/delivery-mobile.png">
            </a>
        </div>
    </div>

    <!-- <div id="popup">
        <div id="msg">
            <a href="javascript:void('0');" id="popup-close">

            </a>
            <a href="mailto:orders@surfhousebarcelona.com">
              <?php if ( $lang['IDIOMA'] == 'es' ) echo '<img src="img/SHB-online-shop-mobile2-CAST.png">';?>
              <?php if ( $lang['IDIOMA'] == 'en' ) echo '<img src="img/SHB-online-shop-mobile2-ENG.png">';?>
            </a>
        </div> 
    </div> -->

    <div class="home-content">
        <div class="img-wrap animation-topToDown3 animation-delay-8"></div>
        <a id="trigger-overlay-logo" class="" href="javascript:void('0');"><img class="logo animation-DownToTop3 animation-delay-8" src="img/logo.png" srcset="img/logo.png 1600w, img/logo.png 2560w" sizes="(max-width: 1280px) 1600px"></span></a>
        <div class="top-bar animation-DownToTop3 animation-delay-8" data-modal="menu">
            <a href="<?php echo $lang['PDF_CARTA_barceloneta_2020'];?>" target="_blank" class="top-bar__link">
                <?php echo $lang['VER_CARTA'];?>
            </a>
        </div>
        <div class="top-bar-modal top-bar-modal--menu">
            <a href="<?php echo $lang['PDF_CARTA_barceloneta_2020'];?>" target="_blank">
                <div class="top-bar-modal__section top-bar-modal__section--top">
                    <h2 class="top-bar-modal__title">SHB <strong>Barceloneta</strong></h2>
                    <p class="top-bar-modal__view"><?php echo $lang['VER_CARTA'];?></p>
                </div>
            </a>
            <a href="<?php echo $lang['PDF_CARTA_secretspot_2020'];?>" target="_blank">
                <div class="top-bar-modal__section top-bar-modal__section--bottom">
                    <h2 class="top-bar-modal__title">SHB <strong>Secret Spot</strong></h2>
                    <p class="top-bar-modal__view"><?php echo $lang['VER_CARTA'];?></p>
                </div>
            </a>
        </div>
        <div class="bottom-bar bottom-bar--main animation-DownToTop animation-delay-12">
            <a class="bottom-bar__icon bottom-bar__icon--phone" data-modal="phones" href="tel:932 507 023" target="_blank">
                <img src="img/phone.svg" alt="Phone" />
            </a>
            <span class="bar"></span>
            <a class="bottom-bar__icon bottom-bar__icon--location" data-modal="locations" href="comgooglemaps://?q=Carrer l'Almirall Aixada, 22, Barcelona&center=41.377533,2.190586" target="_blank">
                <img src="img/location.svg" alt="location" />
            </a>
            <span class="bar"></span>
            <a href="mailto:hello@surfhousebarcelona.com" target="_blank">
                <img src="img/mail.svg" alt="Mail" />
            </a>
        </div>
        <div class="bottom-bar-modal bottom-bar-modal--phones">
            <a href="tel:932 507 023" target="_blank">
                <div class="bottom-bar-modal__section bottom-bar-modal__section--top">
                    <h2 class="bottom-bar-modal__title">SHB <strong>Barceloneta</strong></h2>
                    <p class="bottom-bar-modal__phone">932 507 023</p>
                </div>
            </a>
            <a href="tel:930 313 135" target="_blank">
                <div class="bottom-bar-modal__section bottom-bar-modal__section--bottom">
                    <h2 class="bottom-bar-modal__title">SHB <strong>Secret Spot</strong></h2>
                    <p class="bottom-bar-modal__phone">930 313 135</p>
                </div>
            </a>
        </div>
        <div class="bottom-bar-modal bottom-bar-modal--locations">
            <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank">
                <div class="bottom-bar-modal__section bottom-bar-modal__section--top">
                    <h2 class="bottom-bar-modal__title">SHB <strong>Barceloneta</strong></h2>
                    <p class="bottom-bar-modal__location">C/ L’Almirall Aixada 22,<br>08001 Barcelona</p>
                    <p class="bottom-bar-modal__link"> <?php echo $lang['VER_EN_GOOGLE_MAPS']; ?></p>
                </div>
            </a>
            <a href="https://www.google.es/maps/place/Carrer+d'Amig%C3%B3,+53,+08021+Barcelona/@41.3959585,2.1412942,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498767158a267:0xcff4ce410346db54!8m2!3d41.3959585!4d2.1434882" target="_blank">
                <div class="bottom-bar-modal__section bottom-bar-modal__section--bottom">
                    <h2 class="bottom-bar-modal__title">SHB <strong>Secret Spot</strong></h2>
                    <p class="bottom-bar-modal__location">C/ Amigó 53 (esquina Madrazo),<br>08021 Barcelona</p>
                    <p class="bottom-bar-modal__link"><?php echo $lang['VER_EN_GOOGLE_MAPS']; ?></p>
                </div>
            </a>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
            $('.popup-home--closed').addClass('is-visible');
            /*$('#popup').click(function(){
                $(this).fadeOut();
            });

            $('#popup-close').click(function(){
                $('#popup').fadeOut();
            })*/
        });
    </script>
</body>
</html>
