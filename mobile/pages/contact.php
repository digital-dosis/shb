<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>

<meta property="og:title" content="Surf House Barcelona" />
<meta property="og:description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. 'Fast food' pero 'Healthy' y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/" />

<script type="text/javascript" src="js/menu.js"></script>

<title>SHB Contact</title>

</head>
<body class="bg-home">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-16" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-16">
        <img src="img/top-logo.png" alt="Logo" />
    </a>

    <div class="contact-content">
        <div class="contact-content__container-address">
            <div class="contact-content__address">
                <div class="contact-content__address-info">
                    <h2>SHB <strong>Barceloneta</strong></h2>
                    <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank"><p>C/ L'Almirrall Aixada 22, 08001 Barcelona<br>T. 932 507 023</p></a>
                </div>
                <div class="contact-content__address-schedule">
                    <p><strong><?php echo $lang['MONDAY_AND_TUESDAY'] ?>:</strong> <?php echo $lang['CLOSED'] ?></p>
                    <p><strong><?php echo $lang['WEDNESDAY_AND_THURSDAY'] ?>:</strong> 12h <?php echo $lang['to'] ?> 18h</p>
                    <p><strong><?php echo $lang['FRIDAY_AND_SATURDAY'] ?>:</strong> 12h <?php echo $lang['to'] ?> 23h</p>
                    <p><strong><?php echo $lang['SUNDAY'] ?>:</strong> 12h <?php echo $lang['to'] ?> 22h</p>
                </div>
            </div>
            <div class="contact-content__address">
                <div class="contact-content__address-info">
                    <h2>SHB <strong>Secret Spot</strong></h2>
                    <p><a href="https://www.google.es/maps/place/Carrer+d'Amig%C3%B3,+53,+08021+Barcelona/@41.3959585,2.1412942,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498767158a267:0xcff4ce410346db54!8m2!3d41.3959585!4d2.1434882" target="_blank">C/ Amigó 53, 08021 Barcelona</a><br>T. 930 313 135</p>
                </div>
                <div class="contact-content__address-schedule">
                    <p><strong><?php echo $lang['MONDAY'] ?>:</strong> <?php echo $lang['CLOSED'] ?></p>
                    <p><strong><?php echo $lang['TUESDAY_TO_THURSDAY'] ?>:</strong> 09h <?php echo $lang['to'] ?> 24h</p>
                    <p><strong><?php echo $lang['FRIDAY'] ?>:</strong> 09h <?php echo $lang['to'] ?> 1h</p>
                    <p><strong><?php echo $lang['SATURDAY'] ?>:</strong> 10h <?php echo $lang['to'] ?> 16h <?php echo $lang['AND'] ?> 20h <?php echo $lang['to'] ?> 1h</p>
                    <p><strong><?php echo $lang['SUNDAY'] ?>:</strong> 10h <?php echo $lang['to'] ?> 17h</p>
                </div>
            </div>
        </div>
        <div class="bottom-bar bottom-bar--social animation-DownToTop animation-delay-12">
            <a class="bottom-bar__icon bottom-bar__icon--mail" href="mailto:hello@surfhousebarcelona.com" target="_blank">
                <img src="img/mail.svg" alt="Mail" />
            </a>
             <span class="bar"></span>
            <a class="bottom-bar__icon bottom-bar__icon--facebook" href="https://www.facebook.com/surfhousebarcelona" target="_blank">
                <img src="img/fb.svg" alt="Facebook" />
            </a>
            <span class="bar"></span>
            <a class="bottom-bar__icon bottom-bar__icon--instagram" href="http://instagram.com/surfhousebarcelona" target="_blank">
                <img src="img/ig.svg" alt="Instagram" />
            </a>
            <!-- <a class="bottom-bar__icon bottom-bar__icon--twitter" href="https://twitter.com/surfhousebcn" target="_blank">
                <img src="img/tw.png" alt="Twitter" />
            </a> -->
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });

    </script>
</body>
</html>
