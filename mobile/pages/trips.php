<?php require_once("../admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Trips" />
<meta property="og:description" content="SHB Trips is our own platform to create your perfect surf trip. We are surfers and traveling, meeting people and sharing experiences is our passion." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-Trips.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/trips" />

<script type="text/javascript" src="js/menu.js"></script>
<!-- <script type="text/javascript" src="js/activities.js"></script> -->

<title>SHB <?php echo $lang['TRIPS_SHB'] ?></title>

</head>
<body class="bg-trips">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-8" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-8">
        <img src="img/top-logo.png" alt="Logo" />
    </a>
    <div class="trips-wrapper">
       <div class="trips animation-DownToTop3 animation-delay-6">
          <h1 class="trips__title">
              <?php echo $lang['TRIPS_TEXT_1']; ?>
          </h1>
          <div class="trips__text">
            <p>
              <?php echo $lang['TRIPS_TEXT_2']; ?>
            </p>
            <p>
              <?php echo $lang['TRIPS_TEXT_3']; ?>
            </p>
            <p>
              <?php echo $lang['TRIPS_TEXT_4']; ?>
            </p>
            <img class="trips__image" sizes="(max-width: 1280px) 1600px" src="img/trips/img-mobile.jpg" srcset="img/trips/img-mobile.jpg 1600w, img/trips/img-mobile@2x.jpg 2560w" alt="">
            <p>
              <?php echo $lang['TRIPS_TEXT_5']; ?>
            </p>
            <p>
              <?php echo $lang['TRIPS_TEXT_6']; ?>
            </p>
            <p>
              <?php echo $lang['TRIPS_TEXT_7']; ?>
            </p>
            <p>
              <?php echo $lang['TRIPS_TEXT_8']; ?>
            </p>
          </div>
          <div class="trips__bottom">
            <h2 class="trips__subtitle">
              <?php echo $lang['TRIPS_LAST_TEXT_TOP']; ?>
              <span>
                <?php echo $lang['TRIPS_LAST_TEXT_BOTTOM']; ?>
              </span>
            </h2>
            <a href="mailto:actividades@surfhousebarcelona.com" class="trips__info" target="_blank">
              <?php echo $lang['TRIPS_INFO_1']; ?>
              <span>
                <?php echo $lang['TRIPS_INFO_2']; ?>
              </span>
            </a>
          </div>
      </div>
    </div>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>
</body>
</html>
