<?php require_once("../admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html class="fsvs">

<?php include 'head.php' ?>
<meta property="og:title" content="Surf House Barcelona | Activities" />
<meta property="og:description" content="SURF HOUSE BARCELONA. We offer daily activities such as paddle surf, beach workouts to stay in shape, running, surfing championship broadcasts..." />
<meta property="og:image" content="http://www.surfhousebarcelona.com/fb-SHB-Activities.png" />
<meta property="og:url" content="http://www.surfhousebarcelona.com/activities" />

<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/activities.js"></script>

<title>SHB <?php echo $lang['ACTIVITIES'] ?></title>

</head>
<body class="bg-activities">

    <div id="page-loader">
        <img src="img/shb.gif" class="loading-gif">
    </div>

    <?php include 'pages/menu.php' ?>

    <a id="trigger-overlay" class="menu-button home-animation animation-topToDown animation-delay-8" href="javascript:void('0');">
        <img src="img/menu.png" alt="Menu" />
    </a>
    <a href="home" class="top-logo animation-topToDown animation-delay-8">
        <img src="img/top-logo.png" alt="Logo" />
    </a>

     <div class="activities-content">
        <div id="activities-carousel" class="dragdealer">
            <div class="handle" >
                <div class="page animation-right animation-delay-8">

                <?php

                    $location = "";
                    if ( $lang['IDIOMA'] == 'es' ) {
                        $location = "_es";
                    }

                    foreach(collection("Actividades")->find()->sort(["orden"=>1]) as $item):
                ?>
                        <div class="activitie <?php echo $item["Css_Class"]; ?>" data-dayofweek="<?php echo $item["Dias_en_num"]; ?>">
                            <div></div>
                            <div>
                                <div>
                                    <div>
                                        <img src="../img/activities/<?php echo $item["Css_Class"]; ?>-icon@2x.png" >
                                    </div>
                <?php
                                if ( $item["Precio"] != -1) {
                ?>
                                    <div>
                <?php
                                        if ( $item["Precio"] != 0) {
                                            echo $item["Precio"];
                ?>
                                            €
                <?php
                                        } else {
                                            echo '<img sizes="(max-width: 1280px) 1600px" src="../img/activities/free.png" srcset="../img/activities/free.png 1600w, ../img/activities/free@2x.png 2560w">';
                                        }
                ?>
                                    </div>
                <?php
                                }
                ?>
                                </div><div>
                                    <div>
                                        <h1><?php echo $item["Nombre"]; ?></h1>
                                        <p><?php echo $item["Descripcion" . $location]; ?></p>
                                        <span></span>

                                        <h2><?php echo $item["Dias" . $location]; ?></h2>
                                    </div>



                                    <div class="scheduled">
                                        <img src="../img/activities/clock@2x.png" >
                                        <?php
                                        if ($item["Hora_Inicio"] == "<p>-1</p>") {
                                            echo "<span>" . $lang['ASK_US_FOR_TIMES'] . "</span>";

                                        }
                                        else if($item["Hora_Inicio"] == "<p>-2</p>"){
                                            echo "<span>" . $lang['WHENEVER_YOU_LIKE'] . "</span>";
                                        }

                                        else {
                                          if($item["Hora_Final"]==""):
                                            echo "<span>" . $item["Hora_Inicio"] . " H";
                                          else :
                                          echo "<span>" . $item["Hora_Inicio"]." - ". $item["Hora_Final"]." H" . "</span>";
                                        endif;

                                        }
                                        ?>
                                    </div>



                                <?php if ($item['booking'] == 1) { ?>
                                  <a href="mailto:<?php echo $lang['AC_LOC_MAIL']; ?>" class="booking blink">
                                    <img src="../img/activities/mail@2x.png" >
                                    <span><?php echo $lang['RESERVE'];?></span>
                                  </a>
                                  <?php }else { }
                                ?>


                                <?php
                                if ($item["more_info"]) { ?>
                                  <a href="<?php echo $item["more_info"]; ?>" title="<?php echo $lang['MORE_INFO'];?>" target="_blank" class="more-info blink">
                                    <img src="../img/activities/moreinfo@2x.png" >
                                    <span><?php echo $lang['MORE_INFO'];?></span>
                                  </a>
                                  <?php }else { }
                                ?>




                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                ?>
                </div>
            </div>
        </div>
        <h1 class="animation-DownToTop3 animation-delay-6">
            <?php echo $lang['ACTIVITIES'] ?>
            <b class="line animation-DownToTop3 animation-delay-10"></b>
        </h1>
    </div>
    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');

            var d = new Date();
            var n = d.getDay() - 1;
            //console.log(n);

            $('.activitie').each(function(index, el) {
                var dayOfWeek = $(el).attr('data-dayofweek');

                if ( dayOfWeek.length > 1) {
                    for (var i = 0; i < dayOfWeek.length; i++) {
                        if ( n == dayOfWeek[i] ) {
                            $(el).prepend('<span class="today-activity"><span class="text-today animation-fade animation-delay-14"><?php echo $lang['TODAY']?></span> <span class="line-today animation-line animation-delay-6"></span></span>');
                        }
                    }
                } else {
                    if ( n == dayOfWeek ) {
                        $(el).prepend('<span class="today-activity"><span class="text-today animation-fade animation-delay-14"><?php echo $lang['TODAY']?></span> <span class="line-today animation-line animation-delay-6"></span></span>');
                    }
                }
            });
        });
    </script>
</body>
</html>
