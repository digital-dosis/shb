$(window).load(function() {
	var triggerBttn = document.getElementById( 'trigger-overlay' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( '.overlay-close' );
	if (document.getElementById( 'trigger-overlay-logo' )) {
	var triggerBttnLogo = document.getElementById( 'trigger-overlay-logo' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( '.overlay-close' );
	}

		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };


	function toggleOverlay() {
		if( classie.has( overlay, 'open' ) ) {
			classie.remove( overlay, 'open' );
			classie.add( overlay, 'close' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay, 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !classie.has( overlay, 'close' ) ) {
			classie.add( overlay, 'open' );
		}
	}

	triggerBttn.addEventListener( 'click', toggleOverlay );
	if (document.getElementById( 'trigger-overlay-logo' )) {
		triggerBttnLogo.addEventListener( 'click', toggleOverlay );
	}
	closeBttn.addEventListener( 'click', toggleOverlay );

	$('.bottom-bar--main .bottom-bar__icon').on('click', function(e){
        e.preventDefault();
        var triggerData = $(this).data('modal');
        var panel = '.bottom-bar-modal--'+triggerData;
        if($(panel).hasClass('bottom-bar-modal--open')){
        	$(panel).fadeOut().removeClass('bottom-bar-modal--open');
        }else{
        	$('.bottom-bar-modal').fadeOut().removeClass('bottom-bar-modal--open');
        	$( panel ).fadeIn().toggleClass('bottom-bar-modal--open');
        }
    });
    /*$('.top-bar').on('click', function(e){
        e.preventDefault();
        var triggerData = $(this).data('modal');
        var panel = '.top-bar-modal--'+triggerData;
        if($(panel).hasClass('top-bar-modal--open')){
        	$(panel).fadeOut().removeClass('top-bar-modal--open');
        }else{
        	$('.top-bar-modal').fadeOut().removeClass('top-bar-modal--open');
        	$( panel ).fadeIn().toggleClass('top-bar-modal--open');
        }
    });*/
});
