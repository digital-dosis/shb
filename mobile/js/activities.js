var dragdealerActivities;

$(function() {

dragdealerActivities = new Dragdealer('activities-carousel', {
  speed: 0.3,
  x:0,
  steps: 15,
  loose: true,
  requestAnimationFrame: true
});

});

$(window).load(function() {
    updateCarousel('activities');
    setTimeout(updateCarousel('activities'), 500);
});

window.onresize = function () {
    updateCarousel('activities');
};

var updateCarousel = function(carousel) {
    var carouselWidth = 0;
    
    // Assuming all items in a carousel have the same width
    var numItems = $('#' + carousel + '-carousel > .handle > .page').children().length;
    carouselWidth = numItems * $('#' + carousel + '-carousel > .handle > .page').children().first().outerWidth();
    carouselWidth += numItems * 20;
/*
    $('#' + carousel + '-carousel > .handle > .page').children().each(function(){
         return carouselWidth = carouselWidth + 10 + $(this).outerWidth();
    });
*/
    $('#' + carousel + '-carousel > .handle').width(carouselWidth + 70);

    dragdealerActivities.reflow();
};
