<?php
        if (!isset($_REQUEST['sec'])) {
            include("pages/home.php");
        } else {
            $get = $_REQUEST['sec'];
            if (substr($_REQUEST['sec'], -1) == '/') {
              $get = substr($_REQUEST['sec'], 0, -1);
            }

            $splitURI = explode('/',$get);

            $lengua_en_url = $splitURI[count($splitURI)-1]=='es' ? true : false;
            if (!$lengua_en_url) { $lengua_en_url = $splitURI[count($splitURI)-1]=='en' ? true : false;}

            if ($lengua_en_url) {
                $get = substr($get, 0, -2);
            }

            if (substr($get, -1) == '/') {
              $get = substr($get, 0, -1);
            }

            if ($get == ''){
              include("pages/home.php");
            } else {
              $page = 'pages/' .  $get . '.php';
              if (file_exists($page)) {
                  include($page);
              } else {
                  include("pages/error404.php");
              }
            }
        }
    ?>
</body>
</html>
