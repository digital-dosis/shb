<?php
$lengua_en_url = false;

if (isset($_REQUEST['sec'])) {

    $get = $_GET['sec'];
    if (substr($_GET['sec'], -1) == '/') {
      $get = substr($_GET['sec'], 0, -1);
    }

    $splitURI = explode('/',$get);

    $lengua_en_url = $splitURI[count($splitURI)-1]=='es' ? true : false;
    if (!$lengua_en_url) { $lengua_en_url = $splitURI[count($splitURI)-1]=='en' ? true : false;}
}

if ($lengua_en_url)
{
  $lang_file = 'lang.'.$splitURI[count($splitURI)-1].'.php';
  $_SESSION['lang_session'] = $splitURI[count($splitURI)-1];
  setcookie('lang_cookie', $splitURI[count($splitURI)-1], time() + (3600 * 24 * 30));
}
else if (isSet($_SESSION['lang_session']))
{
  $lang_file = 'lang.'.$_SESSION['lang_session'].'.php';
}
else if(isSet($_COOKIE['lang_cookie']))
{
  $lang_file = 'lang.'.$_COOKIE['lang_cookie'].'.php';
}
else
{
  $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

  switch ($lang) {
    case 'es':
      $lang_file = 'lang.es.php';
      break;

    case 'en':
      $lang_file = 'lang.en.php';
      break;

    default:
      $lang_file = 'lang.en.php';
      break;
  }
}

include_once 'lang/'.$lang_file;
?>