<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="SURF HOUSE BARCELONA nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. “Fast food” pero “Healthy” y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. " />
    <meta name="keywords" content="Surf House Barcelona, smoothies, milkshakes, betidos, sandwich, hamburguesa, burger, fitness, health, Barceloneta, playa, restaurant, cafetería, frappe, cocktail, desayunos, comida, beach, ensaladas, food." />

    <base href="/mobile/">

    <link rel="apple-touch-icon-precomposed" href="apple-touch-SHB.jpg" />
    <link rel="shortcut icon" href="img/shb-favicon.png" />

    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css?<?php echo date('Y-m-d H:i:s'); ?>" />
    <link rel="stylesheet" type="text/css" href="css/animations.css" />

    <script type="text/javascript" src="js/vendor/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/vendor/modernizr.custom.js"></script>
    <script type="text/javascript" src="js/vendor/respimage.min.js"></script>
    <script type="text/javascript" src="js/vendor/dragdealer.min.js"></script>
    <script type="text/javascript" src="js/vendor/classie.js"></script>

    <script type="text/javascript">
        $(window).load(function() {
            $('#page-loader').addClass('hide-animation');
            $('body').addClass('loaded');
        });
    </script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43724663-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '461458387393903');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=461458387393903&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->