<?php
/*
  ------------------
  Language: English
  ------------------
 */

$lang = array();

$lang['IDIOMA'] =

    'en';

$lang['VIEW_OUR_MENU'] =

    'VIEW OUR MENU';

$lang['HIGHLIGHTED_ACTIVITY'] =

    'HIGHLIGHTED ACTIVITY';

$lang['TODAY_AT'] =

    'TODAY AT';

$lang['VIEW_ALL_ACTIVITIES'] =

    'VIEW ALL ACTIVITIES';

$lang['NEXT_EVENT'] =

    'NEXT EVENT';

$lang['NEXT_EVENT_NAME'] =

    'Palo Alto Market';

$lang['NEXT_EVENT_DATE'] =

  'April 2nd & 3th';

$lang['VIEW_ALL_EVENTS'] =

    'MORE INFO';

$lang['SOMETHING_ABOUT_US'] =

    'SOMETHING ABOUT US';

$lang['NOW_IN'] =

    'NOW IN';

$lang['ASK_US_FOR_TIMES'] = 
     'ASK US FOR TIMES';

$lang['WHENEVER_YOU_LIKE'] =
     'WHENEVER YOU LIKE!';

/*
  ------------------
  FOOD & DRINKS
  ------------------
 */

$lang['INTRO_TEXT_FD'] =

    'Healthy “fast food” <br/>with the best quality';

$lang['INTRO_TEXT_2_FD'] =

    'since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['DOWNLOAD_MENU'] =

 'DOWNLOAD A PDF<br/> OF OUR MENU';

$lang['FD_LAST_TEXT_TOP'] =

  'ARE U HUNGRY?';

$lang['FD_LAST_TEXT_BOTTOM'] =

  'COME and VISIT US';

$lang['PDF_MENU'] =

    '/MENU_SHB_INGLES.pdf';

$lang['PDF_BEBIDAS'] =

    '/BEBIDAS_SHB_INGLES.pdf';

/*
  ------------------
  ACTIVITIES
  ------------------
 */

$lang['AC_TEXT'] =

    'we want you <br/>to have a full experience!
                    <p>We offer DAILY activities such as paddle surf, BEACH workouts to stay in shape, RUNNING, surfing championship broadcasts...</p>';

$lang['ACTIVITIES'] =

    'ACTIVITIES';

$lang['AUTUM_WINTER'] =

    'AUTUM / WINTER';

$lanf['SPECIAL_EVENTS'] =

    'SPECIAL <span>EVENTS</span>';

$lang['DOWNLOAD_PDF'] =

    'DOWNLOAD PDF';

$lang['AC_LAST_TEXT_TOP'] =

    '<span>EAT GOOD,</span>';

$lang['AC_LAST_TEXT_BOTTOM'] =

    '<span>LOOK GOOD, FEEL GOOD</span>';

$lang['TODAY'] =

    'TODAY!';

$lang['PDF_ACTIVITIES'] =

    '/Agenda-SHB(ENG).pdf';

/*
  ------------------
  ABOUT
  ------------------
 */

$lang['ABOUT_SHB'] =

    'ABOUT SHB';

$lang['AB_TEXT_1'] =

    'SHB <span>YOUR BEACH CAFE</span>';

$lang['AB_TEXT_2'] =

    'Surf House Barcelona has its origins in California’s lifestyle and we want to give you something different to what you can find anywhere else in the city. Healthy “fast food” with the best quality; since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['AB_TEXT_3'] =

    'But SHB is not a place where you’ll only come to eat and drink, we want you to have a full experience! We offer activities such as paddle surf, workouts to stay in shape (SHB Insane Training), jogging on the beach boardwalk, live surfing championship broadcasts, brand launch events, surf school…';

$lang['AB_TEXT_4'] =

    'We hope Surf House Barcelona is the first of many more to come. We are young and we want to keep growing in every possible sense.';

$lang['AB_LAST_TEXT_TOP'] =

    "LIFE'S A WAVE,";

$lang['AB_LAST_TEXT_BOTTOM'] =

    "CATCH IT";

/*
  ------------------
  TEAM
  ------------------
 */
$lang['ANDREA_LUELMO_TEXT'] =

  'Andrea has one of the best surfing styles of our coast. Always with a big smile on her face, she&#39;s a cool girl loved by everyone. 

She teaches surf lessons at Pukas Surf Eskola and you can see her surfing the Barcelona beaches whenever there are waves. She travels often to Zarautz and Indonesia, and her favourite place out of the water is, obviously, Surf House Barcelona.';

$lang['CLARIS_CARRASCO_TEXT'] =

  'Being half Korean and a Barceloneta local is pretty special. Claris is one of our favorite riders. She started surfing in Razo (Galicia) when she was 15 and hasn&#39;t stopped getting better since. You&#39;ll find her in the water or ripping at the skatepark whenever she&#39;s not studying. Her awesome skating plays a big roll in her surfing style, always trying some radical move in the water  and surprising everyone. We suspect she might be getting all her power thanks to our smoothies and juices.';

$lang['ALBERT_HERNANDEZ_TEXT'] =

  'Galicia’s Junior Champion in 2007, Albert is the first Catalan surfer that has won a circuit outside our territory. And doing so in a place with such good surfing like Galicia is astounding.  He’s back in the game after a couple years focusing mainly on work. Humble and the nicest guy.';

$lang['ALEX_KNOPFEL_TEXT'] =

  'His passion for surfing led him to create Surf House Barcelona, inspired in California, where he lived fora  a little bit. A bit of a workaholic and always thinking of new smoothies or tasty dishes,  he escapes to go surf whenever he has a spare minute, whether it’s in front of SHB or embarking on a road trip / boarding a plane to wherever waves are pumping.';

$lang['CARLES_MEDINA_TEXT'] =

  'Carles Medina, an innovative surfer and the creative mind behind the brand MBC, is known for his good vibe and big smile in and out of the water. The time he spent in Australia enriched his radical surfing and inspired him to bring back to his home town a project with a very bright future.';

$lang['PABLO_VIAPLANA_TEXT'] =

  'Le Cucut. That’ss our favorite DJ’s (and most special surfer) artistic name. When not in the water, we’ll find him spinning the funnest music at places like Surf House or clubs like Be Cool.  He’s not your regular DJ, the kid’s got something special. Stay tuned and you’ll see how far he gets. ';

/* FOOD */

$lang['PATATAS'] =

  'French<br/>Fries';

$lang['COSTILLAS_BBQ'] =

  'Pork Ribs';

$lang['HELADOS_SHB'] =

  'SHB<br/>Ice cream';

$lang['GOFRE_NUTELLA'] =

  'Nutella<br/>Waffle';

/* DRINK */

$lang['LILA'] =

  'Purple';

$lang['AGUA_DE_COCO'] =

  'Coconut<br/>Water';

$lang['TU_CREACION'] =

  'Your own<br/>creation';


/*
  ------------------
  SCHEDULE
  ------------------
 */


$lang['HORARIO'] =

  'SCHEDULE: MONDAY: CLOSED';

$lang['MIERCOLES'] =

  'TUESDAY - THURSDAY: 13h to 20h';

$lang['VIERNES'] =

  'FRIDAY - SATURDAY: 11h to 00h';

$lang['DOMINGO'] =

  'SUNDAY: 11h to 20h';

?>