<?php
/*
  ------------------
  Language: Castellano
  ------------------
 */

$lang = array();

$lang['IDIOMA'] =

    'es';

$lang['VIEW_OUR_MENU'] =

    'VER NUESTRO MENÚ';

$lang['HIGHLIGHTED_ACTIVITY'] =

    'ACTIVIDAD DESTACADA';

$lang['TODAY_AT'] =

    'HOY A LAS';

$lang['VIEW_ALL_ACTIVITIES'] =

    'VER LAS ACTIVIDADES';

$lang['NEXT_EVENT'] =

    'SIGUIENTE EVENTO';
    
$lang['NEXT_EVENT_NAME'] =

    'Palo Alto Market';

$lang['NEXT_EVENT_DATE'] =

    '02 y 03 de Abril';

$lang['VIEW_ALL_EVENTS'] =

    'MÁS INFORMACIÓN';

$lang['SOMETHING_ABOUT_US'] =

    'SOBRE NOSOTROS';

$lang['NOW_IN'] =

    'AHORA EN LA';

$lang['ASK_US_FOR_TIMES'] = 
     '¡PREGÚNTANOS!';

$lang['WHENEVER_YOU_LIKE'] =
     '¡CUANDO TÚ QUIERAS!';

/*
  ------------------
  FOOD & DRINKS
  ------------------
 */

$lang['INTRO_TEXT_FD'] =

    '“Fast food” pero “healthy” y de mucha calidad.';

$lang['INTRO_TEXT_2_FD'] =

    'Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. ¡no es broma! ';

$lang['DOWNLOAD_MENU'] =

    'DESCARGA NUESTRA<br/>CARTA en PDF';

$lang['FD_LAST_TEXT_TOP'] =

  'ARE U HUNGRY?';

$lang['FD_LAST_TEXT_BOTTOM'] =

  'COME and VISIT US';

$lang['PDF_MENU'] =

    '/MENU_SHB_CASTELLANO.pdf';

$lang['PDF_BEBIDAS'] =

    '/BEBIDAS_SHB_CASTELLANO.pdf';

/*
  ------------------
  ACTIVITIES
  ------------------
 */

$lang['AC_TEXT'] =

    '¡queremos que sea toda una experiencia!
    <p>Ofrecemos actividades diarias como paddle surf, workout en la playa para estar en forma, running, broadcast de campeonatos de surf…</p>';

$lang['ACTIVITIES'] =

    'ACTIVIDADES';

$lang['AUTUM_WINTER'] =

    'OTOÑO / INVIERNO';

$lanf['SPECIAL_EVENTS'] =

    'EVENTOS <span>ESPECIALES</span>';

$lang['DOWNLOAD_PDF'] =

    'DESCARGAR PDF';

$lang['AC_LAST_TEXT_TOP'] =

    '<span>EAT GOOD,</span>';

$lang['AC_LAST_TEXT_BOTTOM'] =

    '<span>LOOK GOOD, FEEL GOOD</span>';
$lang['TODAY'] =

    '¡HOY!';

$lang['PDF_ACTIVITIES'] =

    '/Agenda-SHB(ESP).pdf';

/*
  ------------------
  ABOUT
  ------------------
 */

$lang['ABOUT_SHB'] =

    'SOBRE SHB';

$lang['AB_TEXT_1'] =

    'SHB <span>YOUR BEACH CAFE</span>';

$lang['AB_TEXT_2'] =

    'Surf House Barcelona nace del estilo de vida californiano, con ganas de ofrecer algo diferente a lo existente. “Fast food” pero “healthy” y de mucha calidad. Desde que nos despertamos, con los desayunos, hasta que nos vamos a la cama, con los mejores cocktails que hayas probado nunca. ¡no es broma!';

$lang['AB_TEXT_3'] =

    'Pero SHB no es solo un lugar en el que comer y beber, queremos que sea una experiencia. Ofrecemos actividades como paddle surf, entrenos "SHB insane training" (cardio-tono) para estar en forma, running, retransmisiones de campeonatos de surf en directo, eventos, presentaciones de marcas, escuela de surf… ';

$lang['AB_TEXT_4'] =

    'Esperamos que Surf House Barcelona sea el primero de muchos. Somos jóvenes y queremos seguir creciendo en todos los sentidos.   ';

$lang['AB_LAST_TEXT_TOP'] =

    "LIFE'S A WAVE,";

$lang['AB_LAST_TEXT_BOTTOM'] =

    "CATCH IT";


/*
  ------------------
  TEAM
  ------------------
 */

$lang['ANDREA_LUELMO_TEXT'] =

  'Andrea es una de las surfistas con más estilo y clase en el agua de nuestra costa. Siempre que hay olas y su trabajo lo permite, podemos verla en las playas de Barcelona y alrededores. Desde que empezó a trabajar en Pukas Surf Eskola de Barcelona se aficionó mucho más a lo que ahora es su pasión. A la que puede se escapa a Zarautz o a Indonesia y cuando no hay olas en casa, su segundo lugar favorito para pasar el rato y reunirse con amig@s es SHB.';


$lang['ALEX_KNOPFEL_TEXT'] =

  'Su pasión por el Surf le llevó a crear Surf House Barcelona. Siempre pensando en nuevos platos o zumos y cuando puede, se escapa a surfear ya sea delante de su trabajo o escapándose de viaje a cualquier lugar con olitas.';

$lang['CARLES_MEDINA_TEXT'] =

  'Carles Medina, surfer innovador y creativo de la marca MBC es un joven con un surf radical que  caracteriza por su buen rollo y  sonrisa permanente en la cara tanto dentro como fuera del agua. Su estancia en Australia le hizo volver a su ciudad natal con mucho mas surfing y con un proyecto que hoy en día es muy prometedor.';

$lang['CLARIS_CARRASCO_TEXT'] =

  'Con raíces Koreanas y local de Barceloneta es una de nuestras riders favoritas. Se inició en la playa de Razo, Galicia y desde entonces no ha dejado de entrar al agua y progresar. Siempre que no está en clase la vemos en el agua o en el skatepark. dejándose las rodillas. Surfea desde los 15 añitos y su afición por el patín le ayuda a progresar dentro del agua. Siempre busca hacer alguna maniobra radical y cada día que la vemos en el agua nos sorprende más. Serán nuestros zumitos que le dan ese power?';



$lang['PABLO_VIAPLANA_TEXT'] =

  'Le Cucut, así se hace llamar nuestro Dj preferido y surfista peculiar. Si no está en el agua lo podemos ver poniendo buenísima música como “Mambo Number 5” en la terraza de SHB o en locales con Be Cool, en Barcelona. No es un DJ normal, tiene algo especial. El tiempo lo dirá, atentos.';


$lang['ALBERT_HERNANDEZ_TEXT'] =

  'Campeón Junior en Galicia en 2007. El primer catalán en salir y ganar un circuito en un lugar como Galicia donde hay buen surf. Tuvo unos añitos de parón por trabajo pero ha vuelto muy fuerte: Humilde, simpático y muy motivador para ir a la playa qye tenga lo mínimo para practicar el deporte que más nos mueve a todos.';

/* COMIDA */

$lang['PATATAS'] =

    'Patatas';

$lang['COSTILLAS_BBQ'] =

    'Costillas<br/>BBQ';

$lang['HELADOS_SHB'] =

    'Helados SHB';

$lang['GOFRE_NUTELLA'] =

    'Gofre<br/>Nutella';

/* BEBIDA */

$lang['LILA'] =

    'Lila';

$lang['AGUA_DE_COCO'] =

    'Agua <br/>de Coco';

$lang['TU_CREACION'] =

    'Tu Creación';

/*
  ------------------
  SCHEDULE
  ------------------
 */


$lang['HORARIO'] =

  'HORARIO: LUNES: CERRADO';

$lang['MIERCOLES'] =

  'MARTES - JUEVES: 13h a 20h';

$lang['VIERNES'] =

  'VIERNES - SÁBADO: 11h a 00h';
  
$lang['DOMINGO'] =

  'DOMINGO: 11h a 20h';
  
?>
