<?php
/*
  ------------------
  Language: English
  ------------------
 */

$lang = array();

$lang['IDIOMA'] =

    'en';

$lang['VIEW_OUR_MENU'] =

    'VIEW OUR MENU';

$lang['HIGHLIGHTED_ACTIVITY'] =

    'HIGHLIGHTED ACTIVITY';

$lang['TODAY_AT'] =

    'TODAY AT';

$lang['VIEW_ALL_ACTIVITIES'] =

    'VIEW ALL ACTIVITIES';

$lang['NEXT_EVENT'] =

    'NEXT EVENT';

$lang['NEXT_EVENT_NAME'] =

    'SOUNDEAT';

$lang['NEXT_EVENT_DATE'] =

  'MAY 14TH & 15TH';

$lang['VIEW_ALL_EVENTS'] =

    'MORE INFO';

$lang['SOMETHING_ABOUT_US'] =

    'SOMETHING ABOUT US';

$lang['NOW_IN'] =

    'NOW IN';

$lang['ASK_US_FOR_TIMES'] =
     'ASK US';

$lang['WHENEVER_YOU_LIKE'] =
     'WHENEVER YOU LIKE!';

$lang['MORE_INFO'] =
     'MORE INFO';

$lang['VER_EN_GOOGLE_MAPS'] =
    'View on Google Maps';
/*
  ------------------
  FOOD & DRINKS
  ------------------
 */

$lang['INTRO_TEXT_FD'] =

    'Casual, different and&nbsp;quality food';

$lang['INTRO_TEXT_2_FD'] =

    'since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['DOWNLOAD_MENU'] =

    'DOWNLOAD A PDF OF&nbsp;OUR&nbsp;MENU';

$lang['FD_LAST_TEXT_TOP'] =

  'ARE U HUNGRY?';

$lang['FD_LAST_TEXT_BOTTOM'] =

  'COME and VISIT US';

$lang['PDF_MENU'] =

    'MENU_SHB_INGLES.pdf';

$lang['PDF_BEBIDAS'] =

    'BEBIDAS_SHB_INGLES.pdf';

$lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA'] =
    'Download a pdf<br>of our menu';

$lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_1'] =
    '/pdf/MENU_SHB_INGLES.pdf';

$lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_2'] =
    '/pdf/Carta-BRUNCH.pdf';

$lang['FOOD_DRINKS__DESCARGA_NUESTRA_CARTA_LINK_3'] =
    '/pdf/Carta-DINNER.pdf';

/*
  ------------------
  ACTIVITIES
  ------------------
 */

$lang['AC_TEXT'] =

    'we want you <br/>to have a full experience!
                    <p>We offer DAILY activities such as paddle surf, BEACH workouts to stay in shape, RUNNING, surfing championship broadcasts...</p>';

$lang['ACTIVITIES'] =

    'Activities';

$lang['AUTUM_WINTER'] =

    'SPRING / SUMMER';

$lanf['SPECIAL_EVENTS'] =

    'SPECIAL <span>EVENTS</span>';

$lang['DOWNLOAD_PDF'] =

    'DOWNLOAD PDF';

$lang['AC_LAST_TEXT_TOP'] =

    '<span>EAT GOOD,</span>';

$lang['AC_LAST_TEXT_BOTTOM'] =

    '<span>LOOK GOOD, FEEL GOOD</span>';

$lang['TODAY'] =

    'TODAY!';

$lang['PDF_ACTIVITIES'] =

    'Agenda-SHB(ENG).pdf';

$lang['AC_LOC_TEXT_TOP'] =

    'SEND US AN EMAIL TO RESERVE ANY ACTIVITY';

$lang['AC_LOC_MAIL'] =

    'actividades@surfhousebarcelona.com';

$lang['RESERVE'] =

    'RESERVE!';
/*
  ------------------
  ABOUT
  ------------------
 */

$lang['ABOUT_SHB'] =

    'ABOUT SHB';

$lang['AB_TEXT_1'] =

    'SHB <span>YOUR BEACH CAFE</span>';

$lang['AB_TEXT_2'] =

    'Surf House Barcelona has its origins in California’s lifestyle and we want to give you something different to what you can find anywhere else in the city. Casual, different and quality food. since we wake up, with delicious and healthy breakfasts, until we are ready to go to bed, offering you the best fresh cocktails you have ever tried… not kidding!';

$lang['AB_TEXT_3'] =

    'But SHB is not a place where you’ll only come to eat and drink, we want you to have a full experience! We offer activities such as paddle surf, workouts to stay in shape (SHB Insane Training), jogging on the beach boardwalk, live surfing championship broadcasts, brand launch events, surf school…';

$lang['AB_TEXT_4'] =

    'We hope Surf House Barcelona is the first of many more to come. We are young and we want to keep growing in every possible sense.';

$lang['AB_LAST_TEXT_TOP'] =

    "LIFE'S A WAVE,";

$lang['AB_LAST_TEXT_BOTTOM'] =

    "CATCH IT";

$lang['CONTACT'] =

    "Contact";

$lang['MONDAY_AND_TUESDAY'] =

        "Monday and Tuesday";

$lang['WEDNESDAY_AND_THURSDAY'] =

        "Wednesday and Thursday";

$lang['FRIDAY_AND_SATURDAY'] =

        "Friday and Saturday";

$lang['SUNDAY'] =

        "Sunday";

$lang['MONDAY'] =

        "Monday";

$lang['TUESDAY_TO_THURSDAY'] =

        "Tuesday to Thursday";

$lang['FRIDAY'] =

        "Friday";

$lang['SATURDAY'] =

        "Saturday";

$lang['to'] =

        "to";

$lang['AND'] =

        "and";

$lang['CLOSED'] =

        "Closed";



/*
  ------------------
  TRIPS
  ------------------
 */

$lang['TRIPS_SHB'] =

    'TRIPS';

$lang['TRIPS_TEXT_1'] =

    'SHB <span>CUSTOM SURF TRIPS</span>';

$lang['TRIPS_TEXT_2'] =

    'SHB Trips is our own platform to create your perfect surf trip.';

$lang['TRIPS_TEXT_3'] =

    'We are surfers and traveling, meeting people and sharing experiences is our passion.';

$lang['TRIPS_TEXT_4'] =

    'We have been doing this for years and know the places we are going to very well.';

$lang['TRIPS_TEXT_5'] =

    'Paradises with perfect weaves, warmth and safety.';

$lang['TRIPS_TEXT_6'] =

    'All our trips include a photographer and a local guide, to make them unforgettable experiences.';

$lang['TRIPS_TEXT_7'] =

    'Join us for our next adventure or let us know where or how your dream trip is and we’ll organize it for you.';

$lang['TRIPS_TEXT_8'] =

    'Have a nice trip!';

$lang['TRIPS_VIDEOS'] =

    "VIDEOS";

$lang['TRIPS_LAST_TEXT_TOP'] =

    "SURF, EAT, SLEEP...";

$lang['TRIPS_LAST_TEXT_BOTTOM'] =

    "¡REPEAT!";

$lang['TRIPS_INFO_1'] =

    "SEND US AN EMAIL FOR MORE INFORMATION";

$lang['TRIPS_INFO_2'] =

    "ACTIVIDADES@SURFHOUSEBARCELONA.COM";
/*
  ------------------
  SHOP
  ------------------
 */

$lang['SHB_SHOP'] =

    'SURF HOUSE SHOP';

$lang['SH_SHOP'] =

    'SH SHOP';

/*
  ------------------
  TEAM
  ------------------
 */
$lang['ANDREA_LUELMO_TEXT'] =

  'Andrea has one of the best surfing styles of our coast. Always with a big smile on her face, she&#39;s a cool girl loved by everyone.

She teaches surf lessons at Pukas Surf Eskola and you can see her surfing the Barcelona beaches whenever there are waves. She travels often to Zarautz and Indonesia, and her favourite place out of the water is, obviously, Surf House Barcelona.';

$lang['CLARIS_CARRASCO_TEXT'] =

  'Being half Korean and a Barceloneta local is pretty special. Claris is one of our favorite riders. She started surfing in Razo (Galicia) when she was 15 and hasn&#39;t stopped getting better since. You&#39;ll find her in the water or ripping at the skatepark whenever she&#39;s not studying. Her awesome skating plays a big roll in her surfing style, always trying some radical move in the water  and surprising everyone. We suspect she might be getting all her power thanks to our smoothies and juices.';

$lang['ALBERT_HERNANDEZ_TEXT'] =

  'Galicia’s Junior Champion in 2007, Albert is the first Catalan surfer that has won a circuit outside our territory. And doing so in a place with such good surfing like Galicia is astounding.  He’s back in the game after a couple years focusing mainly on work. Humble and the nicest guy.';

$lang['ALEX_KNOPFEL_TEXT'] =

  'His passion for surfing led him to create Surf House Barcelona, inspired in California, where he lived fora  a little bit. A bit of a workaholic and always thinking of new smoothies or tasty dishes,  he escapes to go surf whenever he has a spare minute, whether it’s in front of SHB or embarking on a road trip / boarding a plane to wherever waves are pumping.';

$lang['CARLES_MEDINA_TEXT'] =

  'Carles Medina, an innovative surfer and the creative mind behind the brand MBC, is known for his good vibe and big smile in and out of the water. The time he spent in Australia enriched his radical surfing and inspired him to bring back to his home town a project with a very bright future.';

$lang['PABLO_VIAPLANA_TEXT'] =

  'Le Cucut. That’ss our favorite DJ’s (and most special surfer) artistic name. When not in the water, we’ll find him spinning the funnest music at places like Surf House or clubs like Be Cool.  He’s not your regular DJ, the kid’s got something special. Stay tuned and you’ll see how far he gets. ';

/* FOOD */

$lang['PATATAS'] =

  'French<br/>Fries';

$lang['COSTILLAS_BBQ'] =

  'Pork Ribs';

$lang['HELADOS_SHB'] =

  'SHB<br/>Ice cream';

$lang['GOFRE_NUTELLA'] =

  'Nutella<br/>Waffle';

/* DRINK */

$lang['LILA'] =

  'Purple';

$lang['AGUA_DE_COCO'] =

  'Coconut<br/>Water';

$lang['TU_CREACION'] =

  'Your own<br/>creation';


/*
  ------------------
  SCHEDULE
  ------------------
 */


$lang['HORARIO'] =

 'SCHEDULE:';

$lang['MIERCOLES'] =

 'MONDAY AND TUESDAY: CLOSED';

$lang['VIERNES'] =

 'WEDNESDAY AND THURSDAY: 13h to 20h<br>FRIDAY AND SATURDAY: 12h to 24h';

$lang['DOMINGO'] =

 'SUNDAY: 12h to 22h';

 /*
  ------------------
  SECRET SPOT
  ------------------
 */

  $lang['SECRET-SPOT_SHB'] =

  'SECRET SPOT';

  $lang['SECRET-SPOT_TEXT_1'] =

  '6 years after the birth of Surf House Barcelona, Surf House Secret Spot is born.';

  $lang['SECRET-SPOT_TEXT_2'] =

  'A cozy and charming corner, like that hidden secret beach you only share with a few.';

  $lang['SECRET-SPOT_TEXT_3'] =

  'Same values, same goals, same stoke and same feel. Not as close to the beach and waves, but same good vibes and quality on each of its details.';

  $lang['SECRET-SPOT_TEXT_4'] =

  'Sport activities in surrounding parks, weekly events, live surfing contest projections and many new and exciting things will be cooked in this new magic spot.';

  $lang['SECRET-SPOT_VER_CARTA'] =

  'Dinner Menu';

  $lang['SECRET-SPOT_VER_CARTA_BRUNCH'] =

  'Brunch Menu';

  $lang['SECRET-SPOT_LAST_TEXT_BOTTOM_1'] =

  'come and discover';

  $lang['SECRET-SPOT_LAST_TEXT_BOTTOM_2'] =

  'what it hides!';

     /*
  ------------------
  HOME MOBILE
  ------------------
 */
  $lang['VER_CARTA'] =

  'View Menu';

  $lang['PDF_CARTA_barceloneta_2020'] =

  'http://carta-en.surfhousebarcelona.com';

  $lang['PDF_CARTA_secretspot_2020'] =

  'http://carta-en.surfhousebarcelona.com';

?>
