<ul class="menu-bottom">
  <li class="blink animation-DownToTop animation-delay-7">
    <a href="http://www.surfhousebarcelona.com/home/">HOME</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-8">
    <a href="http://www.surfhousebarcelona.com/fooddrinks/">FOOD&DRINKS</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-9">
    <a href="http://www.surfhousebarcelona.com/activities/"><?php echo $lang['ACTIVITIES'] ?></a>
  </li>
  <li class="blink animation-DownToTop animation-delay-10">
    <a href="http://www.surfhousebarcelona.com/about/"><?php echo $lang['ABOUT_SHB'] ?></a>
  </li>
  <li class="blink animation-DownToTop animation-delay-11">
    <a href="http://www.surfhousebarcelona.com/onwheels/">ON WHEELS</a>
  </li>
  <li class="blink animation-DownToTop animation-delay-12">
    <a href="http://surfhouse-shop.com/" target="_blank">SH SHOP</a>
  </li>
</ul>
