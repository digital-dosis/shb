;( function( window ) {

var fsvs =$.fn.fsvs({
    speed           : 800,
    mouseSwipeDisance : 60,
    detectHash      : false
});

$('#down').click(function(event) {
  fsvs.slideDown();
});

var showEvents = false;

$('.hide-events').click(function(event) {
    if ( showEvents ) {
        $('#next-events-wrapper').fadeIn();
    } else {
        $('#next-events-wrapper').fadeOut();
    }

    showEvents = !showEvents;
});

$('#events-slide').slippry({

  adaptiveHeight: true,
  pager: false,
  controls: false,
  autoHover: false,
  transition: 'fade',
  speed: 2000,
  useCSS: true,
});

$('#wheels-slide').slippry({
  pause: 4000,
  adaptiveHeight: true,
  pager: false,
  controls: false,
  autoHover: false,
  transition: 'horizontal',
  speed: 500,
  useCSS: true
});

} )( window );