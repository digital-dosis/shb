<?php require_once("../admin/bootstrap.php");?>
<?php require_once("common.php");?>
<!DOCTYPE html>
<html lang="en" class="no-js fsvs">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<meta name="description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterranea, con algunas sopresas o versiones mas “street” de nuestros clásico" />
	<meta name="keywords" content="Surf House, Barcelona, foodtruck, street food, burgers, surf, on wheels" />

	<meta property="og:title" content="Surf House On Wheels" />
	<meta property="og:description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterránea, con algunas sorpresas o versiones mas “street” de nuestros clásico" />
	<meta property="og:image" content="http://www.surfhousebarcelona.com/onwheels/img/fb-wheels.png" />
	<meta property="og:url" content="http://www.surfhousebarcelona.com/onwheels/" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@DigitalDosis" />
	<meta name="twitter:title" content="Surf House On Wheels" />
	<meta name="twitter:description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterranea, con algunas sopresas o versiones mas “street” de nuestros clásico" />
	<meta name="twitter:image" content="http://www.surfhousebarcelona.com/onwheels/img/tw-1325.png" />

	<link rel="stylesheet" type="text/css" href="css/normalize" />
	<link rel="stylesheet" type="text/css" href="css/slippry.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/animations_all.css" />
	<link rel="shortcut icon" href="shb-favicon.png" />

	<script type="text/javascript" src="js/libraries/jquery-2.1.4.min.js,libraries/respimage.min.js,libraries/slippry.min.js,libraries/modernizr.custom.js"></script>
	<script type="text/javascript" src="js/libraries/classie.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>

	<script type="text/javascript" src="js/vendor/jquery.easings.min.js"></script>
	<script type="text/javascript" src="js/vendor/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="js/vendor/jquery.fullPage.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css">
	<script type="text/javascript" src="js/onwheels.js"></script>

	<title><?php echo $lang['ONWHEELS_TITLE']; ?></title>
</head>

<body id="fsvs-body">

	<div class="orientation">
		<img src="img/landscape.jpg" alt="Rotate Device">
	</div>

	<div id="page-loader">
		<img src="img/shb.gif" class="loading-gif">
	</div>

	<?php include 'menu.php' ?>

	<a id="trigger-overlay" class="menu-button" href="javascript:void('0');">
	   <span class="desktop">MENU <span></span></span>
	   <span class="mobile"><img src="img/menu.png" alt="Menu"></span>
	</a>

	<div>
		<div id="fullpage" class="fullpage wheels-content">


			<!-- COVER SECTION-->
			<div class="section " id="section0">
				<div id="portada">
					<div data-offset="2" class="poster-wrapper">
						<div class="poster">
							<div>
								<div data-offset="5" class="layer-bg"></div>
							</div>
							<div data-offset="10" class="layer-title portada-title">
								<img class="portada-title layer-" src="img/onwheels.png" srcset="img/onwheels.png 1600w, img/onwheels@2x.png 2560w" alt="ONWHEELS" sizes="(max-width: 1280px) 1600px">
							</div>
						</div>
					</div>
					<div class="down">
						<a href="javascript:void('0');" id="down" class="blink">
							<img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow@2x.png 2560w" sizes="(max-width: 1280px) 1600px">
						</a>
						<span></span>
					</div>
				</div>
			</div>


			<!-- INTRO SECTION-->
			<div class="section slide-1" id="section1">
				<div class="center-wrapper">
					<div class="green-border"></div>
					<h1><?php echo $lang['ONWHEELS_H1']; ?></h1>
					<p><?php echo $lang['ONWHEELS_P_1']; ?></p>
					<img src="img/shb-1.jpg" srcset="img/shb-1.jpg 1600w, img/shb-1@2x.jpg 2560w">
				</div>
			</div>


			<!-- SLIDE H 1 - EVENTS SECTION-->
			<div class="section events-content" id="section2">
				<div class="slideshow events-slideshow">
					<ul id="events-slide">
						<li>
							<img src="img/events/whitesummer.jpg" srcset="img/events/whitesummer-small.jpg 960w, img/events/whitesummer.jpg 1600w, img/events/whitesummer@2x.jpg 2560w" alt="">
							<div class="events-caption">
								<h3><?php echo $lang['LAST_EVENTS']; ?></h3>
								<p>WHITE SUMMER</p>
							</div>
						</li>
						<li>
							<img src="img/events/primaverapro.jpg" srcset="img/events/primaverapro-small.jpg 960w, img/events/primaverapro.jpg 1600w, img/events/primaverapro@2x.jpg 2560w" alt="">
							<div class="events-caption">
								<h3><?php echo $lang['LAST_EVENTS']; ?></h3>
								<p>PRIMAVERA PRO</p>
							</div>
						</li>
						<li>
							<img src="img/events/summerparty.jpg" srcset="img/events/summerparty-small.jpg 960w, img/events/summerparty.jpg 1600w, img/events/summerparty@2x.jpg 2560w" alt="">
							<div class="events-caption">
								<h3><?php echo $lang['LAST_EVENTS']; ?></h3>
								<p>SHB SUMMER PARTY</p>
							</div>
						</li>
					</ul>
				</div>



			<!-- NEXT EVENTS -->

			<div id="next-events-wrapper">
				<span class="next-event">
				<!--	<div>
					    <h3>NEXT EVENT</h3>
					    <span></span>
					</div>
				</span>

				
				<div class="event">
					<h3>World Padel Tour</h3>
					<p>29 al 04 de</p>
					<p class="month">Junio</p>
					<span class="event-line"></span>
					<a href="https://www.google.es/maps/place/Real+Club+de+Polo+de+Barcelona/@41.3816808,2.1114273,15z/data=!4m2!3m1!1s0x0:0x225943abaa0899f3?sa=X&ved=0ahUKEwjShKScobXSAhXLSxoKHbRHCbgQ_BIIlAUwZw" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1600w, img/marker@2x.png 2560w">
						Barcelona</p></a>
						<a href="http://www.worldpadeltour.com/torneos/" target="_blank" class="event-moreinfo">
							<img src="img/mas.png" srcset="img/mas.png 1600w, img/mas@2x.png 2560w">
						</a>
				</div>-->
			</div>
			</div> 







			<!-- SLIDE H 2 SECTION-->
			<div class="section" id="section3">
				<div class="slideshow wheels-slideshow">
					<ul id="wheels-slide">
						<li>
							<img src="img/slide/1.jpg" srcset="img/slide/1-small.jpg 960w, img/slide/1.jpg 1600w, img/slide/1@2x.jpg 2560w" alt="">
							<span class="wheels-slide-caption"></span>
						</li>
						<li>
							<img src="img/slide/2.jpg" srcset="img/slide/2-small.jpg 960w, img/slide/2.jpg 1600w, img/slide/2@2x.jpg 2560w" alt="">
							<span class="wheels-slide-caption"></span>
						</li>
						<li>
							<img src="img/slide/3.jpg" srcset="img/slide/3-small.jpg 960w, img/slide/3.jpg 1600w, img/slide/3@2x.jpg 2560w" alt="">
							<span class="wheels-slide-caption"></span>
						</li>
					</ul>
				</div>
			</div>



			<!-- CONTACT SECTION-->

			<div class="section wheels-contacto" id="section4">
				<div class="center-wrapper">
					<span class="line animation-DownToTop animation-delay-5"></span>
					<h1>
						<span class="top animation-DownToTop animation-delay-4"><?php echo $lang['HIRE_US']; ?></span>
						<span class="bottom animation-DownToTop animation-delay-6"><?php echo $lang['FOR_YOUR_EVENT']; ?></span>
					</h1>
					<div class="location animation-DownToTop animation-delay-7">
						<p>
							<a href="mailto:truck@surfhousebarcelona.com" class="blink">
								<b class="email">truck@surfhousebarcelona.com</b>
							</a><br />
							<b class="telephone">+34 618 141 764</b>
						</p>
					</div>
				</div>



				<!-- MENU -->
        <?php include('inc/footer/menu.php'); ?>

        <!-- CREDITS-->
        <?php include('../pages/inc/footer/credits.php'); ?>


			</div>
		</div>
	</div>
	<style type="text/css">
	html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
	html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
		border-color: #4A1D88;
	}
	html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
		background: #4A1D88;
	}
	</style>

	<script type="text/javascript">
	$(window).load(function() {
		$('#page-loader').addClass('hide-animation');
		$('body').addClass('loaded');
	});
	</script>
</body>
</html>
