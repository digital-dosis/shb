<div class="overlay overlay-hugeinc">
    <a href="javascript:void('0');" class="overlay-close opacity-hover">Close</a>

    <nav>
        <ul>
            <li>
                    <a href="http://surfhousebarcelona.com/">
                        HOME
                    </a>
                </li>
                <li>
                    <a href="http://surfhousebarcelona.com/fooddrinks">
                        FOOD&DRINKS
                    </a>
                </li>
                <li>
                    <a href="http://surfhousebarcelona.com/activities">
                        ACTIVIDADES
                    </a>
                </li>
                <li>
                    <a href="http://surfhousebarcelona.com/about">
                        SOBRE SHB
                    </a>
                </li>
                <li>
                    <a href="http://www.surfhousebarcelona.com/onwheels/">
                        ON WHEELS
                    </a>
                </li>
                <li>
                    <span class="border"></span>
                    <a href="http://surfhouse-shop.com/" target="_blank">
                        SH SHOP
                    </a>
                </li>
        </ul>
    </nav>
    <a href="http://www.digitaldosis.com" target="_blank" class="firma-menu">
        DIGITAL DOSIS WEB DESIGN
    </a>

    <div class="menu-footer">
        <ul>
            <li>
                HORARIO: LUNES / MARTES: CERRADO
            </li>
            <li>
                MIÉRCOLES - VIERNES: 12h - 01h
            </li>
            <li>
                SÁBADO: 11h - 01h  DOMINGO: 11h - 23h
            </li>
        </ul>

        <ul>
            <li>
                <a href="mailto:hello@surfhousebarcelona.com">
                    HELLO@SURFHOUSEBARCELONA.COM
                </a>
            </li>
            <li>
                T. 932 507 023
            </li>
            <li>
                <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank">
                    C/ L’ALMIRALL AIXADA 22 08003 BARCELONA
                </a>
            </li>
        </ul>
    </div>

</div>
