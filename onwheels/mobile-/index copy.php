<!DOCTYPE html>
<html lang="en" class="no-js fsvs">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterranea, con algunas sopresas o versiones mas “street” de nuestros clásico" />
		<meta name="keywords" content="Surf House, Barcelona, foodtruck, street food, burgers, surf, on wheels" />

		<meta property="og:title" content="Surf House On Wheels" />
		<meta property="og:description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterránea, con algunas sorpresas o versiones mas “street” de nuestros clásico" />
		<meta property="og:image" content="http://www.surfhousebarcelona.com/onwheels/img/fb-wheels.png" />
		<meta property="og:url" content="http://www.surfhousebarcelona.com/onwheels/" />

	    <meta name="twitter:card" content="summary_large_image" />
	    <meta name="twitter:site" content="@DigitalDosis" />
	    <meta name="twitter:title" content="Surf House On Wheels" />
	    <meta name="twitter:description" content="Surf House on Wheels ha nacido para llevar el concepto de Surf House Barcelona tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterranea, con algunas sopresas o versiones mas “street” de nuestros clásico" />
	    <meta name="twitter:image" content="http://www.surfhousebarcelona.com/onwheels/img/tw-1325.png" />

		<link rel="stylesheet" type="text/css" href="css/normalize" />
		<link rel="stylesheet" type="text/css" href="css/slippry.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="shortcut icon" href="shb-favicon.png" />

		<script type="text/javascript" src="js/libraries/jquery-2.1.4.min.js,libraries/fsvs.js,libraries/respimage.min.js,libraries/slippry.min.js,libraries/modernizr.custom.js,libraries/classie.js"></script>
		<script type="text/javascript" src="http://www.surfhousebarcelona.com/onwheels/js/menu.js"></script>



		<title>SHB On Wheels</title>
	</head>
	<body>

		<div class="orientation">
			<img src="img/device-rotate.png" alt="Rotate Device">
		</div>

		<div id="page-loader">
			<img src="img/shb.gif" class="loading-gif">
		</div>

		<?php include 'menu.php' ?>

	    <a id="trigger-overlay" class="menu-button" href="javascript:void('0');">MENU <span></span></a>

		<div id="fsvs-body" class="wheels-content">
			<div class="slide">
				<div class="img-wrap">
				</div>
				<img src="img/onwheels.png" alt="ON WHEELS" class="portada-title">
				<div class="down">
					<a href="javascript:void('0');" id="down">
						<img src="img/down-arrow.png" srcset="img/down-arrow.png 1600w, img/down-arrow.png 2560w">
					</a>

					<span></span>
				</div>
			</div>
		    <div class="slide slide-1">
		        <div class="center-wrapper">
		            <div class="green-border"></div>
		            <h1>SURF HOUSE <span>ON WHEELS</span></h1>

		            <p>
		                Surf House on Wheels ha nacido para llevar el concepto de <b>Surf House Barcelona</b> tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana / mediterránea, con algunas sorpresas o versiones más “street” de nuestros clásico, más allá de la Barceloneta. En markets y mercadillos, eventos ‘street food’, bodas, fiestas privadas… donde la imaginación te lleve.
		                <br/><br/>
		                <span>Queremos conocerte, explorar nuevos terrenos y hacer muchos kilómetros. <b>On the road we go!</b></span>
		            </p>

		            <img src="img/shb-1.jpg" srcset="img/shb-1.jpg 1600w, img/shb-1@2x.jpg 2560w">

		        </div>
		    </div>
		    <div class="slide events-content">
		        <div class="slideshow events-slideshow">

		            <ul id="events-slide">
		              <li>
		                <img src="img/events/whitesummer.jpg" srcset="img/events/whitesummer-small.jpg 960w, img/events/whitesummer.jpg 1600w, img/events/whitesummer@2x.jpg 2560w" alt="">
		                <div class="events-caption">
		                    <h3>LAST EVENTS</h3>
		                    <p>WHITE SUMMER</p>
		                </div>
		              </li>
		              <li>
		                <img src="img/events/primaverapro.jpg" srcset="img/events/primaverapro-small.jpg 960w, img/events/primaverapro.jpg 1600w, img/events/primaverapro@2x.jpg 2560w" alt="">
		                <div class="events-caption">
		                    <h3>LAST EVENTS</h3>
		                    <p>PRIMAVERA PRO</p>
		                </div>
		              </li>
		              <li>
		                <img src="img/events/summerparty.jpg" srcset="img/events/summerparty-small.jpg 960w, img/events/summerparty.jpg 1600w, img/events/summerparty@2x.jpg 2560w" alt="">
		                <div class="events-caption">
		                    <h3>LAST EVENTS</h3>
		                    <p>SHB SUMMER PARTY</p>
		                </div>
		              </li>
		            </ul>
		        </div>

		        <div id="next-events-wrapper">
		            <span class="next-event">
		                <div>
		                    <h3>NEXT EVENT</h3>
		                    <span></span>
		                </div>
		            </span>


                   <div class="event">
                        <h3>Palo Alto</h3>
                        <p>02 y 03 de</p>
                        <p class="month">Abril</p>
                        <span class="event-line"></span>
                        <a href="https://www.google.es/maps?ion=1&espv=2&q=palo+alto&um=1&ie=UTF-8&sa=X&ved=0ahUKEwiel_Hf9pXLAhXMWRoKHc6fA8QQ_AUIBygB" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1600w, img/marker@2x.png 2560w">
                        Barcelona</p>
                        <a href="http://www.paloaltomarket.com/es/home" target="_blank" class="event-moreinfo">
                            <img src="img/mas.png" srcset="img/mas.png 1600w, img/mas@2x.png 2560w">
                        </a>
                    </div>
                   <div class="event">
                        <h3>Surfcity</h3>
                        <p>09 y 10 de</p>
                        <p class="month">Abril</p>
                        <span class="event-line"></span>
                        <a href="https://www.google.es/maps/place/F%C3%A0brica+Fabra+I+Coats/@41.432946,2.1906728,15z/data=!4m2!3m1!1s0x0:0x6bfdb8c8a071548c?sa=X&ved=0ahUKEwi4zN-U95XLAhWGiRoKHd3PC6oQ_BIIvgUwag" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1600w, img/marker@2x.png 2560w">
                        Barcelona</p>
                        <a href="http://surfcityfest.com/" target="_blank" class="event-moreinfo">
                            <img src="img/mas.png" srcset="img/mas.png 1600w, img/mas@2x.png 2560w">
                        </a>
                    </div>
                                       <div class="event">
                        <h3>Nomad</h3>
                        <p>29 y 30 de</p>
                        <p class="month">Abril</p>
                        <span class="event-line"></span>
                        <a href="http://www.cerverapaeria.cat/agenda/nomad-segarra" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1600w, img/marker@2x.png 2560w">
                        Cervera</p>
                        <a href="http://www.cerverapaeria.cat/agenda/nomad-segarra" target="_blank" class="event-moreinfo">
                            <img src="img/mas.png" srcset="img/mas.png 1600w, img/mas@2x.png 2560w">
                        </a>
                    </div>
		        </div>
		    </div>
		    <div class="slide slideshow-slide">
		        <div class="slideshow wheels-slideshow">
		            <ul id="wheels-slide">
		              <li>
		                <img src="img/slide/1.jpg" srcset="img/slide/1-small.jpg 960w, img/slide/1.jpg 1600w, img/slide/1@2x.jpg 2560w" alt="">
		                <span class="wheels-slide-caption"></span>
		              </li>
		              <li>
		                <img src="img/slide/2.jpg" srcset="img/slide/2-small.jpg 960w, img/slide/2.jpg 1600w, img/slide/2@2x.jpg 2560w" alt="">
		                <span class="wheels-slide-caption"></span>
		              </li>
		              <li>
		                <img src="img/slide/3.jpg" srcset="img/slide/3-small.jpg 960w, img/slide/3.jpg 1600w, img/slide/3@2x.jpg 2560w" alt="">
		                <span class="wheels-slide-caption"></span>
		              </li>
		            </ul>
		        </div>
		    </div>

		    <div class="slide wheels-contacto">
		    <div class="center-wrapper">
                <span class="line"></span>
                <h1>
                    <span class="top">HIRE US</span>
                    <span class="bottom">FOR YOUR EVENT!</span>
                </h1>
                <div class="location">
                    <p>
                    <a href="mailto:truck@surfhousebarcelona.com" class="blink">
                    <b>truck@surfhousebarcelona.com</b></a><br>
                    <b>+34 618 141 764</b>
                    </p>


                    
                </div>

            </div>



<ul class="menu-bottom">

                <li class="blink animation-DownToTop animation-delay-7">
                    <a href="http://surfhousebarcelona.com/">
                        HOME
                    </a>
                </li>
                <li class="blink animation-DownToTop animation-delay-8">
                    <a href="../fooddrinks/">
                        FOOD&amp;DRINKS
                    </a>
                </li>
                <li class="blink animation-DownToTop animation-delay-9">
                    <a href="../activities/">
                        ACTIVIDADES                    </a>
                </li>
                <li class="blink animation-DownToTop animation-delay-10">
                    <a href="../about/">
                        SOBRE SHB                    </a>
                </li>
                <li class="blink animation-DownToTop animation-delay-11">
                    <a href="http://www.surfhousebarcelona.com/onwheels/">
                        ON WHEELS
                    </a>
                </li>
                <li class="blink animation-DownToTop animation-delay-12">
                    <a href="http://surfhouse-shop.com/" target="_blank">
                        SH SHOP
                    </a>
                </li>
            </ul>




				<a href="http://www.digitaldosis.com" target="_blank" class="firma blink">
	                <span class="topline"></span>
	                Digital Dosis
	                <span>Web Design</span>
	            </a>
		    </div>
		</div>
		<style type="text/css">

		    html.fsvs body.active-slide-2 #fsvs-pagination li.active > span,
		    html.fsvs body.active-slide-2 #fsvs-pagination li.active > span > span {
		        border-color: #4A1D88;
		    }

		    html.fsvs body.active-slide-2 #fsvs-pagination li > span > span {
		        background: #4A1D88;
		    }
		</style>
		<script type="text/javascript" src="js/slideshow.js"></script>

		<script type="text/javascript">
		    $(window).load(function() {
		        $('#page-loader').addClass('hide-animation');
		        $('body').addClass('loaded');
		    });
		</script>
	</body>
</html>
