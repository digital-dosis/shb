<div id="fsvs-body" class="wheels-content">

    <div class="slide slide-1">

        <div class="center-wrapper">
            <div class="green-border"></div>
            <h1>SURF HOUSE <span>ON WHEELS</span></h1>

            <p>
                Surf House on Wheels ha nacido para llevar el concepto de <b>Surf House Barcelona</b> tan lejos como nos sea posible. Te llevamos nuestra deliciosa comida californiana/mediterranea, con algunas sopresas o versiones mas “street” de nuestros clásico, más allá de la Barceloneta. En markets y mercadillos, eventos ‘street food’, bodas, fiestas privadas… donde la imaginacion te lleve.
                <br/><br/>
                Queremos conocerte, explorar nuevos terrenos y hacer muchos kilometros. <b>On the road we go!</b>
            </p>

            <img src="img/shb-1.jpg" srcset="img/shb-1.jpg 1280w, img/shb-1@2x.jpg 2560w">

        </div>
    </div>
    <div class="slide events-content">
        <div class="slideshow events-slideshow">

        <a class="hide-events" href="javascript:void(0);"><img src="img/camera.png" srcset="img/camera.png 1280w, img/camera@2x.png 2560w"></a>

            <ul id="events-slide">
              <li>
                <img src="img/events/primaverapro.jpg" srcset="img/events/primaverapro.jpg 1280w, img/events/primaverapro@2x.jpg 2560w" alt="">
                <div class="events-caption">
                    <h3>PREVIOUS EVENTS</h3>
                    <p>PRIMAVERA PRO</p>
                </div>
              </li>
              <li>
                <img src="img/events/summerparty.jpg" srcset="img/events/summerparty.jpg 1280w, img/events/summerparty@2x.jpg 2560w" alt="">
                <div class="events-caption">
                    <h3>PREVIOUS EVENTS</h3>
                    <p>SHB SUMMER PARTY</p>
                </div>
              </li>
            </ul>
        </div>

        <div id="next-events-wrapper">
            <span class="next-event">
                <div>
                    <h3>NEXT EVENT</h3>
                    <span></span>
                </div>
            </span>

            <div class="event">
                <h3>WHITE SUMMER</h3>
                <p>del 8 al 23</p>
                <p class="month">AGOSTO</p>
                <span class="event-line"></span>
                <a href="" target="_blank" class="location">
                <img src="img/marker.png" srcset="img/marker.png 1280w, img/marker@2x.png 2560w">
                Pals (Girona)</p>
                <a href="" class="event-moreinfo">
                    <img src="img/mas.png" srcset="img/mas.png 1280w, img/mas@2x.png 2560w">
                </a>
            </div><div class="event">
                <h3>PALO ALTO MARKET</h3>
                <p>del 04 al 05</p>
                <p class="month">SEPTIEMBRE</p>
                <span class="event-line"></span>
                <a href="" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1280w, img/marker@2x.png 2560w">
                Barcelona</p>
                <a href="" class="event-moreinfo">
                    <img src="img/mas.png" srcset="img/mas.png 1280w, img/mas@2x.png 2560w">
                </a>
            </div><div class="event">
                <h3>FESTA DE LA MERCÈ</h3>
                <p>día 08 de</p>
                <p class="month">SEPTIEMBRE</p>
                <span class="event-line"></span>
                <a href="" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1280w, img/marker@2x.png 2560w">
                Barcelona</p>
                <a href="" class="event-moreinfo">
                    <img src="img/mas.png" srcset="img/mas.png 1280w, img/mas@2x.png 2560w">
                </a>
            </div>
            <div class="event">
                <h3>TOMORROW -LAND</h3>
                <p>del 12 al 14</p>
                <p class="month">SEPTIEMBRE</p>
                <span class="event-line"></span>
                <a href="" target="_blank" class="location"><img src="img/marker.png" srcset="img/marker.png 1280w, img/marker@2x.png 2560w">
                Barcelona</p>
                <a href="" class="event-moreinfo">
                    <img src="img/mas.png" srcset="img/mas.png 1280w, img/mas@2x.png 2560w">
                </a>
            </div>
        </div>
    </div>
    <div class="slide">
        <div class="slideshow wheels-slideshow">
            <ul id="wheels-slide">
              <li>
                <img src="img/slide/1.jpg" srcset="img/slide/1.jpg 1280w, img/slide/1@2x.jpg 2560w" alt="">
                <span class="wheels-slide-caption"></span>
              </li>
              <li>
                <img src="img/slide/2.jpg" srcset="img/slide/2.jpg 1280w, img/slide/2@2x.jpg 2560w" alt="">
                <span class="wheels-slide-caption"></span>
              </li>
              <li>
                <img src="img/slide/3.jpg" srcset="img/slide/3.jpg 1280w, img/slide/3@2x.jpg 2560w" alt="">
                <span class="wheels-slide-caption"></span>
              </li>
            </ul>
        </div>
    </div>
    <div class="slide wheels-contacto">
        <div>
            <h1>contact.</h1>
            <span>Email. <a href="mailto:truck@surfhousebarcelona.com">truck@surfhousebarcelona.com</a></span>
            <span>Phone. +34 618 141 764</span>
            <span class="wheels-line"></span>
        </div>

        <span class="firma blink"><a href="http://www.digitaldosis.com" target="_blank">DIGITAL DOSIS WEB DESIGN</a></span>
    </div>
</div>
<style type="text/css">

    html.fsvs body.active-slide-1 #fsvs-pagination li.active > span,
    html.fsvs body.active-slide-1 #fsvs-pagination li.active > span > span {
        border-color: #4A1D88;
    }

    html.fsvs body.active-slide-1 #fsvs-pagination li > span > span {
        background: #4A1D88;
    }
</style>
<script type="text/javascript" src="js/slideshow.js"></script>