<div class="overlay overlay-hugeinc">
    <a href="javascript:void('0');" class="overlay-close opacity-hover">Close</a>

    <div class="lang-switch menu-lang">
        <a href="en" <?php if ( $lang['IDIOMA'] == 'en' ) echo 'class="selected"'; ?>>ENG <span class="border"></span></a>
        <span>  /  </span>
        <a href="es" <?php if ( $lang['IDIOMA'] == 'es' ) echo 'class="selected"'; ?>>ESP <span class="border"></span></a>
    </div>

    <nav>
        <ul>
            <li>
                    <a href="/">
                        HOME
                    </a>
                </li>
                <li class="desktop">
                    <a href="/fooddrinks">
                        FOOD&DRINKS
                    </a>
                </li>
                <li class="mobile">
                    <a href="/mobile/food">
                        FOOD
                    </a>
                </li>
                <li class="mobile">
                    <a href="/mobile/drinks">
                        DRINKS
                    </a>
                </li>
                <li class="desktop">
                    <a href="/activities">
                        <?php echo $lang['ACTIVITIES'] ?>
                    </a>
                </li>
                <li class="mobile">
                    <a href="/mobile/activities">
                        <?php echo $lang['ACTIVITIES'] ?>
                    </a>
                </li>
                <li class="desktop">
                    <a href="/about">
                        <?php echo $lang['ABOUT_SHB'] ?>
                    </a>
                </li>
                <li class="mobile">
                    <a href="/mobile/contact">
                        <?php echo $lang['CONTACT'] ?>
                    </a>
                </li>
                <li>
                    <a href="http://surfhouseonwheels.com/">
                        ON WHEELS
                    </a>
                </li>
                <li>
                    <span class="border"></span>
                    <a href="http://surfhouse-shop.com/" target="_blank">
                        SH SHOP
                    </a>
                </li>
        </ul>
    </nav>

    <a href="http://www.digitaldosis.com" target="_blank" class="firma-menu">
        DIGITAL DOSIS WEB DESIGN
    </a>

    <div class="menu-footer">
        <ul>
            <li>
                <?php echo $lang['HORARIO']; ?>
            </li>
            <li>
                <?php echo $lang['MIERCOLES']; ?>
            </li>
            <li>
                <?php echo $lang['VIERNES']; ?>
            </li>
             <li>
                <?php echo $lang['DOMINGO']; ?>
            </li>
        </ul>

        <ul>
            <li>
                <a href="mailto:hello@surfhousebarcelona.com" class="blink">
                    HELLO@SURFHOUSEBARCELONA.COM
                </a>
            </li>
            <li>
                T. 932 507 023
            </li>
            <li>
                <a href="https://www.google.es/maps/place/Surf+House+Barcelona/@41.3784416,2.1912691,17.51z/data=!4m2!3m1!1s0x0000000000000000:0xed8b40122aff6ebd" target="_blank" class="blink">
                    C/ L’ALMIRALL AIXADA 22 08003 BARCELONA
                </a>
            </li>
        </ul>
    </div>

</div>
